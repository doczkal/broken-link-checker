Getting links from: https://fsfe.org/news/nl/nl-201310.hr.html
├───OK─── https://fsfe.org/news/nl/nl-201310.de.html
├───OK─── https://fsfe.org/news/nl/nl-201310.fr.html
├───OK─── https://fsfe.org/freesoftware/basics/gnuproject.hr.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.hr.html (HTTP_404)
Finished! 120 links found. 116 excluded. 1 broken.
