Getting links from: https://fsfe.org/news/nl/nl-201704.ro.html#
├───OK─── https://fsfe.org/activities/radiodirective/index.ro.html
├───OK─── https://fsfe.org/news/2017/news-20170321-01.ro.html
├───OK─── https://fsfe.org/news/2017/news-20170116-01.ro.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.ro.html#id-fellowship-seats (HTTP_404)
Finished! 103 links found. 99 excluded. 1 broken.
