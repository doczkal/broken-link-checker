Getting links from: https://fsfe.org/news/nl/nl-201501.tr.html
├───OK─── https://fsfe.org/news/2014/news-20141218-02.tr.html
├───OK─── https://fsfe.org/fellowship/index.tr.html
├───OK─── https://fsfe.org/fellowship/about/fellowship.tr.html#youget
├─BROKEN─ https://fsfe.org/about/legal/constitution.tr.html#id-fellowship-seats (HTTP_404)
├─BROKEN─ https://fsfe.org/about/ojasild (HTTP_404)
Finished! 88 links found. 83 excluded. 2 broken.
