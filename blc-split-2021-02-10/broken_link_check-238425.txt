Getting links from: https://fsfe.org/news/nl/nl-201704.ar.html
├───OK─── https://fsfe.org/news/nl/nl-201704.de.html
├───OK─── https://fsfe.org/news/nl/nl-201704.es.html
├───OK─── https://fsfe.org/news/nl/nl-201704.sq.html
├───OK─── https://fsfe.org/news/2017/news-20170321-01.ar.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.ar.html#id-fellowship-seats (HTTP_404)
Finished! 104 links found. 99 excluded. 1 broken.
