Getting links from: https://fsfe.org/freesoftware/education/edu-related-content.sq.html
├───OK─── https://fsfe.org/activities/mankind/lsm2002/index.sq.html
├─BROKEN─ https://fsfe.org/associates/ofset/associate (HTTP_404)
├───OK─── https://fsfe.org/activities/mankind/lsm2002/press-release.sq.html
├───OK─── https://fsfe.org/news/2001/article2001-12-17-01.sq.html
├───OK─── https://fsfe.org/freesoftware/transcripts/rms-fs-2006-03-09.sq.html
├─BROKEN─ https://fsfe.org/freesoftware/education/news/2012/news-20121217-01 (HTTP_404)
├───OK─── https://fsfe.org/news/2003/lettera_MIUR-2003-07-16.sq.html
├───OK─── https://fsfe.org/activities/tgs/tgs.sq.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool1.sq.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool2.sq.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool3.sq.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool4.sq.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool5.sq.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool6.sq.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool7.sq.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool8.sq.html
Finished! 111 links found. 95 excluded. 2 broken.
