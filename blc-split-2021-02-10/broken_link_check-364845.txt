Getting links from: https://fsfe.org/news/nl/nl-201205.cs.html
├─BROKEN─ https://fsfe.org/activities/swpat/swapt.cs.html (HTTP_404)
├─BROKEN─ https://fsfe.org/activities/elections/askyourcandidates/askyourcandidtes.cs.html (HTTP_404)
├─BROKEN─ https://fsfe.org/project/os/def.cs.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.cs.html (HTTP_404)
Finished! 112 links found. 108 excluded. 4 broken.
