Getting links from: https://fsfe.org/news/2019/news-20191022-01.nl.html
├───OK─── https://fsfe.org/activities/routers/index.nl.html
├───OK─── https://fsfe.org/news/2016/news-20160725-01.nl.html
├─BROKEN─ https://fsfe.org/news/2019/(https:/t3n.de/news/barcelona-touristen-hochburg-1139070/) (HTTP_404)
├───OK─── https://fsfe.org/news/2019/news-20190515-01.nl.html
├───OK─── https://fsfe.org/news/2019/news-20190515-02.nl.html
├─BROKEN─ https://fsfe.org/news/2019/about/code%20of%20conduct (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/contribute/web/web.nl.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/news-20191002.nl.html (HTTP_404)
├───OK─── https://fsfe.org/activities/ftf/legal-conference.nl.html
├───OK─── https://fsfe.org/news/2019/news-20190701-01.nl.html
Finished! 220 links found. 210 excluded. 4 broken.
