Getting links from: https://fsfe.org/about/people/roy/roy.es.html
├───OK─── https://fsfe.org/contribute/internship.es.html
├───OK─── https://fsfe.org/activities/ftf/ftf.es.html
├─BROKEN─ https://fsfe.org/fr/index.es.html (HTTP_404)
├─BROKEN─ https://fsfe.org/legal (HTTP_404)
Finished! 81 links found. 77 excluded. 2 broken.
