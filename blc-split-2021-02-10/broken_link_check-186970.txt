Getting links from: https://fsfe.org/news/2007/news-20070630-01.ru.html
├───OK─── https://fsfe.org/activities/ipred2/index.ru.html
├─BROKEN─ https://fsfe.org/ftf (HTTP_404)
├───OK─── https://fsfe.org/activities/self/index.ru.html
├─BROKEN─ https://fsfe.org/fellows/greve/img/zrh_hoist (HTTP_404)
├─BROKEN─ https://fsfe.org/fellows/meetings/2006_bolzano (HTTP_404)
├───OK─── https://fsfe.org/associates/index.ru.html
Finished! 105 links found. 99 excluded. 3 broken.
