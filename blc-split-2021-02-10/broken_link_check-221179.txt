Getting links from: https://fsfe.org/news/2015/news-20150303-01.ru.html
├───OK─── https://fsfe.org/news/2015/news-20150303-01.es.html
├─BROKEN─ https://fsfe.org/activities/ilovefs/whylovefs/photos/gallery/ilovefs-gallery-thumb-39.jpg (HTTP_404)
├─BROKEN─ https://fsfe.org/activities/ilovefs/whylovefs/photos/gallery/ilovefs-gallery-thumb-35.jpg (HTTP_404)
├─BROKEN─ https://fsfe.org/activities/ilovefs/whylovefs/photos/gallery/ilovefs-gallery-thumb-25.jpg (HTTP_404)
├───OK─── https://fsfe.org/activities/ilovefs/whylovefs/whylovefs.ru.html
├───OK─── https://fsfe.org/activities/ilovefs/artwork/artwork.ru.html
Finished! 105 links found. 99 excluded. 3 broken.
