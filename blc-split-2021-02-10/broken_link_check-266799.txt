Getting links from: https://fsfe.org/about/people/mehl/index.et.html
├───OK─── https://fsfe.org/activities/radiodirective/index.et.html
├─BROKEN─ https://fsfe.org/de (HTTP_404)
├───OK─── https://fsfe.org/activities/android/index.et.html
├───OK─── https://fsfe.org/activities/drm/index.et.html
├───OK─── https://fsfe.org/contribute/web/index.et.html
├─BROKEN─ https://fsfe.org/de (HTTP_404)
├─BROKEN─ https://fsfe.org/de (HTTP_404)
Finished! 82 links found. 75 excluded. 3 broken.
