Getting links from: https://fsfe.org/news/nl/nl-201704.de.html
├───OK─── https://fsfe.org/news/2017/news-20170302-01.de.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.de.html#id-fellowship-seats (HTTP_404)
Finished! 100 links found. 98 excluded. 1 broken.
