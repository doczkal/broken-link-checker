Getting links from: https://fsfe.org/news/nl/nl-201106.cs.html
├───OK─── https://fsfe.org/news/nl/nl-201106.en.html
├───OK─── https://fsfe.org/activities/ms-vs-eu/timeline.cs.html
├───OK─── https://fsfe.org/freesoftware/standards/bt-open-letter.cs.html
├───OK─── https://fsfe.org/news/2011/news-20110511-01.cs.html
├───OK─── https://fsfe.org/news/2011/news-20110520-01.cs.html
├─BROKEN─ https://fsfe.org/source/activities/elections/askyourcandidates/askyourcandidates.xhtml (HTTP_404)
Finished! 90 links found. 84 excluded. 1 broken.
