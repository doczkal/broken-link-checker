Getting links from: https://fsfe.org/freesoftware/education/edu-related-content.da.html
├───OK─── https://fsfe.org/activities/mankind/lsm2002/index.da.html
├─BROKEN─ https://fsfe.org/associates/ofset/associate (HTTP_404)
├───OK─── https://fsfe.org/activities/mankind/lsm2002/press-release.da.html
├───OK─── https://fsfe.org/news/2001/article2001-12-17-01.da.html
├───OK─── https://fsfe.org/freesoftware/transcripts/rms-fs-2006-03-09.da.html
├───OK─── https://fsfe.org/events/2004/FISL/fisl.da.html
├─BROKEN─ https://fsfe.org/freesoftware/education/news/2012/news-20121217-01 (HTTP_404)
├───OK─── https://fsfe.org/news/2003/lettera_MIUR-2003-07-16.da.html
├───OK─── https://fsfe.org/activities/tgs/tgs.da.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool1.da.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool2.da.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool3.da.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool4.da.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool5.da.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool6.da.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool7.da.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool8.da.html
Finished! 111 links found. 94 excluded. 2 broken.
