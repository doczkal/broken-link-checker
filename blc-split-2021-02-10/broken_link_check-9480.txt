Getting links from: https://fsfe.org/news/2009/news-20090620-01.nl.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.nl.html (HTTP_404)
├─BROKEN─ https://fsfe.org/about/holz/holz.nl.html (HTTP_404)
├─BROKEN─ https://fsfe.org/activities/self/SELF-LegalPolicy-0.9_1.pdf (HTTP_404)
├───OK─── https://fsfe.org/activities/stacs/london.nl.html
├───OK─── https://fsfe.org/activities/stacs/belgrade.nl.html
Finished! 127 links found. 122 excluded. 3 broken.
