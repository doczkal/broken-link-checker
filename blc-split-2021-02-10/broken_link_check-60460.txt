Getting links from: https://fsfe.org/news/nl/nl-201202.ar.html
├───OK─── https://fsfe.org/news/2012/news-20120131-01.ar.html
├─BROKEN─ https://fsfe.org/activities/ilovefs/2012/banners.ar.html (HTTP_404)
├───OK─── https://fsfe.org/activities/ilovefs/2012/unperfekthaus.ar.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.ar.html (HTTP_404)
Finished! 110 links found. 106 excluded. 2 broken.
