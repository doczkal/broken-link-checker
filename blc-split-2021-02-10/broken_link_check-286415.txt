Getting links from: https://fsfe.org/about/people/roy/roy.da.html
├───OK─── https://fsfe.org/activities/ftf/ftf.da.html
├─BROKEN─ https://fsfe.org/fr/index.da.html (HTTP_404)
├─BROKEN─ https://fsfe.org/legal (HTTP_404)
├───OK─── https://fsfe.org/fellowship/index.da.html
Finished! 80 links found. 76 excluded. 2 broken.
