Getting links from: https://fsfe.org/news/nl/nl-201201.ro.html
├───OK─── https://fsfe.org/news/2011/news-20111128-02.ro.html
├─BROKEN─ https://fsfe.org/activities/elections/askyourcandidates/example-questions.xhtml (HTTP_404)
├───OK─── https://fsfe.org/news/2011/news-20111201-02.ro.html
├───OK─── https://fsfe.org/news/2011/news-20111213-01.ro.html
├───OK─── https://fsfe.org/news/2011/news-20111220-01.ro.html
├───OK─── https://fsfe.org/donate/letter-2011.ro.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.ro.html (HTTP_404)
Finished! 87 links found. 80 excluded. 2 broken.
