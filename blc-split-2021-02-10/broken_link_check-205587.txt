Getting links from: https://fsfe.org/news/2012/report-2012.en.html
├─BROKEN─ https://fsfe.org/news/2012/blogs.fsfe.org/samtuke/%3Fp%3D255 (HTTP_404)
├───OK─── https://fsfe.org/news/2012/news-20120627-01.en.html
├───OK─── https://fsfe.org/activities/ftf/ftf.en.html
Finished! 90 links found. 87 excluded. 1 broken.
