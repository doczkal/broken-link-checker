Getting links from: https://fsfe.org/activities/igf/sovsoft.et.html
├─BROKEN─ https://fsfe.org/activities/igf/ref3 (HTTP_404)
├─BROKEN─ https://fsfe.org/activities/igf/4 (HTTP_404)
├───OK─── https://fsfe.org/activities/ms-vs-eu/index.et.html
├───OK─── https://fsfe.org/activities/wipo/fser.et.html
Finished! 93 links found. 89 excluded. 2 broken.
