Getting links from: https://fsfe.org/news/nl/nl-201106.et.html
├───OK─── https://fsfe.org/news/nl/nl-201106.cs.html
├───OK─── https://fsfe.org/news/nl/nl-201106.it.html
├───OK─── https://fsfe.org/freesoftware/standards/bt-open-letter.et.html
├───OK─── https://fsfe.org/news/2011/news-20110511-01.et.html
├───OK─── https://fsfe.org/news/2011/news-20110520-01.et.html
├─BROKEN─ https://fsfe.org/source/activities/elections/askyourcandidates/askyourcandidates.xhtml (HTTP_404)
Finished! 92 links found. 86 excluded. 1 broken.
