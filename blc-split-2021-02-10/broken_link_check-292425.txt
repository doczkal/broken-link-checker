Getting links from: https://fsfe.org/activities/pdfreaders/buglist.en.html
├───OK─── https://fsfe.org/activities/pdfreaders/buglist.de.html
├───OK─── https://fsfe.org/activities/pdfreaders/buglist.el.html
├───OK─── https://fsfe.org/activities/pdfreaders/buglist.it.html
├───OK─── https://fsfe.org/activities/pdfreaders/buglist.pt.html
├───OK─── https://fsfe.org/activities/pdfreaders/buglist.ru.html
├─BROKEN─ https://fsfe.org/activities/pdfreaders/lodz.uw.gov.pl/data/auctions/siwz_serwis.pdf (HTTP_404)
Finished! 2206 links found. 2200 excluded. 1 broken.
