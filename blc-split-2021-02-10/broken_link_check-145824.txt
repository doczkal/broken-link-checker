Getting links from: https://fsfe.org/about/people/testimonials.sl.html#kroah-hartman
├───OK─── https://fsfe.org/about/people/interviews/cryptie.sl.html#video-30s-cryptie
├───OK─── https://fsfe.org/about/people/interviews/lequertier.sl.html#video-30s-vincent
├───OK─── https://fsfe.org/about/people/interviews/zerolo.sl.html#video-30s-tomas
├─BROKEN─ https://fsfe.org/about/people/interviews/bernhard.sl.html#video-30s-bernhard (HTTP_404)
├───OK─── https://fsfe.org/about/people/interviews/weitzhofer.sl.html#interview
├───OK─── https://fsfe.org/about/people/interviews/grun.sl.html#interview
├───OK─── https://fsfe.org/about/people/interviews/ockers.sl.html#video-30s-andre
├───OK─── https://fsfe.org/about/people/interviews/mueller.sl.html#interview
├───OK─── https://fsfe.org/about/people/interviews/snow.sl.html#interview
├───OK─── https://fsfe.org/about/people/interviews/gkotsopoulou.sl.html#interview
Finished! 122 links found. 112 excluded. 1 broken.
