Getting links from: https://fsfe.org/news/nl/nl-201302.sv.html#
├───OK─── https://fsfe.org/news/2012/news-20121211-01.sv.html
├───OK─── https://fsfe.org/news/2012/news-20121217-01.sv.html
├───OK─── https://fsfe.org/news/2012/report-2012.sv.html
├───OK─── https://fsfe.org/activities/ilovefs/2013/index.sv.html
├───OK─── https://fsfe.org/activities/ilovefs/2013/whylovefs.sv.html
├─BROKEN─ https://fsfe.org/activities/ilovefs/2013/banners.sv.html (HTTP_404)
├─BROKEN─ https://fsfe.org/order/promoorder.sv.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.sv.html (HTTP_404)
Finished! 119 links found. 111 excluded. 3 broken.
