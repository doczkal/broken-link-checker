Getting links from: https://fsfe.org/news/nl/nl-201209.ar.html
├───OK─── https://fsfe.org/freesoftware/standards/def.ar.html
├───OK─── https://fsfe.org/activities/msooxml/msooxml-questions.ar.html
├───OK─── https://fsfe.org/news/2012/news-20120831-01.ar.html
├───OK─── https://fsfe.org/news/2011/news-20110301-01.ar.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.ar.html (HTTP_404)
Finished! 116 links found. 111 excluded. 1 broken.
