Getting links from: https://fsfe.org/news/2019/news-20191022-01.el.html#events2019
├───OK─── https://fsfe.org/activities/radiodirective
├───OK─── https://fsfe.org/activities/radiodirective/statement.el.html
├───OK─── https://fsfe.org/news/2019/news-20190807-01.el.html
├───OK─── https://fsfe.org/contribute/promopics/ilovefs-sticker-444px.jpg
├───OK─── https://fsfe.org/about/people/interviews/grun.el.html#interview
├─BROKEN─ https://fsfe.org/news/2019/about/codeofconduct (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/contribute/web/web.el.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/news-20191002.el.html (HTTP_404)
├───OK─── https://fsfe.org/news/2019/news-20190701-01.el.html
├───OK─── https://fsfe.org/news/2019/news-20191014-01.el.html
├───OK─── https://fsfe.org/order/2017/kidstshirt-fork-red-front-large-350px.jpg
Finished! 222 links found. 211 excluded. 3 broken.
