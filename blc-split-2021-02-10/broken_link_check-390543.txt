Getting links from: https://fsfe.org/news/nl/nl-201810.hu.html
├─BROKEN─ https://fsfe.org/about/mancheva/mancheva (HTTP_404)
├───OK─── https://fsfe.org/news/2018/news-20181010-01.hu.html
├───OK─── https://fsfe.org/news/2018/news-20180913-01.hu.html
├───OK─── https://fsfe.org/tags/tagged-microsoft.hu.html
├───OK─── https://fsfe.org/tags/tagged-digital-o-mat.hu.html
Finished! 93 links found. 88 excluded. 1 broken.
