Getting links from: https://fsfe.org/news/nl/nl-201212.en.html
├───OK─── https://fsfe.org/news/2012/news-20121116-01.en.html
├─BROKEN─ https://fsfe.org/news/nl/lwn.net/Articles/523537/index.en.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.en.html (HTTP_404)
Finished! 112 links found. 109 excluded. 2 broken.
