Getting links from: https://fsfe.org/news/nl/nl-201306.nb.html
├───OK─── https://fsfe.org/news/2013/news-20130503-01.nb.html
├───OK─── https://fsfe.org/events/2013/linuxtag-2013.nb.html
├───OK─── https://fsfe.org/news/2013/news-20130429-01.nb.html
├───OK─── https://fsfe.org/freesoftware/legal/flashingdevices.nb.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.nb.html (HTTP_404)
Finished! 117 links found. 112 excluded. 1 broken.
