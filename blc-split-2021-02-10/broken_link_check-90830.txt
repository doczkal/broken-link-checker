Getting links from: https://fsfe.org/news/2017/index.nn.html
├───OK─── https://fsfe.org/news/2017/news-20171211-01.nn.html
├───OK─── https://fsfe.org/news/2017/news-20171207-02.nn.html
├───OK─── https://fsfe.org/news/2017/news-20170911-01.nn.html
├───OK─── https://fsfe.org/news/2017/news-20170906-01.nn.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.nn.html#id-fellowship-seats (HTTP_404)
Finished! 300 links found. 294 excluded. 2 broken.
