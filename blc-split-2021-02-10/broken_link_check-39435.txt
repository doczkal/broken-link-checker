Getting links from: https://fsfe.org/news/nl/nl-201501.sk.html
├───OK─── https://fsfe.org/fellowship/about/fellowship.sk.html#youget
├─BROKEN─ https://fsfe.org/about/legal/constitution.sk.html#id-fellowship-seats (HTTP_404)
├─BROKEN─ https://fsfe.org/about/ojasild (HTTP_404)
Finished! 88 links found. 85 excluded. 2 broken.
