Getting links from: https://fsfe.org/news/2016/news-20160224-01.cs.html
├─BROKEN─ https://fsfe.org/activities/ilovefs/whylovefs/photos/gallery/ilovefs-gallery-thumb-25.jpg (HTTP_404)
├───OK─── https://fsfe.org/news/2015/news-20150325-01.cs.html
Finished! 89 links found. 87 excluded. 1 broken.
