Getting links from: https://fsfe.org/news/2009/news-20090620-01.sl.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.sl.html (HTTP_404)
├─BROKEN─ https://fsfe.org/about/holz/holz.sl.html (HTTP_404)
├─BROKEN─ https://fsfe.org/activities/self/SELF-LegalPolicy-0.9_1.pdf (HTTP_404)
├───OK─── https://fsfe.org/activities/stacs/london.sl.html
├───OK─── https://fsfe.org/activities/stacs/belgrade.sl.html
Finished! 127 links found. 122 excluded. 3 broken.
