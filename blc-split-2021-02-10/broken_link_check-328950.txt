Getting links from: https://fsfe.org/news/nl/nl-201501.fr.html
├───OK─── https://fsfe.org/news/2014/news-20141218-02.fr.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.fr.html#id-fellowship-seats (HTTP_404)
├─BROKEN─ https://fsfe.org/about/ojasild (HTTP_404)
Finished! 86 links found. 83 excluded. 2 broken.
