Getting links from: https://fsfe.org/news/2019/news-20191022-01.sq.html#events2019
├───OK─── https://fsfe.org/activities/routers/timeline.sq.html
├─BROKEN─ https://fsfe.org/news/2019/about/codeofconduct (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/contribute/web/web.sq.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/news-20191002.sq.html (HTTP_404)
├───OK─── https://fsfe.org/activities/ln/llw.sq.html
├───OK─── https://fsfe.org/news/2019/news-20190701-01.sq.html
├───OK─── https://fsfe.org/news/2019/news-20191014-01.sq.html
├───OK─── https://fsfe.org/tags/tagged-savecodeshare.sq.html
Finished! 222 links found. 214 excluded. 3 broken.
