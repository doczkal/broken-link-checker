Getting links from: https://fsfe.org/contact/index.sv.html
├───OK─── https://fsfe.org/about/legal/legal.sv.html
├───OK─── https://fsfe.org/freesoftware/index.sv.html
├───OK─── https://fsfe.org/activities/ln/ln.sv.html
├───OK─── https://fsfe.org/about/people/kirschner/index.sv.html
├───OK─── https://fsfe.org/activities/licence-questions/licence-questions.sv.html
├─BROKEN─ https://fsfe.org/about/codeofconduct/index.sv.html (HTTP_404)
├───OK─── https://fsfe.org/contact/community.sv.html#irc
├───OK─── https://fsfe.org/news/news.sv.rss
Finished! 86 links found. 78 excluded. 1 broken.
