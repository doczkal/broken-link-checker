Getting links from: https://fsfe.org/news/nl/nl-201209.bs.html
├───OK─── https://fsfe.org/freesoftware/standards/def.bs.html
├───OK─── https://fsfe.org/activities/msooxml/msooxml-questions.bs.html
├───OK─── https://fsfe.org/news/2012/news-20120831-01.bs.html
├───OK─── https://fsfe.org/news/2011/news-20110301-01.bs.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.bs.html (HTTP_404)
Finished! 115 links found. 110 excluded. 1 broken.
