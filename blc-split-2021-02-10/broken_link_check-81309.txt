Getting links from: https://fsfe.org/news/2012/report-2012.sv.html
├─BROKEN─ https://fsfe.org/news/2012/blogs.fsfe.org/samtuke/%3Fp%3D255 (HTTP_404)
├───OK─── https://fsfe.org/news/2012/news-20121208-01.sv.html
├───OK─── https://fsfe.org/news/2012/news-20120425-02.sv.html
├───OK─── https://fsfe.org/news/2012/news-20121112-01.sv.html
├───OK─── https://fsfe.org/news/2012/news-20121106-01.sv.html
Finished! 93 links found. 88 excluded. 1 broken.
