Getting links from: https://fsfe.org/news/nl/nl-202005.fi.html
├───OK─── https://fsfe.org/news/2020/news-20200408-01.fi.html
├───OK─── https://fsfe.org/news/2020/news-20200506-01.fi.html
├───OK─── https://fsfe.org/news/2020/news-20200402-02.fi.html
├───OK─── https://fsfe.org/news/podcast/episode-5.fi.html
├───OK─── https://fsfe.org/news/podcast/episode-special-1.fi.html
├───OK─── https://fsfe.org/news/2020/news-20200427-01.fi.html
├─BROKEN─ https://fsfe.org/https:/librezoom.net/lz19-die-nexte-bitte/index.fi.html (HTTP_404)
├───OK─── https://fsfe.org/news/2020/news-20200424-01.fi.html
Finished! 99 links found. 91 excluded. 1 broken.
