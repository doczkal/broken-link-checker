Getting links from: https://fsfe.org/news/nl/nl-201202.uk.html
├───OK─── https://fsfe.org/news/nl/nl-201202.fr.html
├───OK─── https://fsfe.org/news/2012/news-20120131-01.uk.html
├─BROKEN─ https://fsfe.org/activities/ilovefs/2012/banners.uk.html (HTTP_404)
├───OK─── https://fsfe.org/activities/ilovefs/2012/unperfekthaus.uk.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.uk.html (HTTP_404)
Finished! 109 links found. 104 excluded. 2 broken.
