Getting links from: https://fsfe.org/about/people/roy/index.da.html
├───OK─── https://fsfe.org/about/people/roy/roy.el.html
├───OK─── https://fsfe.org/about/people/roy/roy.en.html
├───OK─── https://fsfe.org/about/people/roy/roy.fr.html
├───OK─── https://fsfe.org/about/people/roy/roy.nb.html
├───OK─── https://fsfe.org/about/people/roy/roy.nl.html
├───OK─── https://fsfe.org/about/people/roy/roy.ru.html
├─BROKEN─ https://fsfe.org/fr/index.da.html (HTTP_404)
├─BROKEN─ https://fsfe.org/legal (HTTP_404)
Finished! 80 links found. 72 excluded. 2 broken.
