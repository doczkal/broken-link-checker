Getting links from: https://fsfe.org/news/nl/nl-201212.da.html
├───OK─── https://fsfe.org/news/2012/news-20121116-01.da.html
├─BROKEN─ https://fsfe.org/news/nl/lwn.net/Articles/523537/index.da.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.da.html (HTTP_404)
Finished! 115 links found. 112 excluded. 2 broken.
