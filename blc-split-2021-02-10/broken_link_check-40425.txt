Getting links from: https://fsfe.org/news/2019/news-20191022-01.zh.html
├───OK─── https://fsfe.org/activities/radiodirective
├─BROKEN─ https://fsfe.org/news/2019/about/codeofconduct (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/contribute/web/web.zh.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/news-20191002.zh.html (HTTP_404)
├───OK─── https://fsfe.org/news/2019/news-20190701-01.zh.html
Finished! 222 links found. 217 excluded. 3 broken.
