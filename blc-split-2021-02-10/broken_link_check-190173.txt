Getting links from: https://fsfe.org/news/nl/nl-201308.cs.html
├───OK─── https://fsfe.org/news/2013/news-20130729-01.cs.html
├───OK─── https://fsfe.org/news/2013/news-20130730-01.cs.html
├─BROKEN─ https://fsfe.org/news/2013/2013-07-26_Open_Letter_to_NEC.cs.html (HTTP_404)
├───OK─── https://fsfe.org/news/2013/news-20130712-01.cs.html
├───OK─── https://fsfe.org/freesoftware/standards/transparency-letter.cs.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.cs.html (HTTP_404)
Finished! 112 links found. 106 excluded. 2 broken.
