Getting links from: https://fsfe.org/news/nl/nl-201202.ro.html
├───OK─── https://fsfe.org/activities/nledu/nledu.ro.html
├───OK─── https://fsfe.org/news/2012/news-20120131-01.ro.html
├─BROKEN─ https://fsfe.org/activities/ilovefs/2012/banners.ro.html (HTTP_404)
├───OK─── https://fsfe.org/activities/ilovefs/2012/unperfekthaus.ro.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.ro.html (HTTP_404)
Finished! 109 links found. 104 excluded. 2 broken.
