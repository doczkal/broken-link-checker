Getting links from: https://fsfe.org/news/2008/news-20081210-01.ar.html
├───OK─── https://fsfe.org/news/2008/news-20081210-01.de.html
├───OK─── https://fsfe.org/news/2008/news-20081210-01.el.html
├───OK─── https://fsfe.org/news/2008/news-20081210-01.fr.html
├───OK─── https://fsfe.org/news/2008/news-20081210-01.it.html
├───OK─── https://fsfe.org/news/2008/news-20081210-01.nl.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.ar.html (HTTP_404)
Finished! 69 links found. 63 excluded. 1 broken.
