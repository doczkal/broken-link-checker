Getting links from: https://fsfe.org/activities/igf/wgig.de.html
├───OK─── https://fsfe.org/associates/index.de.html
├─BROKEN─ https://fsfe.org/activities/igf/WGIG-WP-Cybercrime-Comments.pdf (HTTP_404)
├─BROKEN─ https://fsfe.org/activities/igf/WGIG-WP-IPR-Comments.pdf (HTTP_404)
├───OK─── https://fsfe.org/activities/igf/index.de.html
Finished! 60 links found. 56 excluded. 2 broken.
