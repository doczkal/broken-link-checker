Getting links from: https://fsfe.org/contact/index.el.html
├───OK─── https://fsfe.org/about/index.el.html
├─BROKEN─ https://fsfe.org/about/codeofconduct/index.el.html (HTTP_404)
├───OK─── https://fsfe.org/news/newsletter.el.html
Finished! 86 links found. 83 excluded. 1 broken.
