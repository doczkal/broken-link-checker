Getting links from: https://fsfe.org/about/funds/2006.hr.html
├───OK─── https://fsfe.org/about/funds/2006.de.html
├───OK─── https://fsfe.org/about/funds/2006.el.html
├───OK─── https://fsfe.org/about/funds/2006.fr.html
├───OK─── https://fsfe.org/about/funds/2006.it.html
├───OK─── https://fsfe.org/about/funds/2006.nl.html
├───OK─── https://fsfe.org/help/thankgnus-2006.hr.html
├─BROKEN─ https://fsfe.org/about/legal/de/de.hr.html (HTTP_404)
Finished! 89 links found. 82 excluded. 1 broken.
