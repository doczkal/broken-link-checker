Getting links from: https://fsfe.org/news/nl/nl-201308.de.html
├───OK─── https://fsfe.org/news/2013/news-20130729-01.de.html
├─BROKEN─ https://fsfe.org/news/2013/2013-07-26_Open_Letter_to_NEC.de.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.de.html (HTTP_404)
Finished! 110 links found. 107 excluded. 2 broken.
