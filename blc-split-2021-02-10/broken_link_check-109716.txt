Getting links from: https://fsfe.org/contact/contact.ca.html
├───OK─── https://fsfe.org/about/index.ca.html
├───OK─── https://fsfe.org/about/legal/legal.ca.html
├───OK─── https://fsfe.org/freesoftware/index.ca.html
├───OK─── https://fsfe.org/activities/ln/ln.ca.html
├───OK─── https://fsfe.org/activities/policy.ca.html
├───OK─── https://fsfe.org/about/team.ca.html
├─BROKEN─ https://fsfe.org/about/codeofconduct/index.ca.html (HTTP_404)
Finished! 86 links found. 79 excluded. 1 broken.
