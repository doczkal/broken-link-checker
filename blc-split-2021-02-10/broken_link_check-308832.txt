Getting links from: https://fsfe.org/news/nl/nl-201704.tr.html
├───OK─── https://fsfe.org/associates/associates.tr.html
├───OK─── https://fsfe.org/activities/radiodirective/index.tr.html
├───OK─── https://fsfe.org/news/2017/news-20170315-01.tr.html
├───OK─── https://fsfe.org/activities/ilovefs/whylovefs/gallery.tr.html
├───OK─── https://fsfe.org/news/2017/news-20170321-01.tr.html
├───OK─── https://fsfe.org/news/2017/news-20170116-01.tr.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.tr.html#id-fellowship-seats (HTTP_404)
Finished! 103 links found. 96 excluded. 1 broken.
