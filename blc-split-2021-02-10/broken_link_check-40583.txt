Getting links from: https://fsfe.org/about/people/kirschner/index.ca.html
├───OK─── https://fsfe.org/about/people/kirschner/kirschner.en.html
├───OK─── https://fsfe.org/about/people/kirschner/kirschner-public.asc
├─BROKEN─ https://fsfe.org/de/index.ca.html (HTTP_404)
├───OK─── https://fsfe.org/fellowship/index.ca.html
├─BROKEN─ https://fsfe.org/de/index.ca.html (HTTP_404)
├─BROKEN─ https://fsfe.org/de/index.ca.html (HTTP_404)
Finished! 72 links found. 66 excluded. 3 broken.
