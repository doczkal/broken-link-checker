Getting links from: https://fsfe.org/news/2019/news-20191022-01.el.html
├───OK─── https://fsfe.org/activities/routers/timeline.el.html
├───OK─── https://fsfe.org/news/2016/news-20160725-01.el.html
├───OK─── https://fsfe.org/news/2019/news-20190515-02.el.html
├─BROKEN─ https://fsfe.org/news/2019/about/codeofconduct (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/contribute/web/web.el.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/news-20191002.el.html (HTTP_404)
├───OK─── https://fsfe.org/news/2019/news-20190701-01.el.html
├───OK─── https://fsfe.org/news/2019/news-20190927-01.el.html
├───OK─── https://fsfe.org/tags/tagged-savecodeshare.el.html
Finished! 222 links found. 213 excluded. 3 broken.
