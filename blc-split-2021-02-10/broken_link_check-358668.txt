Getting links from: https://fsfe.org/news/nl/nl-201704.bg.html
├───OK─── https://fsfe.org/news/2017/news-20170321-01.bg.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.bg.html#id-fellowship-seats (HTTP_404)
Finished! 103 links found. 101 excluded. 1 broken.
