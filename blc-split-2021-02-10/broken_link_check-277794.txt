Getting links from: https://fsfe.org/news/nl/nl-201810.nn.html
├───OK─── https://fsfe.org/news/nl/nl-201810.en.html
├─BROKEN─ https://fsfe.org/about/mancheva/mancheva (HTTP_404)
├───OK─── https://fsfe.org/news/2018/news-20181010-01.nn.html
├───OK─── https://fsfe.org/news/2018/news-20180913-01.nn.html
├───OK─── https://fsfe.org/tags/tagged-microsoft.nn.html
├───OK─── https://fsfe.org/tags/tagged-digital-o-mat.nn.html
Finished! 93 links found. 87 excluded. 1 broken.
