Getting links from: https://fsfe.org/news/nl/nl-201112.cs.html
├───OK─── https://fsfe.org/news/nl/nl-201112.en.html
├───OK─── https://fsfe.org/news/2011/news-20111128-01.cs.html
├───OK─── https://fsfe.org/activities/pdfreaders/buglist.cs.html
├───OK─── https://fsfe.org/activities/pdfreaders/letter.cs.html
├───OK─── https://fsfe.org/news/2011/news-20111122-01.cs.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.cs.html (HTTP_404)
Finished! 108 links found. 102 excluded. 1 broken.
