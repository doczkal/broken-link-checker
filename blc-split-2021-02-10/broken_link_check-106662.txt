Getting links from: https://fsfe.org/news/2013/index.uk.html
├───OK─── https://fsfe.org/news/2013/news-20131213-01.uk.html
├───OK─── https://fsfe.org/news/2013/news-20130927-01.uk.html
├─BROKEN─ https://fsfe.org/news/2013/2013-07-26_Open_Letter_to_NEC.uk.html (HTTP_404)
├───OK─── https://fsfe.org/news/2013/news-20130226-01.uk.html
Finished! 289 links found. 285 excluded. 1 broken.
