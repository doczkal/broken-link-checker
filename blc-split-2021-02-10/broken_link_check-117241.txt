Getting links from: https://fsfe.org/news/nl/nl-201305.nl.html
├───OK─── https://fsfe.org/news/nl/nl-201305.de.html
├───OK─── https://fsfe.org/news/nl/nl-201305.it.html
├───OK─── https://fsfe.org/news/nl/nl-201305.sq.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.nl.html (HTTP_404)
Finished! 107 links found. 103 excluded. 1 broken.
