Getting links from: https://fsfe.org/news/nl/nl-201105.ro.html
├───OK─── https://fsfe.org/activities/swpat/letter-20101222.ro.html
├───OK─── https://fsfe.org/activities/swpat/letter-20110406.ro.html
├───OK─── https://fsfe.org/activities/swpat/novell-cptn.ro.html
├─BROKEN─ https://fsfe.org/join.ro.html (HTTP_404)
├───OK─── https://fsfe.org/activities/pdfreaders/follow-up.ro.html
├───OK─── https://fsfe.org/about/tuke/tuke.ro.html
Finished! 103 links found. 97 excluded. 1 broken.
