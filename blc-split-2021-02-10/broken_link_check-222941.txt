Getting links from: https://fsfe.org/contribute/spreadtheword.fr.html#pmpc-brochure
├───OK─── https://fsfe.org/about/graphics/index.fr.html
├───OK─── https://fsfe.org/activities/publiccode/brochure.fr.html
├─BROKEN─ https://fsfe.org/contribute/activities/ilovefs/index.fr.html (HTTP_404)
Finished! 275 links found. 272 excluded. 1 broken.
