Getting links from: https://fsfe.org/news/nl/nl-201112.de.html
├───OK─── https://fsfe.org/news/2011/news-20111128-01.de.html
├───OK─── https://fsfe.org/activities/pdfreaders/buglist.de.html
├───OK─── https://fsfe.org/activities/pdfreaders/letter.de.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.de.html (HTTP_404)
Finished! 108 links found. 104 excluded. 1 broken.
