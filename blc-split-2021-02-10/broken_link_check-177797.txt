Getting links from: https://fsfe.org/news/nl/nl-201501.nb.html
├───OK─── https://fsfe.org/fellowship/index.nb.html
├───OK─── https://fsfe.org/fellowship/about/fellowship.nb.html#youget
├─BROKEN─ https://fsfe.org/about/legal/constitution.nb.html#id-fellowship-seats (HTTP_404)
├───OK─── https://fsfe.org/news/2014/news-20141219-01.nb.html
├───OK─── https://fsfe.org/news/2014/news-20141212-01.nb.html
├─BROKEN─ https://fsfe.org/about/ojasild (HTTP_404)
Finished! 88 links found. 82 excluded. 2 broken.
