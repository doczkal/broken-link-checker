Getting links from: https://fsfe.org/freesoftware/education/edu-related-content.fi.html
├───OK─── https://fsfe.org/activities/mankind/lsm2002/index.fi.html
├─BROKEN─ https://fsfe.org/associates/ofset/associate (HTTP_404)
├───OK─── https://fsfe.org/activities/mankind/lsm2002/press-release.fi.html
├───OK─── https://fsfe.org/activities/policy.fi.html
├───OK─── https://fsfe.org/news/2001/article2001-12-17-01.fi.html
├───OK─── https://fsfe.org/freesoftware/transcripts/rms-fs-2006-03-09.fi.html
├─BROKEN─ https://fsfe.org/freesoftware/education/news/2012/news-20121217-01 (HTTP_404)
├───OK─── https://fsfe.org/news/2003/lettera_MIUR-2003-07-16.fi.html
├───OK─── https://fsfe.org/activities/tgs/tgs.fi.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool1.fi.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool2.fi.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool3.fi.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool4.fi.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool5.fi.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool6.fi.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool7.fi.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool8.fi.html
Finished! 112 links found. 95 excluded. 2 broken.
