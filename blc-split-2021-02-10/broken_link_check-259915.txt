Getting links from: https://fsfe.org/news/nl/nl-201302.it.html
├───OK─── https://fsfe.org/news/2012/news-20121217-01.it.html
├───OK─── https://fsfe.org/activities/ilovefs/2013/index.it.html
├───OK─── https://fsfe.org/activities/ilovefs/2013/whylovefs.it.html
├─BROKEN─ https://fsfe.org/activities/ilovefs/2013/banners.it.html (HTTP_404)
├─BROKEN─ https://fsfe.org/order/promoorder.it.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.it.html (HTTP_404)
Finished! 119 links found. 113 excluded. 3 broken.
