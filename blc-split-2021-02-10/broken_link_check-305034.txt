Getting links from: https://fsfe.org/news/nl/nl-201502.es.html
├───OK─── https://fsfe.org/news/nl/nl-201502.fr.html
├─BROKEN─ https://fsfe.org/about/ojasild/ojasild.es.html (HTTP_404)
├───OK─── https://fsfe.org/about/people/gerloff/gerloff.es.html
├─BROKEN─ https://fsfe.org/about/ojasild (HTTP_404)
Finished! 96 links found. 92 excluded. 2 broken.
