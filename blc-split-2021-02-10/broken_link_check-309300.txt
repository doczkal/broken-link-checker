Getting links from: https://fsfe.org/news/nl/nl-201306.el.html
├───OK─── https://fsfe.org/news/nl/nl-201306.en.html
├───OK─── https://fsfe.org/events/2013/linuxtag-2013.el.html
├───OK─── https://fsfe.org/freesoftware/legal/flashingdevices.el.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.el.html (HTTP_404)
Finished! 117 links found. 113 excluded. 1 broken.
