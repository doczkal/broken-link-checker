Getting links from: https://fsfe.org/news/nl/nl-201310.bg.html
├───OK─── https://fsfe.org/freesoftware/basics/gnuproject.bg.html
├───OK─── https://fsfe.org/news/2013/news-20130926-01.bg.html
├───OK─── https://fsfe.org/news/2013/news-20130918-01.bg.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.bg.html (HTTP_404)
Finished! 120 links found. 116 excluded. 1 broken.
