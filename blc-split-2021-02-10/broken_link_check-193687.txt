Getting links from: https://fsfe.org/news/nl/nl-201302.sr.html
├───OK─── https://fsfe.org/news/nl/nl-201302.sq.html
├───OK─── https://fsfe.org/activities/ilovefs/2013/index.sr.html
├───OK─── https://fsfe.org/activities/ilovefs/2013/whylovefs.sr.html
├─BROKEN─ https://fsfe.org/activities/ilovefs/2013/banners.sr.html (HTTP_404)
├─BROKEN─ https://fsfe.org/order/promoorder.sr.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.sr.html (HTTP_404)
Finished! 119 links found. 113 excluded. 3 broken.
