Getting links from: https://fsfe.org/news/nl/nl-201308.pt.html
├─BROKEN─ https://fsfe.org/news/2013/2013-07-26_Open_Letter_to_NEC.pt.html (HTTP_404)
├───OK─── https://fsfe.org/news/2013/news-20130712-01.pt.html
├───OK─── https://fsfe.org/freesoftware/standards/transparency-letter.pt.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.pt.html (HTTP_404)
Finished! 113 links found. 109 excluded. 2 broken.
