Getting links from: https://fsfe.org/news/nl/nl-201502.ru.html
├───OK─── https://fsfe.org/news/nl/nl-201502.de.html
├───OK─── https://fsfe.org/news/nl/nl-201502.it.html
├───OK─── https://fsfe.org/news/nl/nl-201502.nl.html
├───OK─── https://fsfe.org/news/nl/nl-201502.sq.html
├─BROKEN─ https://fsfe.org/about/ojasild/ojasild.ru.html (HTTP_404)
├─BROKEN─ https://fsfe.org/about/ojasild (HTTP_404)
Finished! 96 links found. 90 excluded. 2 broken.
