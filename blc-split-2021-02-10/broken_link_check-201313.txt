Getting links from: https://fsfe.org/news/2018/news-20180215-01.el.html
├───OK─── https://fsfe.org/news/2017/news-20170214-02.el.html
├───OK─── https://fsfe.org/news/2017/news-20171130-01.el.html
├─BROKEN─ https://fsfe.org/freesoftware/standards/standards.html%3E (HTTP_404)
Finished! 73 links found. 70 excluded. 1 broken.
