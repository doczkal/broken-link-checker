Getting links from: https://fsfe.org/news/2012/report-2012.it.html
├─BROKEN─ https://fsfe.org/news/2012/blogs.fsfe.org/samtuke/%3Fp%3D255 (HTTP_404)
├───OK─── https://fsfe.org/news/2012/news-20120425-02.it.html
├───OK─── https://fsfe.org/news/2012/news-20120619-01.it.html
├───OK─── https://fsfe.org/news/2012/news-20121112-01.it.html
├───OK─── https://fsfe.org/donate/thankgnus.it.html
Finished! 93 links found. 88 excluded. 1 broken.
