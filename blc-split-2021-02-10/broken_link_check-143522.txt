Getting links from: https://fsfe.org/about/people/testimonials.sq.html#guenther
├───OK─── https://fsfe.org/about/people/interviews/cryptie.sq.html#video-30s-cryptie
├───OK─── https://fsfe.org/about/people/interviews/zerolo.sq.html#video-30s-tomas
├─BROKEN─ https://fsfe.org/about/people/interviews/bernhard.sq.html#video-30s-bernhard (HTTP_404)
├───OK─── https://fsfe.org/about/people/interviews/weitzhofer.sq.html#interview
├───OK─── https://fsfe.org/about/people/interviews/ockers.sq.html#video-30s-andre
├───OK─── https://fsfe.org/about/people/interviews/mueller.sq.html#interview
├───OK─── https://fsfe.org/about/people/interviews/snow.sq.html#interview
├───OK─── https://fsfe.org/about/people/interviews/gkotsopoulou.sq.html#interview
Finished! 122 links found. 114 excluded. 1 broken.
