Getting links from: https://fsfe.org/news/nl/nl-201302.bs.html
├───OK─── https://fsfe.org/activities/ilovefs/2013/index.bs.html
├───OK─── https://fsfe.org/activities/ilovefs/2013/whylovefs.bs.html
├─BROKEN─ https://fsfe.org/activities/ilovefs/2013/banners.bs.html (HTTP_404)
├─BROKEN─ https://fsfe.org/order/promoorder.bs.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.bs.html (HTTP_404)
Finished! 119 links found. 114 excluded. 3 broken.
