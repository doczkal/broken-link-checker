Getting links from: https://fsfe.org/news/nl/nl-201112.uk.html
├───OK─── https://fsfe.org/news/2011/news-20111128-01.uk.html
├───OK─── https://fsfe.org/activities/pdfreaders/buglist.uk.html
├───OK─── https://fsfe.org/activities/pdfreaders/letter.uk.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.uk.html (HTTP_404)
Finished! 108 links found. 104 excluded. 1 broken.
