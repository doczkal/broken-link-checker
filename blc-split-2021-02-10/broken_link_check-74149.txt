Getting links from: https://fsfe.org/about/funds/2004.nb.html
├───OK─── https://fsfe.org/about/funds/2004.fr.html
├───OK─── https://fsfe.org/about/funds/2004.it.html
├───OK─── https://fsfe.org/about/funds/2004.nl.html
├───OK─── https://fsfe.org/help/thankgnus-2004.nb.html
├─BROKEN─ https://fsfe.org/about/legal/de/de.nb.html (HTTP_404)
Finished! 87 links found. 82 excluded. 1 broken.
