Getting links from: https://fsfe.org/news/nl/nl-201305.it.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.it.html (HTTP_404)
├───OK─── https://fsfe.org/news/news.it.rss
├───OK─── https://fsfe.org/events/events.it.rss
├───OK─── https://fsfe.org/contact/community.it.html
├───OK─── https://fsfe.org/tags/tagged-newsletter.it.html
Finished! 104 links found. 99 excluded. 1 broken.
