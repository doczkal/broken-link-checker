Getting links from: https://fsfe.org/news/nl/nl-201110.mk.html
├───OK─── https://fsfe.org/news/nl/nl-201110.el.html
├───OK─── https://fsfe.org/news/nl/nl-201110.fr.html
├───OK─── https://fsfe.org/news/nl/nl-201110.it.html
├───OK─── https://fsfe.org/freesoftware/education/eduteam.mk.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.mk.html (HTTP_404)
Finished! 101 links found. 96 excluded. 1 broken.
