Getting links from: https://fsfe.org/news/nl/nl-201309.es.html
├───OK─── https://fsfe.org/news/nl/nl-201309.el.html
├───OK─── https://fsfe.org/news/nl/nl-201309.fr.html
├───OK─── https://fsfe.org/activities/ms-vs-eu/ms-vs-eu.es.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.es.html (HTTP_404)
Finished! 118 links found. 114 excluded. 1 broken.
