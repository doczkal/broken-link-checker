Getting links from: https://fsfe.org/about/people/mehl/mehl.ar.html
├───OK─── https://fsfe.org/activities/radiodirective/index.ar.html
├─BROKEN─ https://fsfe.org/de (HTTP_404)
├───OK─── https://fsfe.org/activities/routers/index.ar.html
├───OK─── https://fsfe.org/activities/android/index.ar.html
├───OK─── https://fsfe.org/activities/drm/index.ar.html
├───OK─── https://fsfe.org/contribute/web/index.ar.html
├─BROKEN─ https://fsfe.org/de (HTTP_404)
├─BROKEN─ https://fsfe.org/de (HTTP_404)
Finished! 83 links found. 75 excluded. 3 broken.
