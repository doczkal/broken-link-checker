Getting links from: https://fsfe.org/news/nl/nl-201310.el.html
├───OK─── https://fsfe.org/freesoftware/basics/gnuproject.el.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.el.html (HTTP_404)
Finished! 118 links found. 116 excluded. 1 broken.
