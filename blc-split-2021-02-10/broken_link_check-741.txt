Getting links from: https://fsfe.org/news/nl/nl-201309.en.html
├───OK─── https://fsfe.org/activities/android/promomaterial/fdroid-folder.pdf
├───OK─── https://fsfe.org/activities/msooxml/msooxml-interoperability.en.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.en.html (HTTP_404)
Finished! 114 links found. 111 excluded. 1 broken.
