Getting links from: https://fsfe.org/news/nl/nl-201302.hu.html#
├───OK─── https://fsfe.org/activities/ilovefs/2013/index.hu.html
├───OK─── https://fsfe.org/activities/ilovefs/2013/whylovefs.hu.html
├─BROKEN─ https://fsfe.org/activities/ilovefs/2013/banners.hu.html (HTTP_404)
├─BROKEN─ https://fsfe.org/order/promoorder.hu.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.hu.html (HTTP_404)
Finished! 119 links found. 114 excluded. 3 broken.
