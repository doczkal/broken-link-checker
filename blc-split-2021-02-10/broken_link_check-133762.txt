Getting links from: https://fsfe.org/news/nl/nl-201105.ar.html
├───OK─── https://fsfe.org/activities/swpat/letter-20101222.ar.html
├───OK─── https://fsfe.org/activities/swpat/letter-20110406.ar.html
├───OK─── https://fsfe.org/activities/swpat/novell-cptn.ar.html
├─BROKEN─ https://fsfe.org/join.ar.html (HTTP_404)
├───OK─── https://fsfe.org/activities/pdfreaders/follow-up.ar.html
├───OK─── https://fsfe.org/about/tuke/tuke.ar.html
Finished! 104 links found. 98 excluded. 1 broken.
