Getting links from: https://fsfe.org/activities/gplv3/europe-gplv3-conference.ar.html
├─BROKEN─ https://fsfe.org/activities/gplv3/GPLv3-logo-red.png (HTTP_404)
├───OK─── https://fsfe.org/about/oriordan/oriordan.ar.html
├───OK─── https://fsfe.org/about/maffulli/maffulli.ar.html
├───OK─── https://fsfe.org/activities/gplv3/barcelona-rms-transcript.ar.html
├───OK─── https://fsfe.org/activities/gplv3/barcelona-moglen-transcript.ar.html
Finished! 104 links found. 99 excluded. 1 broken.
