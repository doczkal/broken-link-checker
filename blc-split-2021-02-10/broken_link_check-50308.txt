Getting links from: https://fsfe.org/news/2008/news-20081208-01.de.html
├───OK─── https://fsfe.org/news/2008/news-20081208-01.el.html
├───OK─── https://fsfe.org/news/2008/news-20081208-01.en.html
├───OK─── https://fsfe.org/news/2008/news-20081208-01.nl.html
├───OK─── https://fsfe.org/activities/ftf/reporting-fixing-violations.de.html
├─BROKEN─ https://fsfe.org/ftf/index.de.html (HTTP_404)
├─BROKEN─ https://fsfe.org/ftf (HTTP_404)
Finished! 62 links found. 56 excluded. 2 broken.
