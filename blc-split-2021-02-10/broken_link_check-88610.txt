Getting links from: https://fsfe.org/news/nl/nl-201212.it.html#
├───OK─── https://fsfe.org/news/2012/news-20121116-01.it.html
├─BROKEN─ https://fsfe.org/news/nl/lwn.net/Articles/523537/index.it.html (HTTP_404)
├───OK─── https://fsfe.org/events/index.it.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.it.html (HTTP_404)
Finished! 115 links found. 111 excluded. 2 broken.
