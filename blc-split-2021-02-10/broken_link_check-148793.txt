Getting links from: https://fsfe.org/news/nl/nl-201810.ca.html
├───OK─── https://fsfe.org/news/2018/news-20181023-02.ca.html
├─BROKEN─ https://fsfe.org/about/mancheva/mancheva (HTTP_404)
├───OK─── https://fsfe.org/news/2018/news-20181010-01.ca.html
├───OK─── https://fsfe.org/news/2018/news-20180913-01.ca.html
├───OK─── https://fsfe.org/contribute/internship.ca.html
├───OK─── https://fsfe.org/tags/tagged-microsoft.ca.html
├───OK─── https://fsfe.org/tags/tagged-swpat.ca.html
├───OK─── https://fsfe.org/tags/tagged-digital-o-mat.ca.html
Finished! 93 links found. 85 excluded. 1 broken.
