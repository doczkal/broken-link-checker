Getting links from: https://fsfe.org/about/people/roy/index.it.html
├───OK─── https://fsfe.org/activities/ftf/ftf.it.html
├─BROKEN─ https://fsfe.org/fr/index.it.html (HTTP_404)
├─BROKEN─ https://fsfe.org/legal (HTTP_404)
Finished! 80 links found. 77 excluded. 2 broken.
