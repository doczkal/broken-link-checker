Getting links from: https://fsfe.org/news/nl/nl-201310.hu.html
├───OK─── https://fsfe.org/freesoftware/basics/gnuproject.hu.html
├───OK─── https://fsfe.org/news/2013/news-20130923-01.hu.html
├───OK─── https://fsfe.org/news/2013/news-20130926-01.hu.html
├───OK─── https://fsfe.org/news/2013/news-20130918-01.hu.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.hu.html (HTTP_404)
Finished! 120 links found. 115 excluded. 1 broken.
