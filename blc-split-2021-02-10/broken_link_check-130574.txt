Getting links from: https://fsfe.org/news/nl/nl-201106.it.html
├───OK─── https://fsfe.org/freesoftware/standards/bt-open-letter.it.html
├─BROKEN─ https://fsfe.org/source/activities/elections/askyourcandidates/askyourcandidates.xhtml (HTTP_404)
Finished! 90 links found. 88 excluded. 1 broken.
