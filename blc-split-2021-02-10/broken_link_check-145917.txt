Getting links from: https://fsfe.org/news/nl/nl-201501.sl.html
├───OK─── https://fsfe.org/news/nl/nl-201501.de.html
├───OK─── https://fsfe.org/news/nl/nl-201501.it.html
├───OK─── https://fsfe.org/news/nl/nl-201501.nl.html
├───OK─── https://fsfe.org/news/2014/news-20141218-02.sl.html
├───OK─── https://fsfe.org/fellowship/index.sl.html
├───OK─── https://fsfe.org/fellowship/about/fellowship.sl.html#youget
├─BROKEN─ https://fsfe.org/about/legal/constitution.sl.html#id-fellowship-seats (HTTP_404)
├───OK─── https://fsfe.org/news/2014/news-20141219-01.sl.html
├───OK─── https://fsfe.org/news/2014/news-20141212-01.sl.html
├─BROKEN─ https://fsfe.org/about/ojasild (HTTP_404)
Finished! 88 links found. 78 excluded. 2 broken.
