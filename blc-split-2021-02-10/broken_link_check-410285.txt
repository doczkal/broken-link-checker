Getting links from: https://fsfe.org/news/2019/news-20191022-01.ar.html
├─BROKEN─ https://fsfe.org/news/2019/about/codeofconduct (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/contribute/web/web.ar.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/news-20191002.ar.html (HTTP_404)
├───OK─── https://fsfe.org/news/2019/news-20190701-01.ar.html
Finished! 223 links found. 219 excluded. 3 broken.
