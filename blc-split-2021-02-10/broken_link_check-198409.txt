Getting links from: https://fsfe.org/news/nl/nl-201105.sk.html
├───OK─── https://fsfe.org/activities/swpat/letter-20101222.sk.html
├───OK─── https://fsfe.org/activities/swpat/letter-20110406.sk.html
├───OK─── https://fsfe.org/activities/swpat/novell-cptn.sk.html
├─BROKEN─ https://fsfe.org/join.sk.html (HTTP_404)
├───OK─── https://fsfe.org/activities/pdfreaders/follow-up.sk.html
├───OK─── https://fsfe.org/about/tuke/tuke.sk.html
Finished! 103 links found. 97 excluded. 1 broken.
