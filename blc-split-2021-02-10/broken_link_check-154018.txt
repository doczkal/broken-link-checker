Getting links from: https://fsfe.org/news/nl/nl-201202.it.html
├───OK─── https://fsfe.org/news/2012/news-20120131-01.it.html
├───OK─── https://fsfe.org/news/2012/news-20120110-01.it.html
├─BROKEN─ https://fsfe.org/activities/ilovefs/2012/banners.it.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.it.html (HTTP_404)
Finished! 107 links found. 103 excluded. 2 broken.
