Getting links from: https://fsfe.org/contribute/translators/index.nb.html
├───OK─── https://fsfe.org/contribute/index.nb.html
├───OK─── https://fsfe.org/events/events.nb.html
├───OK─── https://fsfe.org/news/newsletter.nb.html
├───OK─── https://fsfe.org/contribute/spreadtheword.nb.html
├───OK─── https://fsfe.org/contribute/translators/wordlist.nb.html
├─BROKEN─ https://fsfe.org/contribute/translators/web.nb.html (HTTP_404)
Finished! 91 links found. 85 excluded. 1 broken.
