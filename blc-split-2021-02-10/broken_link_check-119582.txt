Getting links from: https://fsfe.org/news/nl/nl-201501.sl.html
├───OK─── https://fsfe.org/fellowship/index.sl.html
├───OK─── https://fsfe.org/fellowship/about/fellowship.sl.html#youget
├─BROKEN─ https://fsfe.org/about/legal/constitution.sl.html#id-fellowship-seats (HTTP_404)
├─BROKEN─ https://fsfe.org/about/ojasild (HTTP_404)
Finished! 88 links found. 84 excluded. 2 broken.
