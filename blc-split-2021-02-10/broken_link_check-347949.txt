Getting links from: https://fsfe.org/news/2009/news-20090620-01.sq.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.sq.html (HTTP_404)
├─BROKEN─ https://fsfe.org/about/holz/holz.sq.html (HTTP_404)
├───OK─── https://fsfe.org/activities/ms-vs-eu/ms-vs-eu.sq.html
├─BROKEN─ https://fsfe.org/activities/self/SELF-LegalPolicy-0.9_1.pdf (HTTP_404)
├───OK─── https://fsfe.org/activities/ftf/fiduciary.sq.html
├───OK─── https://fsfe.org/activities/igf/igf.sq.html
├───OK─── https://fsfe.org/activities/stacs/stacs.sq.html
├───OK─── https://fsfe.org/activities/stacs/london.sq.html
├───OK─── https://fsfe.org/activities/stacs/belgrade.sq.html
Finished! 127 links found. 118 excluded. 3 broken.
