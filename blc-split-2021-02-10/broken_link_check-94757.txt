Getting links from: https://fsfe.org/news/2019/news-20191022-01.mk.html
├───OK─── https://fsfe.org/news/2019/news-20191022-01.de.html
├───OK─── https://fsfe.org/news/2019/news-20190326-01
├─BROKEN─ https://fsfe.org/news/2019/about/codeofconduct (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/contribute/web/web.mk.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/news-20191002.mk.html (HTTP_404)
├───OK─── https://fsfe.org/news/2019/news-20190701-01.mk.html
Finished! 222 links found. 216 excluded. 3 broken.
