Getting links from: https://fsfe.org/news/nl/nl-201704.tr.html
├───OK─── https://fsfe.org/news/2017/news-20170328-01.tr.html
├───OK─── https://fsfe.org/activities/radiodirective/index.tr.html
├───OK─── https://fsfe.org/news/2017/news-20170321-01.tr.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.tr.html#id-fellowship-seats (HTTP_404)
Finished! 103 links found. 99 excluded. 1 broken.
