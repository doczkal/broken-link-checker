Getting links from: https://fsfe.org/news/nl/nl-201208.it.html
├───OK─── https://fsfe.org/activities/elections/askyourcandidates/askyourcandidates.it.html
├───OK─── https://fsfe.org/contribute/internship.it.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.it.html (HTTP_404)
Finished! 96 links found. 93 excluded. 1 broken.
