Getting links from: https://fsfe.org/about/people/testimonials.da.html#kroah-hartman
├───OK─── https://fsfe.org/about/people/interviews/cryptie.da.html#video-30s-cryptie
├───OK─── https://fsfe.org/about/people/interviews/lequertier.da.html#video-30s-vincent
├───OK─── https://fsfe.org/about/people/interviews/zerolo.da.html#video-30s-tomas
├─BROKEN─ https://fsfe.org/about/people/interviews/bernhard.da.html#video-30s-bernhard (HTTP_404)
├───OK─── https://fsfe.org/about/people/interviews/weitzhofer.da.html#interview
├───OK─── https://fsfe.org/about/people/interviews/grun.da.html#interview
├───OK─── https://fsfe.org/about/people/interviews/ockers.da.html#video-30s-andre
├───OK─── https://fsfe.org/about/people/interviews/mueller.da.html#interview
├───OK─── https://fsfe.org/about/people/interviews/snow.da.html#interview
├───OK─── https://fsfe.org/about/people/interviews/gkotsopoulou.da.html#interview
Finished! 122 links found. 112 excluded. 1 broken.
