Getting links from: https://fsfe.org/news/nl/nl-201704.mk.html
├───OK─── https://fsfe.org/news/nl/nl-201704.de.html
├───OK─── https://fsfe.org/activities/ilovefs/whylovefs/gallery.mk.html
├───OK─── https://fsfe.org/news/2017/news-20170321-01.mk.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.mk.html#id-fellowship-seats (HTTP_404)
Finished! 103 links found. 99 excluded. 1 broken.
