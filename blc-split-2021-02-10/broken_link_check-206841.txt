Getting links from: https://fsfe.org/news/nl/nl-201205.nl.html
├─BROKEN─ https://fsfe.org/activities/swpat/swapt.nl.html (HTTP_404)
├─BROKEN─ https://fsfe.org/project/os/def.nl.html (HTTP_404)
├───OK─── https://fsfe.org/news/2012/news-20120425-02.nl.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.nl.html (HTTP_404)
Finished! 114 links found. 110 excluded. 3 broken.
