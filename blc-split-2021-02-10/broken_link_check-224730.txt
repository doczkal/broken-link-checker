Getting links from: https://fsfe.org/contribute/web/tagging.ro.html
├───OK─── https://fsfe.org/contribute/web/tagging.en.html
├───OK─── https://fsfe.org/contribute/web/index.ro.html
├─BROKEN─ https://fsfe.org/uk/uk.ro.html (HTTP_404)
├───OK─── https://fsfe.org/tags/tagged.ro.html
├───OK─── https://fsfe.org/contribute/web/css.ro.html
├───OK─── https://fsfe.org/contribute/template.ro.html
Finished! 53 links found. 47 excluded. 1 broken.
