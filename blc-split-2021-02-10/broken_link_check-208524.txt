Getting links from: https://fsfe.org/news/nl/nl-201212.pt.html
├───OK─── https://fsfe.org/news/2012/news-20121116-01.pt.html
├─BROKEN─ https://fsfe.org/news/nl/lwn.net/Articles/523537/index.pt.html (HTTP_404)
├───OK─── https://fsfe.org/news/2012/news-20121112-01.pt.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.pt.html (HTTP_404)
Finished! 116 links found. 112 excluded. 2 broken.
