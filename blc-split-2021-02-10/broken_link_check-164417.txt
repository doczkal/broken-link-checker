Getting links from: https://fsfe.org/news/nl/nl-201810.sk.html
├───OK─── https://fsfe.org/news/nl/nl-201810.nl.html
├───OK─── https://fsfe.org/news/2018/news-20181023-02.sk.html
├─BROKEN─ https://fsfe.org/about/mancheva/mancheva (HTTP_404)
├───OK─── https://fsfe.org/about/people/ku/ku
├───OK─── https://fsfe.org/news/2018/news-20181010-01.sk.html
├───OK─── https://fsfe.org/tags/tagged-sfscon.sk.html
Finished! 93 links found. 87 excluded. 1 broken.
