Getting links from: https://fsfe.org/news/nl/nl-201208.es.html
├───OK─── https://fsfe.org/news/nl/nl-201208.el.html
├───OK─── https://fsfe.org/activities/elections/askyourcandidates/askyourcandidates.es.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.es.html (HTTP_404)
Finished! 99 links found. 96 excluded. 1 broken.
