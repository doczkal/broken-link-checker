Getting links from: https://fsfe.org/news/2019/news-20191022-01.sr.html
├───OK─── https://fsfe.org/activities/radiodirective/statement.sr.html
├───OK─── https://fsfe.org/about/people/interviews/grun.sr.html#interview
├─BROKEN─ https://fsfe.org/news/2019/about/codeofconduct (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/contribute/web/web.sr.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/news-20191002.sr.html (HTTP_404)
├───OK─── https://fsfe.org/news/2019/news-20190701-01.sr.html
Finished! 222 links found. 216 excluded. 3 broken.
