Getting links from: https://fsfe.org/news/2019/news-20191022-01.sl.html
├───OK─── https://fsfe.org/activities/routers/index.sl.html
├───OK─── https://fsfe.org/activities/routers/timeline.sl.html
├───OK─── https://fsfe.org/news/2019/news-20190514-01.sl.html
├───OK─── https://fsfe.org/news/2019/news-20190515-01.sl.html
├───OK─── https://fsfe.org/news/2019/news-20190515-02.sl.html
├───OK─── https://fsfe.org/news/2019/news-20190326-01
├───OK─── https://fsfe.org/activities/radiodirective/radiodirective.sl.html
├───OK─── https://fsfe.org/news/2019/news-20190827-01.sl.html
├───OK─── https://fsfe.org/activities/radiodirective/statement.sl.html
├───OK─── https://fsfe.org/news/2019/news-20190807-01.sl.html
├─BROKEN─ https://fsfe.org/news/2019/about/codeofconduct (HTTP_404)
├───OK─── https://fsfe.org/order/index.sl.html
├─BROKEN─ https://fsfe.org/news/2019/contribute/web/web.sl.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/news-20191002.sl.html (HTTP_404)
├───OK─── https://fsfe.org/activities/ln/llw.sl.html
├───OK─── https://fsfe.org/news/2019/news-20191014-01.sl.html
├───OK─── https://fsfe.org/news/2019/news-20190927-01.sl.html
Finished! 222 links found. 205 excluded. 3 broken.
