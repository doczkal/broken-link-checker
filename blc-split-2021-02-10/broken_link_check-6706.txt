Getting links from: https://fsfe.org/news/nl/nl-201810.pt.html
├─BROKEN─ https://fsfe.org/about/mancheva/mancheva (HTTP_404)
├───OK─── https://fsfe.org/news/2018/news-20181010-01.pt.html
├───OK─── https://fsfe.org/news/2018/news-20180913-01.pt.html
├───OK─── https://fsfe.org/tags/tagged-microsoft.pt.html
├───OK─── https://fsfe.org/tags/tagged-digital-o-mat.pt.html
Finished! 94 links found. 89 excluded. 1 broken.
