Getting links from: https://fsfe.org/news/2007/news-20070630-01.es.html#
├───OK─── https://fsfe.org/activities/ipred2/index.es.html
├───OK─── https://fsfe.org/activities/wipo/index.es.html
├─BROKEN─ https://fsfe.org/ftf (HTTP_404)
├───OK─── https://fsfe.org/activities/gplv3/index.es.html#transcripts
├───OK─── https://fsfe.org/activities/self/index.es.html
├─BROKEN─ https://fsfe.org/fellows/greve/img/zrh_hoist (HTTP_404)
├─BROKEN─ https://fsfe.org/fellows/meetings/2006_bolzano (HTTP_404)
Finished! 105 links found. 98 excluded. 3 broken.
