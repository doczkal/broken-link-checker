Getting links from: https://fsfe.org/news/nl/nl-201105.pl.html
├───OK─── https://fsfe.org/news/nl/nl-201105.el.html
├───OK─── https://fsfe.org/news/nl/nl-201105.it.html
├───OK─── https://fsfe.org/activities/swpat/letter-20101222.pl.html
├───OK─── https://fsfe.org/activities/swpat/letter-20110406.pl.html
├───OK─── https://fsfe.org/activities/swpat/novell-cptn.pl.html
├─BROKEN─ https://fsfe.org/join.pl.html (HTTP_404)
├───OK─── https://fsfe.org/activities/pdfreaders/follow-up.pl.html
├───OK─── https://fsfe.org/about/tuke/tuke.pl.html
Finished! 103 links found. 95 excluded. 1 broken.
