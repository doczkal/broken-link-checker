Getting links from: https://fsfe.org/freesoftware/education/edu-related-content.hu.html
├───OK─── https://fsfe.org/activities/mankind/lsm2002/index.hu.html
├─BROKEN─ https://fsfe.org/associates/ofset/associate (HTTP_404)
├───OK─── https://fsfe.org/activities/mankind/lsm2002/press-release.hu.html
├───OK─── https://fsfe.org/news/2001/article2001-12-17-01.hu.html
├───OK─── https://fsfe.org/freesoftware/transcripts/rms-fs-2006-03-09.hu.html
├───OK─── https://fsfe.org/news/2013/news-20130620-01
├─BROKEN─ https://fsfe.org/freesoftware/education/news/2012/news-20121217-01 (HTTP_404)
├───OK─── https://fsfe.org/news/2003/lettera_MIUR-2003-07-16.hu.html
├───OK─── https://fsfe.org/activities/tgs/tgs.hu.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool1.hu.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool2.hu.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool3.hu.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool4.hu.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool5.hu.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool6.hu.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool7.hu.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool8.hu.html
Finished! 111 links found. 94 excluded. 2 broken.
