Getting links from: https://fsfe.org/contribute/web/tagging.sq.html
├───OK─── https://fsfe.org/contribute/web/index.sq.html
├─BROKEN─ https://fsfe.org/uk/uk.sq.html (HTTP_404)
├───OK─── https://fsfe.org/contribute/web/css.sq.html
├───OK─── https://fsfe.org/contribute/template.sq.html
Finished! 53 links found. 49 excluded. 1 broken.
