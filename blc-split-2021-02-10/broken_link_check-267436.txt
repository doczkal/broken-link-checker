Getting links from: https://fsfe.org/activities/swpat/index.de.html
├─BROKEN─ https://fsfe.org/activities/swpat/documents/iprip.de.html (HTTP_404)
├───OK─── https://fsfe.org/activities/swpat/fsfe-patstrat-response.de.html
├───OK─── https://fsfe.org/activities/swpat/status.de.html
Finished! 86 links found. 83 excluded. 1 broken.
