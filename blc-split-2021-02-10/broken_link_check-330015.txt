Getting links from: https://fsfe.org/news/nl/nl-201810.mk.html
├─BROKEN─ https://fsfe.org/about/mancheva/mancheva (HTTP_404)
├───OK─── https://fsfe.org/news/2018/news-20181010-01.mk.html
├───OK─── https://fsfe.org/news/2018/news-20180913-01.mk.html
├───OK─── https://fsfe.org/tags/tagged-microsoft.mk.html
├───OK─── https://fsfe.org/tags/tagged-digital-o-mat.mk.html
Finished! 93 links found. 88 excluded. 1 broken.
