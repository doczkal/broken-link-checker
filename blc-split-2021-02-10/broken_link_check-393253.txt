Getting links from: https://fsfe.org/contribute/translators/index.sv.html
├───OK─── https://fsfe.org/contribute/index.sv.html
├───OK─── https://fsfe.org/news/index.sv.html
├───OK─── https://fsfe.org/events/index.sv.html
├─BROKEN─ https://fsfe.org/news/newletter.sv.html (HTTP_404)
├───OK─── https://fsfe.org/contribute/spreadtheword.sv.html#promo-material
├───OK─── https://fsfe.org/about/index.sv.html
Finished! 91 links found. 85 excluded. 1 broken.
