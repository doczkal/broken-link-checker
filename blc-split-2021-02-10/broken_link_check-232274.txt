Getting links from: https://fsfe.org/news/2008/news-20081210-01.da.html
├───OK─── https://fsfe.org/news/2008/news-20081210-01.el.html
├───OK─── https://fsfe.org/news/2008/news-20081210-01.fr.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.da.html (HTTP_404)
├───OK─── https://fsfe.org/freesoftware/freesoftware.da.html
Finished! 68 links found. 64 excluded. 1 broken.
