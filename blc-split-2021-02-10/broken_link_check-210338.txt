Getting links from: https://fsfe.org/news/nl/nl-201205.ru.html
├─BROKEN─ https://fsfe.org/activities/swpat/swapt.ru.html (HTTP_404)
├───OK─── https://fsfe.org/news/2012/news-20120402-01.ru.html
├─BROKEN─ https://fsfe.org/project/os/def.ru.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.ru.html (HTTP_404)
Finished! 115 links found. 111 excluded. 3 broken.
