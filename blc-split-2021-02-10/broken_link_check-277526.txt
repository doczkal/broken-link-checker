Getting links from: https://fsfe.org/news/nl/nl-201210.cs.html
├───OK─── https://fsfe.org/freesoftware/secure-boot.cs.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.cs.html (HTTP_404)
├───OK─── https://fsfe.org/events/events.cs.rss
Finished! 121 links found. 117 excluded. 2 broken.
