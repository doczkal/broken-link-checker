Getting links from: https://fsfe.org/news/nl/nl-201302.mk.html
├───OK─── https://fsfe.org/activities/ilovefs/2013/index.mk.html
├───OK─── https://fsfe.org/activities/ilovefs/2013/whylovefs.mk.html
├─BROKEN─ https://fsfe.org/activities/ilovefs/2013/banners.mk.html (HTTP_404)
├─BROKEN─ https://fsfe.org/order/promoorder.mk.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.mk.html (HTTP_404)
Finished! 119 links found. 114 excluded. 3 broken.
