Getting links from: https://fsfe.org/news/nl/nl-201305.fi.html
├───OK─── https://fsfe.org/news/nl/nl-201305.de.html
├───OK─── https://fsfe.org/news/nl/nl-201305.it.html
├───OK─── https://fsfe.org/news/nl/nl-201305.sq.html
├───OK─── https://fsfe.org/news/2013/news-20130424-01.fi.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.fi.html (HTTP_404)
Finished! 108 links found. 103 excluded. 1 broken.
