Getting links from: https://fsfe.org/news/nl/nl-201810.sr.html
├───OK─── https://fsfe.org/about/people/albers/albers.sr.html
├─BROKEN─ https://fsfe.org/about/mancheva/mancheva (HTTP_404)
├───OK─── https://fsfe.org/tags/tagged-microsoft.sr.html
Finished! 93 links found. 90 excluded. 1 broken.
