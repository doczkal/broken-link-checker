Getting links from: https://fsfe.org/contact/contact.nn.html
├───OK─── https://fsfe.org/about/index.nn.html
├───OK─── https://fsfe.org/about/people/kirschner/index.nn.html
├─BROKEN─ https://fsfe.org/about/codeofconduct/index.nn.html (HTTP_404)
Finished! 86 links found. 83 excluded. 1 broken.
