Getting links from: https://fsfe.org/news/nl/nl-201105.uk.html
├───OK─── https://fsfe.org/activities/swpat/letter-20101222.uk.html
├───OK─── https://fsfe.org/activities/swpat/letter-20110406.uk.html
├───OK─── https://fsfe.org/activities/swpat/novell-cptn.uk.html
├─BROKEN─ https://fsfe.org/join.uk.html (HTTP_404)
├───OK─── https://fsfe.org/activities/pdfreaders/follow-up.uk.html
├───OK─── https://fsfe.org/about/tuke/tuke.uk.html
Finished! 103 links found. 97 excluded. 1 broken.
