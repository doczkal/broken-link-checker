Getting links from: https://fsfe.org/news/2019/news-20191022-01.fr.html
├───OK─── https://fsfe.org/activities/routers/timeline.fr.html
├─BROKEN─ https://fsfe.org/news/2019/about/codeofconduct (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/contribute/web/web.fr.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/news-20191002.fr.html (HTTP_404)
├───OK─── https://fsfe.org/news/2019/news-20190701-01.fr.html
├───OK─── https://fsfe.org/tags/tagged-annual-report.fr.html
├───OK─── https://fsfe.org/tags/tagged-ilovefs.fr.html
Finished! 222 links found. 215 excluded. 3 broken.
