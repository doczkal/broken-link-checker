Getting links from: https://fsfe.org/about/people/testimonials.hr.html#kroah-hartman
├───OK─── https://fsfe.org/about/people/interviews/cryptie.hr.html#video-30s-cryptie
├───OK─── https://fsfe.org/about/people/interviews/lequertier.hr.html#video-30s-vincent
├───OK─── https://fsfe.org/about/people/interviews/zerolo.hr.html#video-30s-tomas
├─BROKEN─ https://fsfe.org/about/people/interviews/bernhard.hr.html#video-30s-bernhard (HTTP_404)
├───OK─── https://fsfe.org/about/people/interviews/weitzhofer.hr.html#interview
├───OK─── https://fsfe.org/about/people/interviews/grun.hr.html#interview
├───OK─── https://fsfe.org/about/people/interviews/ockers.hr.html#video-30s-andre
├───OK─── https://fsfe.org/about/people/interviews/mueller.hr.html#interview
├───OK─── https://fsfe.org/about/people/interviews/snow.hr.html#interview
├───OK─── https://fsfe.org/about/people/interviews/gkotsopoulou.hr.html#interview
Finished! 122 links found. 112 excluded. 1 broken.
