Getting links from: https://fsfe.org/news/2019/news-20191022-01.nn.html
├─BROKEN─ https://fsfe.org/news/2019/about/codeofconduct (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/contribute/web/web.nn.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/news-20191002.nn.html (HTTP_404)
├───OK─── https://fsfe.org/news/2019/news-20190701-01.nn.html
Finished! 222 links found. 218 excluded. 3 broken.
