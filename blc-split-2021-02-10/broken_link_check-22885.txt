Getting links from: https://fsfe.org/contact/contact.en.html
├───OK─── https://fsfe.org/about/people/kirschner/index.en.html
├─BROKEN─ https://fsfe.org/about/codeofconduct/index.en.html (HTTP_404)
Finished! 83 links found. 81 excluded. 1 broken.
