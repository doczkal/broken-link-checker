Getting links from: https://fsfe.org/news/nl/nl-201302.es.html#
├───OK─── https://fsfe.org/news/nl/nl-201302.en.html
├───OK─── https://fsfe.org/news/2012/report-2012.es.html
├───OK─── https://fsfe.org/activities/ilovefs/2013/index.es.html
├─BROKEN─ https://fsfe.org/activities/ilovefs/2013/banners.es.html (HTTP_404)
├─BROKEN─ https://fsfe.org/order/promoorder.es.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.es.html (HTTP_404)
Finished! 120 links found. 114 excluded. 3 broken.
