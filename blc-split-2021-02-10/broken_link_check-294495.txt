Getting links from: https://fsfe.org/about/people/testimonials.en.html
├───OK─── https://fsfe.org/about/people/interviews/cryptie.en.html#video-30s-cryptie
├───OK─── https://fsfe.org/about/people/interviews/zerolo.en.html#video-30s-tomas
├─BROKEN─ https://fsfe.org/about/people/interviews/bernhard.en.html#video-30s-bernhard (HTTP_404)
├───OK─── https://fsfe.org/about/people/interviews/weitzhofer.en.html#interview
├───OK─── https://fsfe.org/about/people/interviews/ockers.en.html#video-30s-andre
├───OK─── https://fsfe.org/about/people/interviews/mueller.en.html#interview
├───OK─── https://fsfe.org/about/people/interviews/snow.en.html#interview
├───OK─── https://fsfe.org/about/people/interviews/gkotsopoulou.en.html#interview
Finished! 119 links found. 111 excluded. 1 broken.
