Getting links from: https://fsfe.org/about/people/mehl/mehl.sr.html
├───OK─── https://fsfe.org/activities/radiodirective/index.sr.html
├─BROKEN─ https://fsfe.org/de (HTTP_404)
├───OK─── https://fsfe.org/activities/drm/index.sr.html
├───OK─── https://fsfe.org/contribute/web/index.sr.html
├─BROKEN─ https://fsfe.org/de (HTTP_404)
├─BROKEN─ https://fsfe.org/de (HTTP_404)
Finished! 82 links found. 76 excluded. 3 broken.
