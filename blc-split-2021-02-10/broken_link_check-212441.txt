Getting links from: https://fsfe.org/news/nl/nl-201309.nb.html
├───OK─── https://fsfe.org/activities/swpat/swpat.nb.html
├───OK─── https://fsfe.org/activities/msooxml/msooxml-interoperability.nb.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.nb.html (HTTP_404)
Finished! 117 links found. 114 excluded. 1 broken.
