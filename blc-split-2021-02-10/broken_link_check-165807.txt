Getting links from: https://fsfe.org/news/2007/news-20070630-01.uk.html
├───OK─── https://fsfe.org/activities/ms-vs-eu/index.uk.html
├───OK─── https://fsfe.org/activities/ipred2/index.uk.html
├───OK─── https://fsfe.org/activities/wsis/index.uk.html
├───OK─── https://fsfe.org/activities/igf/index.uk.html
├───OK─── https://fsfe.org/activities/igf/a2k.uk.html
├───OK─── https://fsfe.org/activities/igf/dcos.uk.html
├───OK─── https://fsfe.org/activities/wipo/statement-20050721.uk.html
├─BROKEN─ https://fsfe.org/ftf (HTTP_404)
├───OK─── https://fsfe.org/activities/gplv3/index.uk.html#transcripts
├───OK─── https://fsfe.org/activities/self/index.uk.html
├─BROKEN─ https://fsfe.org/fellows/greve/img/zrh_hoist (HTTP_404)
├─BROKEN─ https://fsfe.org/fellows/meetings/2006_bolzano (HTTP_404)
Finished! 104 links found. 92 excluded. 3 broken.
