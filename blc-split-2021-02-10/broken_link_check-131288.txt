Getting links from: https://fsfe.org/news/2019/news-20191022-01.bg.html#ngi0
├───OK─── https://fsfe.org/news/2019/news-20191007-01.bg.html
├───OK─── https://fsfe.org/about/people/interviews/grun.bg.html#interview
├─BROKEN─ https://fsfe.org/news/2019/about/codeofconduct (HTTP_404)
├───OK─── https://fsfe.org/events/index.bg.html
├─BROKEN─ https://fsfe.org/news/2019/contribute/web/web.bg.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/news-20191002.bg.html (HTTP_404)
├───OK─── https://fsfe.org/news/2019/news-20190701-01.bg.html
├───OK─── https://fsfe.org/news/2019/news-20190927-01.bg.html
├───OK─── https://fsfe.org/tags/tagged-sustainability.bg.html
├───OK─── https://fsfe.org/tags/tagged-savecodeshare.bg.html
Finished! 222 links found. 212 excluded. 3 broken.
