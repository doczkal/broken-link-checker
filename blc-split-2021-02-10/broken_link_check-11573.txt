Getting links from: https://fsfe.org/news/nl/nl-201501.cs.html
├───OK─── https://fsfe.org/fellowship/index.cs.html
├───OK─── https://fsfe.org/fellowship/about/fellowship.cs.html#youget
├─BROKEN─ https://fsfe.org/about/legal/constitution.cs.html#id-fellowship-seats (HTTP_404)
├─BROKEN─ https://fsfe.org/about/ojasild (HTTP_404)
Finished! 88 links found. 84 excluded. 2 broken.
