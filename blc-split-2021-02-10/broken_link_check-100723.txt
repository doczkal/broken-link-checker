Getting links from: https://fsfe.org/news/nl/nl-201309.hr.html
├───OK─── https://fsfe.org/news/nl/nl-201309.en.html
├───OK─── https://fsfe.org/activities/android/promomaterial/fdroid-folder.pdf
├───OK─── https://fsfe.org/activities/msooxml/msooxml-interoperability.hr.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.hr.html (HTTP_404)
Finished! 117 links found. 113 excluded. 1 broken.
