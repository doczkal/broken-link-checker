Getting links from: https://fsfe.org/news/nl/nl-201308.nb.html
├───OK─── https://fsfe.org/news/nl/nl-201308.de.html
├─BROKEN─ https://fsfe.org/news/2013/2013-07-26_Open_Letter_to_NEC.nb.html (HTTP_404)
├───OK─── https://fsfe.org/news/2013/news-20130712-01.nb.html
├───OK─── https://fsfe.org/freesoftware/standards/transparency-letter.nb.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.nb.html (HTTP_404)
Finished! 112 links found. 107 excluded. 2 broken.
