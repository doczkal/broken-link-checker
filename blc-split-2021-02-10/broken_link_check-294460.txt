Getting links from: https://fsfe.org/news/nl/nl-201105.zh.html
├───OK─── https://fsfe.org/activities/swpat/letter-20101222.zh.html
├───OK─── https://fsfe.org/activities/swpat/letter-20110406.zh.html
├───OK─── https://fsfe.org/activities/swpat/novell-cptn.zh.html
├─BROKEN─ https://fsfe.org/join.zh.html (HTTP_404)
├───OK─── https://fsfe.org/activities/pdfreaders/follow-up.zh.html
├───OK─── https://fsfe.org/about/tuke/tuke.zh.html
Finished! 103 links found. 97 excluded. 1 broken.
