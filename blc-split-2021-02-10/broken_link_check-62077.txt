Getting links from: https://fsfe.org/news/2013/index.nl.html
├───OK─── https://fsfe.org/news/2013/news-20131213-01.nl.html
├─BROKEN─ https://fsfe.org/news/2013/2013-07-26_Open_Letter_to_NEC.nl.html (HTTP_404)
├───OK─── https://fsfe.org/news/2013/news-20130712-01.nl.html
├───OK─── https://fsfe.org/news/2013/news-20130226-01.nl.html
Finished! 289 links found. 285 excluded. 1 broken.
