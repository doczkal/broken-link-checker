Getting links from: https://fsfe.org/news/nl/nl-201210.cs.html
├───OK─── https://fsfe.org/contribute/translators/index.cs.html
├───OK─── https://fsfe.org/index.cs.html
├───OK─── https://fsfe.org/about/about.cs.html
├───OK─── https://fsfe.org/activities/activities.cs.html
├───OK─── https://fsfe.org/news/2012/news-20120920-01.cs.html
├───OK─── https://fsfe.org/news/2012/news-20120918-01.cs.html
├───OK─── https://fsfe.org/contribute/translators/translators.cs.html
├───OK─── https://fsfe.org/news/2012/news-20120925-01.cs.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.cs.html (HTTP_404)
├───OK─── https://fsfe.org/about/js-licences.cs.html
├───OK─── https://fsfe.org/about/legal/imprint.cs.html
├───OK─── https://fsfe.org/about/transparency-commitment.cs.html
├───OK─── https://fsfe.org/contribute/web/web.cs.html
Finished! 121 links found. 107 excluded. 2 broken.
