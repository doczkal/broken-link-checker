Getting links from: https://fsfe.org/about/people/testimonials.sr.html#kroah-hartman
├───OK─── https://fsfe.org/about/people/interviews/cryptie.sr.html#video-30s-cryptie
├───OK─── https://fsfe.org/about/people/interviews/zerolo.sr.html#video-30s-tomas
├─BROKEN─ https://fsfe.org/about/people/interviews/bernhard.sr.html#video-30s-bernhard (HTTP_404)
├───OK─── https://fsfe.org/about/people/interviews/weitzhofer.sr.html#interview
├───OK─── https://fsfe.org/about/people/interviews/grun.sr.html#interview
├───OK─── https://fsfe.org/about/people/interviews/ockers.sr.html#video-30s-andre
├───OK─── https://fsfe.org/about/people/interviews/mueller.sr.html#interview
├───OK─── https://fsfe.org/about/people/interviews/snow.sr.html#interview
├───OK─── https://fsfe.org/about/people/interviews/gkotsopoulou.sr.html#interview
Finished! 122 links found. 113 excluded. 1 broken.
