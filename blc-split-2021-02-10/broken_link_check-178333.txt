Getting links from: https://fsfe.org/news/nl/nl-201112.pt.html
├───OK─── https://fsfe.org/news/2011/news-20111128-01.pt.html
├───OK─── https://fsfe.org/activities/pdfreaders/buglist.pt.html
├───OK─── https://fsfe.org/activities/pdfreaders/letter.pt.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.pt.html (HTTP_404)
Finished! 109 links found. 105 excluded. 1 broken.
