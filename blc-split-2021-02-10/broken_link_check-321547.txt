Getting links from: https://fsfe.org/news/nl/nl-201704.sv.html
├───OK─── https://fsfe.org/news/2017/news-20170328-01.sv.html
├───OK─── https://fsfe.org/news/2017/news-20170302-01.sv.html
├───OK─── https://fsfe.org/news/2017/news-20170321-01.sv.html
├───OK─── https://fsfe.org/news/2017/news-20170116-01.sv.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.sv.html#id-fellowship-seats (HTTP_404)
Finished! 103 links found. 98 excluded. 1 broken.
