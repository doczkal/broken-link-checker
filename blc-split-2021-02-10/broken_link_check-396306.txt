Getting links from: https://fsfe.org/news/nl/nl-201302.et.html
├───OK─── https://fsfe.org/news/nl/nl-201302.de.html
├───OK─── https://fsfe.org/news/2012/report-2012.et.html
├─BROKEN─ https://fsfe.org/activities/ilovefs/2013/banners.et.html (HTTP_404)
├─BROKEN─ https://fsfe.org/order/promoorder.et.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.et.html (HTTP_404)
Finished! 119 links found. 114 excluded. 3 broken.
