Getting links from: https://fsfe.org/news/2017/index.bs.html
├───OK─── https://fsfe.org/news/2017/news-20171211-01.bs.html
├───OK─── https://fsfe.org/news/2017/news-20171116-01.bs.html
├───OK─── https://fsfe.org/news/2017/news-20171207-02.bs.html
├───OK─── https://fsfe.org/news/2017/news-20170911-01.bs.html
├───OK─── https://fsfe.org/news/2017/news-20170906-01.bs.html
├───OK─── https://fsfe.org/news/2017/news-20170726-01.bs.html
├───OK─── https://fsfe.org/news/2017/news-20170425-01.bs.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.bs.html#id-fellowship-seats (HTTP_404)
Finished! 300 links found. 291 excluded. 2 broken.
