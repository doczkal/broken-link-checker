Getting links from: https://fsfe.org/about/people/testimonials.pt.html#guenther
├───OK─── https://fsfe.org/about/people/interviews/cryptie.pt.html#video-30s-cryptie
├───OK─── https://fsfe.org/about/people/interviews/lequertier.pt.html#video-30s-vincent
├───OK─── https://fsfe.org/about/people/interviews/zerolo.pt.html#video-30s-tomas
├─BROKEN─ https://fsfe.org/about/people/interviews/bernhard.pt.html#video-30s-bernhard (HTTP_404)
├───OK─── https://fsfe.org/about/people/interviews/weitzhofer.pt.html#interview
├───OK─── https://fsfe.org/about/people/interviews/ockers.pt.html#video-30s-andre
├───OK─── https://fsfe.org/about/people/interviews/mueller.pt.html#interview
├───OK─── https://fsfe.org/about/people/interviews/snow.pt.html#interview
├───OK─── https://fsfe.org/about/people/interviews/gkotsopoulou.pt.html#interview
Finished! 122 links found. 113 excluded. 1 broken.
