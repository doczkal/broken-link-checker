Getting links from: https://fsfe.org/about/people/testimonials.nb.html
├───OK─── https://fsfe.org/about/people/interviews/cryptie.nb.html#video-30s-cryptie
├───OK─── https://fsfe.org/about/people/interviews/zerolo.nb.html#video-30s-tomas
├─BROKEN─ https://fsfe.org/about/people/interviews/bernhard.nb.html#video-30s-bernhard (HTTP_404)
├───OK─── https://fsfe.org/about/people/interviews/weitzhofer.nb.html#interview
├───OK─── https://fsfe.org/about/people/interviews/ockers.nb.html#video-30s-andre
├───OK─── https://fsfe.org/about/people/interviews/mueller.nb.html#interview
├───OK─── https://fsfe.org/about/people/interviews/snow.nb.html#interview
├───OK─── https://fsfe.org/about/people/interviews/gkotsopoulou.nb.html#interview
Finished! 122 links found. 114 excluded. 1 broken.
