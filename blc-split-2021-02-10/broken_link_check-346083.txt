Getting links from: https://fsfe.org/news/2019/news-20191022-01.bg.html
├───OK─── https://fsfe.org/news/2019/news-20190329-01.bg.html
├─BROKEN─ https://fsfe.org/news/2019/about/codeofconduct (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/contribute/web/web.bg.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/news-20191002.bg.html (HTTP_404)
├───OK─── https://fsfe.org/news/2019/news-20190701-01.bg.html
Finished! 222 links found. 217 excluded. 3 broken.
