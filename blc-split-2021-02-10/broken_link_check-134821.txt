Getting links from: https://fsfe.org/news/2017/news-20171107-01.bg.html
├───OK─── https://fsfe.org/about/legal/minutes/minutes-2017-10-15.en.pdf
├─BROKEN─ https://fsfe.org/about/legal/constitution#id-fellowship-seats (HTTP_404)
Finished! 69 links found. 67 excluded. 1 broken.
