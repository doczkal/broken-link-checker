Getting links from: https://fsfe.org/freesoftware/education/edu-related-content.nl.html
├───OK─── https://fsfe.org/freesoftware/education/eduteam.nl.html
├───OK─── https://fsfe.org/freesoftware/education/argumentation.nl.html
├───OK─── https://fsfe.org/activities/mankind/lsm2002/index.nl.html
├─BROKEN─ https://fsfe.org/associates/ofset/associate (HTTP_404)
├───OK─── https://fsfe.org/activities/mankind/lsm2002/press-release.nl.html
├───OK─── https://fsfe.org/news/2011/news-20111128-02.nl.html#education
├───OK─── https://fsfe.org/news/2001/article2001-12-17-01.nl.html
├───OK─── https://fsfe.org/freesoftware/transcripts/rms-fs-2006-03-09.nl.html
├───OK─── https://fsfe.org/donate/letter-2011.nl.html
├───OK─── https://fsfe.org/events/2004/FISL/fisl.nl.html
├─BROKEN─ https://fsfe.org/freesoftware/education/news/2012/news-20121217-01 (HTTP_404)
├───OK─── https://fsfe.org/freesoftware/standards/guardian-open-letter.nl.html
├───OK─── https://fsfe.org/news/2003/lettera_MIUR-2003-07-16.nl.html
├───OK─── https://fsfe.org/activities/tgs/tgs.nl.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool1.nl.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool2.nl.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool3.nl.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool4.nl.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool5.nl.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool6.nl.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool7.nl.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool8.nl.html
Finished! 111 links found. 89 excluded. 2 broken.
