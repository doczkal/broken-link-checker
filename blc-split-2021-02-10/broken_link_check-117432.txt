Getting links from: https://fsfe.org/news/nl/nl-201704.it.html
├───OK─── https://fsfe.org/news/2017/news-20170321-01.it.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.it.html#id-fellowship-seats (HTTP_404)
Finished! 103 links found. 101 excluded. 1 broken.
