Getting links from: https://fsfe.org/contribute/translators/index.nb.html
├───OK─── https://fsfe.org/contribute/translators/translators.es.html
├───OK─── https://fsfe.org/contribute/translators/translators.et.html
├───OK─── https://fsfe.org/contribute/translators/translators.pt.html
├───OK─── https://fsfe.org/contribute/translators/translators.sq.html
├───OK─── https://fsfe.org/about/index.nb.html
├─BROKEN─ https://fsfe.org/contribute/translators/web.nb.html (HTTP_404)
Finished! 91 links found. 85 excluded. 1 broken.
