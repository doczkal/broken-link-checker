Getting links from: https://fsfe.org/about/funds/2007.cs.html
├───OK─── https://fsfe.org/help/thankgnus-2007.cs.html
├─BROKEN─ https://fsfe.org/about/legal/de/de.cs.html (HTTP_404)
├───OK─── https://fsfe.org/about/people/greve/index.cs.html
Finished! 87 links found. 84 excluded. 1 broken.
