Getting links from: https://fsfe.org/news/nl/nl-201310.cs.html
├───OK─── https://fsfe.org/news/nl/nl-201310.en.html
├───OK─── https://fsfe.org/freesoftware/basics/gnuproject.cs.html
├───OK─── https://fsfe.org/news/2013/news-20130926-01.cs.html
├───OK─── https://fsfe.org/news/2013/news-20130918-01.cs.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.cs.html (HTTP_404)
Finished! 120 links found. 115 excluded. 1 broken.
