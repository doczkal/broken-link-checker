Getting links from: https://fsfe.org/about/people/testimonials.zh.html#kroah-hartman
├───OK─── https://fsfe.org/about/people/interviews/cryptie.zh.html#video-30s-cryptie
├───OK─── https://fsfe.org/about/people/interviews/lequertier.zh.html#video-30s-vincent
├───OK─── https://fsfe.org/about/people/interviews/zerolo.zh.html#video-30s-tomas
├─BROKEN─ https://fsfe.org/about/people/interviews/bernhard.zh.html#video-30s-bernhard (HTTP_404)
├───OK─── https://fsfe.org/about/people/interviews/weitzhofer.zh.html#interview
├───OK─── https://fsfe.org/about/people/interviews/grun.zh.html#interview
├───OK─── https://fsfe.org/about/people/interviews/ockers.zh.html#video-30s-andre
├───OK─── https://fsfe.org/about/people/interviews/mueller.zh.html#interview
├───OK─── https://fsfe.org/about/people/interviews/snow.zh.html#interview
├───OK─── https://fsfe.org/about/people/interviews/gkotsopoulou.zh.html#interview
Finished! 122 links found. 112 excluded. 1 broken.
