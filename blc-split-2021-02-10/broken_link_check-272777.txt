Getting links from: https://fsfe.org/news/nl/nl-201302.ru.html
├───OK─── https://fsfe.org/news/2012/news-20121211-01.ru.html
├───OK─── https://fsfe.org/activities/ilovefs/2013/whylovefs.ru.html
├─BROKEN─ https://fsfe.org/activities/ilovefs/2013/banners.ru.html (HTTP_404)
├─BROKEN─ https://fsfe.org/order/promoorder.ru.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.ru.html (HTTP_404)
Finished! 120 links found. 115 excluded. 3 broken.
