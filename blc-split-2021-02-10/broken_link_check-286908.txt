Getting links from: https://fsfe.org/news/nl/nl-201912.nl.html
├───OK─── https://fsfe.org/news/2019/news-20191125-01.nl.html
├─BROKEN─ https://fsfe.org/nieuws/2019/nieuws-20191022-01.nl.html (HTTP_404)
├─BROKEN─ https://fsfe.org/nieuws/2019/nieuws-20191205-01.nl.html (HTTP_404)
├───OK─── https://fsfe.org/order/index.nl.html#tshirt-multilingual-black
Finished! 98 links found. 94 excluded. 2 broken.
