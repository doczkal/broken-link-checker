Getting links from: https://fsfe.org/news/2017/index.nl.html
├───OK─── https://fsfe.org/activities/radiodirective/index.nl.html
├───OK─── https://fsfe.org/news/2017/news-20171114-01.nl.html
├───OK─── https://fsfe.org/news/2017/news-20171207-02.nl.html
├───OK─── https://fsfe.org/news/2017/news-20170726-01.nl.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.nl.html#id-fellowship-seats (HTTP_404)
Finished! 302 links found. 296 excluded. 2 broken.
