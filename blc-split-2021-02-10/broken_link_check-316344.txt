Getting links from: https://fsfe.org/news/nl/nl-201502.fi.html
├───OK─── https://fsfe.org/news/2014/news-20140221-01.fi.html
├─BROKEN─ https://fsfe.org/about/ojasild/ojasild.fi.html (HTTP_404)
├─BROKEN─ https://fsfe.org/about/ojasild (HTTP_404)
Finished! 96 links found. 93 excluded. 2 broken.
