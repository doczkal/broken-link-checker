Getting links from: https://fsfe.org/news/nl/nl-201501.de.html
├───OK─── https://fsfe.org/fellowship/about/fellowship.de.html#youget
├─BROKEN─ https://fsfe.org/about/legal/constitution.de.html#id-fellowship-seats (HTTP_404)
├─BROKEN─ https://fsfe.org/about/ojasild (HTTP_404)
Finished! 86 links found. 83 excluded. 2 broken.
