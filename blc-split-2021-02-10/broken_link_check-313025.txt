Getting links from: https://fsfe.org/news/nl/nl-201704.pt.html
├───OK─── https://fsfe.org/news/2017/news-20170328-01.pt.html
├───OK─── https://fsfe.org/news/2017/news-20170302-01.pt.html
├───OK─── https://fsfe.org/news/2017/news-20170321-01.pt.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.pt.html#id-fellowship-seats (HTTP_404)
Finished! 104 links found. 100 excluded. 1 broken.
