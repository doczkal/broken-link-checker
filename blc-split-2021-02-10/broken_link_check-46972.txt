Getting links from: https://fsfe.org/news/nl/nl-201501.ru.html
├───OK─── https://fsfe.org/fellowship/index.ru.html
├───OK─── https://fsfe.org/fellowship/about/fellowship.ru.html#youget
├─BROKEN─ https://fsfe.org/about/legal/constitution.ru.html#id-fellowship-seats (HTTP_404)
├─BROKEN─ https://fsfe.org/about/ojasild (HTTP_404)
Finished! 89 links found. 85 excluded. 2 broken.
