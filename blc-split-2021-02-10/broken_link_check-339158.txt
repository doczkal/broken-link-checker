Getting links from: https://fsfe.org/contribute/web/tagging.ru.html
├───OK─── https://fsfe.org/contribute/web/index.ru.html
├─BROKEN─ https://fsfe.org/uk/uk.ru.html (HTTP_404)
├───OK─── https://fsfe.org/tags/tags.ru.html
├───OK─── https://fsfe.org/tags/tagged.ru.html
Finished! 54 links found. 50 excluded. 1 broken.
