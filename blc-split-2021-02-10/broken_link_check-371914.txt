Getting links from: https://fsfe.org/news/2012/report-2012.cs.html
├─BROKEN─ https://fsfe.org/news/2012/blogs.fsfe.org/samtuke/%3Fp%3D255 (HTTP_404)
├───OK─── https://fsfe.org/freesoftware/standards/standards.cs.html
├───OK─── https://fsfe.org/news/2012/news-20120627-01.cs.html
├───OK─── https://fsfe.org/news/2012/news-20120619-01.cs.html
├───OK─── https://fsfe.org/news/2012/news-20121112-01.cs.html
Finished! 93 links found. 88 excluded. 1 broken.
