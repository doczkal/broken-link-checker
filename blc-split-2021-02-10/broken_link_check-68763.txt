Getting links from: https://fsfe.org/news/nl/nl-201305.es.html
├───OK─── https://fsfe.org/news/2013/news-20130424-01.es.html
├───OK─── https://fsfe.org/news/2013/news-20130319-01.es.html
├───OK─── https://fsfe.org/news/2013/news-20130423-02.es.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.es.html (HTTP_404)
Finished! 108 links found. 104 excluded. 1 broken.
