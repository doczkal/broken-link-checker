Getting links from: https://fsfe.org/about/people/mehl/index.de.html
├───OK─── https://fsfe.org/activities/radiodirective/index.de.html
├─BROKEN─ https://fsfe.org/de (HTTP_404)
├───OK─── https://fsfe.org/activities/routers/index.de.html
├───OK─── https://fsfe.org/activities/android/index.de.html
├───OK─── https://fsfe.org/activities/drm/index.de.html
├───OK─── https://fsfe.org/contribute/web/index.de.html
├─BROKEN─ https://fsfe.org/de (HTTP_404)
├───OK─── https://fsfe.org/contribute/internship.de.html
├─BROKEN─ https://fsfe.org/de (HTTP_404)
Finished! 81 links found. 72 excluded. 3 broken.
