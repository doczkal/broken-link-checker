Getting links from: https://fsfe.org/activities/tgs/tagatschool6.sq.html
├─BROKEN─ https://fsfe.org/activities/tgs/http./www.qcad.org/index.sq.html (HTTP_404)
├───OK─── https://fsfe.org/activities/tgs/img/qcad.en.png
├───OK─── https://fsfe.org/activities/tgs/img/qcad-tux.en.png
├───OK─── https://fsfe.org/activities/tgs/img/schoolforge.en.png
Finished! 170 links found. 166 excluded. 1 broken.
