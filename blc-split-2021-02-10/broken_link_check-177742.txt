Getting links from: https://fsfe.org/news/nl/nl-202005.nb.html
├───OK─── https://fsfe.org/news/2020/news-20200408-01.nb.html
├───OK─── https://fsfe.org/news/2020/news-20200506-01.nb.html
├───OK─── https://fsfe.org/news/podcast/episode-5.nb.html
├───OK─── https://fsfe.org/news/podcast/episode-special-1.nb.html
├───OK─── https://fsfe.org/news/2020/news-20200427-01.nb.html
├─BROKEN─ https://fsfe.org/https:/librezoom.net/lz19-die-nexte-bitte/index.nb.html (HTTP_404)
├───OK─── https://fsfe.org/news/2020/news-20200424-01.nb.html
Finished! 98 links found. 91 excluded. 1 broken.
