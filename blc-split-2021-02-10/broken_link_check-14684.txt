Getting links from: https://fsfe.org/news/nl/nl-201501.ro.html
├───OK─── https://fsfe.org/news/nl/nl-201501.en.html
├───OK─── https://fsfe.org/fellowship/about/fellowship.ro.html#youget
├─BROKEN─ https://fsfe.org/about/legal/constitution.ro.html#id-fellowship-seats (HTTP_404)
├─BROKEN─ https://fsfe.org/about/ojasild (HTTP_404)
Finished! 88 links found. 84 excluded. 2 broken.
