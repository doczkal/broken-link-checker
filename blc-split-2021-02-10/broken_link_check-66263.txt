Getting links from: https://fsfe.org/about/people/roy/index.sq.html
├───OK─── https://fsfe.org/activities/ftf/ftf.sq.html
├─BROKEN─ https://fsfe.org/fr/index.sq.html (HTTP_404)
├─BROKEN─ https://fsfe.org/legal (HTTP_404)
├───OK─── https://fsfe.org/fellowship/index.sq.html
Finished! 80 links found. 76 excluded. 2 broken.
