Getting links from: https://fsfe.org/news/nl/nl-201501.mk.html
├───OK─── https://fsfe.org/news/nl/nl-201501.sq.html
├───OK─── https://fsfe.org/fellowship/about/fellowship.mk.html#youget
├─BROKEN─ https://fsfe.org/about/legal/constitution.mk.html#id-fellowship-seats (HTTP_404)
├─BROKEN─ https://fsfe.org/about/ojasild (HTTP_404)
Finished! 88 links found. 84 excluded. 2 broken.
