Getting links from: https://fsfe.org/news/nl/nl-201112.sl.html
├───OK─── https://fsfe.org/news/2011/news-20111128-01.sl.html
├───OK─── https://fsfe.org/news/2011/news-20111114-01.sl.html
├───OK─── https://fsfe.org/activities/pdfreaders/buglist.sl.html
├───OK─── https://fsfe.org/activities/pdfreaders/letter.sl.html
├───OK─── https://fsfe.org/news/2011/news-20111110-01.sl.html
├───OK─── https://fsfe.org/news/2011/news-20111122-01.sl.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.sl.html (HTTP_404)
Finished! 108 links found. 101 excluded. 1 broken.
