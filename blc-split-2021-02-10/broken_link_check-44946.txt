Getting links from: https://fsfe.org/news/2009/news-20090620-01.nb.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.nb.html (HTTP_404)
├─BROKEN─ https://fsfe.org/about/holz/holz.nb.html (HTTP_404)
├───OK─── https://fsfe.org/about/funds/funds.nb.html
├───OK─── https://fsfe.org/about/funds/2008.nb.html
├─BROKEN─ https://fsfe.org/activities/self/SELF-LegalPolicy-0.9_1.pdf (HTTP_404)
├───OK─── https://fsfe.org/activities/ftf/fiduciary.nb.html
├───OK─── https://fsfe.org/activities/stacs/london.nb.html
├───OK─── https://fsfe.org/activities/stacs/belgrade.nb.html
Finished! 127 links found. 119 excluded. 3 broken.
