Getting links from: https://fsfe.org/about/people/mehl/index.uk.html
├───OK─── https://fsfe.org/activities/radiodirective/index.uk.html
├─BROKEN─ https://fsfe.org/de (HTTP_404)
├───OK─── https://fsfe.org/activities/android/index.uk.html
├───OK─── https://fsfe.org/activities/drm/index.uk.html
├───OK─── https://fsfe.org/contribute/web/index.uk.html
├─BROKEN─ https://fsfe.org/de (HTTP_404)
├─BROKEN─ https://fsfe.org/de (HTTP_404)
Finished! 82 links found. 75 excluded. 3 broken.
