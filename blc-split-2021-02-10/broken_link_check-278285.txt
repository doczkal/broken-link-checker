Getting links from: https://fsfe.org/news/nl/nl-201804.pl.html#
├───OK─── https://fsfe.org/news/nl/nl-201804.es.html
├───OK─── https://fsfe.org/news/nl/nl-201804.nl.html
├───OK─── https://fsfe.org/news/nl/nl-201804.sq.html
├─BROKEN─ https://fsfe.org/news/2018/news-20180414 (HTTP_404)
Finished! 94 links found. 90 excluded. 1 broken.
