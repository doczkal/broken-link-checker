Getting links from: https://fsfe.org/news/nl/nl-201302.de.html
├───OK─── https://fsfe.org/news/2012/news-20121211-01.de.html
├───OK─── https://fsfe.org/news/2012/report-2012.de.html
├───OK─── https://fsfe.org/activities/ilovefs/2013/index.de.html
├───OK─── https://fsfe.org/activities/ilovefs/2013/whylovefs.de.html
├─BROKEN─ https://fsfe.org/activities/ilovefs/2013/banners.de.html (HTTP_404)
├─BROKEN─ https://fsfe.org/order/promoorder.de.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.de.html (HTTP_404)
Finished! 117 links found. 110 excluded. 3 broken.
