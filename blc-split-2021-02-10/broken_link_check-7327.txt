Getting links from: https://fsfe.org/activities/ilovefs/2013/whylovefs.en.html
├───OK─── https://fsfe.org/activities/ilovefs/2012/photos/barbieri.jpg
├───OK─── https://fsfe.org/activities/ilovefs/2012/photos/consonni_sm.jpg
├───OK─── https://fsfe.org/activities/ilovefs/2012/photos/gratzer_sm.jpg
├───OK─── https://fsfe.org/activities/ilovefs/2012/photos/jelmorini_sm.jpg
├───OK─── https://fsfe.org/activities/ilovefs/2012/photos/kkretschmann_sm.jpg
├───OK─── https://fsfe.org/activities/ilovefs/2012/photos/rusu_sm.jpg
├───OK─── https://fsfe.org/activities/ilovefs/2012/photos/schumacher.jpg
├─BROKEN─ https://fsfe.org/activities/ilovefs/2012/photos/schulz.jpg (HTTP_404)
├───OK─── https://fsfe.org/activities/ilovefs/2012/photos/woolfrey_sm.jpg
├───OK─── https://fsfe.org/activities/ilovefs/2012/photos/mzuther_sm.jpg
Finished! 59 links found. 49 excluded. 1 broken.
