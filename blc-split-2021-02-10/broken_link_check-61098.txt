Getting links from: https://fsfe.org/news/nl/nl-201305.ca.html
├───OK─── https://fsfe.org/news/nl/nl-201305.en.html
├───OK─── https://fsfe.org/news/2013/news-20130424-01.ca.html
├───OK─── https://fsfe.org/news/2013/news-20130422-01.ca.html
├───OK─── https://fsfe.org/news/2013/news-20130319-01.ca.html
├───OK─── https://fsfe.org/news/2013/news-20130423-02.ca.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.ca.html (HTTP_404)
Finished! 107 links found. 101 excluded. 1 broken.
