Getting links from: https://fsfe.org/freesoftware/education/education.sv.html
├───OK─── https://fsfe.org/about/about.sv.html
├───OK─── https://fsfe.org/freesoftware/education/argumentation.sv.html
├─BROKEN─ https://fsfe.org/freesoftware/education/tgs/tgs.en.html (HTTP_404)
├───OK─── https://fsfe.org/freesoftware/education/eduteam.sv.html
Finished! 80 links found. 76 excluded. 1 broken.
