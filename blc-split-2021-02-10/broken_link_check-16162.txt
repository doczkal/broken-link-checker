Getting links from: https://fsfe.org/news/nl/nl-201105.sr.html
├───OK─── https://fsfe.org/activities/swpat/letter-20110406.sr.html
├───OK─── https://fsfe.org/activities/swpat/novell-cptn.sr.html
├─BROKEN─ https://fsfe.org/join.sr.html (HTTP_404)
├───OK─── https://fsfe.org/activities/pdfreaders/follow-up.sr.html
├───OK─── https://fsfe.org/about/tuke/tuke.sr.html
Finished! 103 links found. 98 excluded. 1 broken.
