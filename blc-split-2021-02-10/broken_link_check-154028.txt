Getting links from: https://fsfe.org/activities/pdfreaders/buglist.bs.html
├───OK─── https://fsfe.org/activities/pdfreaders/buglist.de.html
├───OK─── https://fsfe.org/activities/pdfreaders/buglist.el.html
├───OK─── https://fsfe.org/activities/pdfreaders/buglist.es.html
├───OK─── https://fsfe.org/activities/pdfreaders/buglist.fr.html
├───OK─── https://fsfe.org/activities/pdfreaders/buglist.hr.html
├───OK─── https://fsfe.org/activities/pdfreaders/buglist.pt.html
├───OK─── https://fsfe.org/activities/pdfreaders/buglist.ru.html
├───OK─── https://fsfe.org/activities/pdfreaders/buglist.sq.html
├─BROKEN─ https://fsfe.org/activities/pdfreaders/lodz.uw.gov.pl/data/auctions/siwz_serwis.pdf (HTTP_404)
Finished! 2209 links found. 2200 excluded. 1 broken.
