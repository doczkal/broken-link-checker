Getting links from: https://fsfe.org/news/nl/nl-201209.cs.html
├───OK─── https://fsfe.org/news/nl/nl-201208.cs.html
├───OK─── https://fsfe.org/freesoftware/standards/def.cs.html
├───OK─── https://fsfe.org/news/2012/news-20120831-01.cs.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.cs.html (HTTP_404)
Finished! 115 links found. 111 excluded. 1 broken.
