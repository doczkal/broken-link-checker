Getting links from: https://fsfe.org/news/2017/news-20171107-01.sv.html
├───OK─── https://fsfe.org/news/2017/news-20171107-01.en.html
├───OK─── https://fsfe.org/about/people/kirschner/kirschner
├───OK─── https://fsfe.org/about/people/repentinus/repentinus
├─BROKEN─ https://fsfe.org/about/legal/constitution#id-fellowship-seats (HTTP_404)
Finished! 69 links found. 65 excluded. 1 broken.
