Getting links from: https://fsfe.org/news/nl/nl-201501.et.html
├───OK─── https://fsfe.org/news/2014/news-20141218-02.et.html
├───OK─── https://fsfe.org/fellowship/about/fellowship.et.html#youget
├─BROKEN─ https://fsfe.org/about/legal/constitution.et.html#id-fellowship-seats (HTTP_404)
├─BROKEN─ https://fsfe.org/about/ojasild (HTTP_404)
Finished! 88 links found. 84 excluded. 2 broken.
