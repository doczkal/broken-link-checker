Getting links from: https://fsfe.org/news/2019/news-20191022-01.tr.html
├───OK─── https://fsfe.org/news/2019/news-20190514-01.tr.html
├───OK─── https://fsfe.org/news/2019/news-20191007-01.tr.html
├───OK─── https://fsfe.org/news/2019/news-20190329-01.tr.html
├───OK─── https://fsfe.org/about/people/testimonials
├───OK─── https://fsfe.org/about/people/interviews/grun.tr.html#interview
├─BROKEN─ https://fsfe.org/news/2019/about/codeofconduct (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/contribute/web/web.tr.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/news-20191002.tr.html (HTTP_404)
├───OK─── https://fsfe.org/news/2019/news-20190701-01.tr.html
├───OK─── https://fsfe.org/news/2019/news-20190927-01.tr.html
Finished! 222 links found. 212 excluded. 3 broken.
