Getting links from: https://fsfe.org/news/2018/news-20180215-01.es.html
├───OK─── https://fsfe.org/news/2017/news-20170214-02.es.html
├─BROKEN─ https://fsfe.org/freesoftware/standards/standards.html%3E (HTTP_404)
├───OK─── https://fsfe.org/tags/tagged-policy.es.html
├───OK─── https://fsfe.org/tags/tagged-pmpc.es.html
├───OK─── https://fsfe.org/tags/tagged-openstandards.es.html
├───OK─── https://fsfe.org/tags/tagged-public-administration.es.html
Finished! 74 links found. 68 excluded. 1 broken.
