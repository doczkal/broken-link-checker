Getting links from: https://fsfe.org/news/nl/nl-201309.ro.html
├───OK─── https://fsfe.org/activities/swpat/swpat.ro.html
├───OK─── https://fsfe.org/news/2013/news-20130612-01.ro.html
├───OK─── https://fsfe.org/activities/msooxml/msooxml-interoperability.ro.html
├───OK─── https://fsfe.org/activities/ms-vs-eu/ms-vs-eu.ro.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.ro.html (HTTP_404)
Finished! 117 links found. 112 excluded. 1 broken.
