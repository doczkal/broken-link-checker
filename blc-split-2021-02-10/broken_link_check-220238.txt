Getting links from: https://fsfe.org/news/2018/news-20180215-01.da.html
├───OK─── https://fsfe.org/news/2017/news-20171130-01.da.html
├───OK─── https://fsfe.org/activities/msooxml/msooxml.da.html
├─BROKEN─ https://fsfe.org/freesoftware/standards/standards.html%3E (HTTP_404)
├───OK─── https://fsfe.org/tags/tagged-policy.da.html
Finished! 73 links found. 69 excluded. 1 broken.
