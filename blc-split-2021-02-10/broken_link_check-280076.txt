Getting links from: https://fsfe.org/news/2008/index.nl.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.nl.html (HTTP_404)
├───OK─── https://fsfe.org/tags/tagged-competition.nl.html
├───OK─── https://fsfe.org/tags/tagged-enterprise.nl.html
├───OK─── https://fsfe.org/news/2008/news-20080220-01.nl.html
Finished! 138 links found. 134 excluded. 1 broken.
