Getting links from: https://fsfe.org/news/nl/nl-201202.et.html
├───OK─── https://fsfe.org/news/nl/nl-201202.de.html
├───OK─── https://fsfe.org/news/nl/nl-201202.el.html
├───OK─── https://fsfe.org/news/nl/nl-201202.es.html
├───OK─── https://fsfe.org/news/nl/nl-201202.fr.html
├───OK─── https://fsfe.org/news/nl/nl-201202.it.html
├───OK─── https://fsfe.org/news/nl/nl-201202.nl.html
├───OK─── https://fsfe.org/activities/nledu/nledu.et.html
├───OK─── https://fsfe.org/news/2012/news-20120130-01.et.html
├───OK─── https://fsfe.org/news/2012/news-20120131-01.et.html
├─BROKEN─ https://fsfe.org/activities/ilovefs/2012/banners.et.html (HTTP_404)
├───OK─── https://fsfe.org/activities/ilovefs/2012/unperfekthaus.et.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.et.html (HTTP_404)
Finished! 109 links found. 97 excluded. 2 broken.
