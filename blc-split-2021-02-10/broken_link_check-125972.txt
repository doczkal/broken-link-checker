Getting links from: https://fsfe.org/about/funds/2004.sq.html
├───OK─── https://fsfe.org/about/funds/2004.de.html
├───OK─── https://fsfe.org/about/funds/2004.el.html
├───OK─── https://fsfe.org/about/funds/2004.fr.html
├───OK─── https://fsfe.org/help/thankgnus-2004.sq.html
├─BROKEN─ https://fsfe.org/about/legal/de/de.sq.html (HTTP_404)
Finished! 87 links found. 82 excluded. 1 broken.
