Getting links from: https://fsfe.org/news/2007/news-20070630-01.en.html
├───OK─── https://fsfe.org/activities/wsis/index.en.html
├───OK─── https://fsfe.org/activities/wipo/index.en.html
├─BROKEN─ https://fsfe.org/ftf (HTTP_404)
├─BROKEN─ https://fsfe.org/fellows/greve/img/zrh_hoist (HTTP_404)
├─BROKEN─ https://fsfe.org/fellows/meetings/2006_bolzano (HTTP_404)
Finished! 101 links found. 96 excluded. 3 broken.
