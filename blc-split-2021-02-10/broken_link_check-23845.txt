Getting links from: https://fsfe.org/news/nl/nl-201502.en.html
├───OK─── https://fsfe.org/news/nl/nl-201502.nl.html
├───OK─── https://fsfe.org/news/nl/nl-201502.sq.html
├───OK─── https://fsfe.org/news/2014/news-20140221-01.en.html
├─BROKEN─ https://fsfe.org/about/ojasild/ojasild.en.html (HTTP_404)
├─BROKEN─ https://fsfe.org/about/ojasild (HTTP_404)
Finished! 92 links found. 87 excluded. 2 broken.
