Getting links from: https://fsfe.org/news/nl/nl-201502.fr.html
├───OK─── https://fsfe.org/news/2014/news-20140221-01.fr.html
├─BROKEN─ https://fsfe.org/about/ojasild/ojasild.fr.html (HTTP_404)
├─BROKEN─ https://fsfe.org/about/ojasild (HTTP_404)
Finished! 90 links found. 87 excluded. 2 broken.
