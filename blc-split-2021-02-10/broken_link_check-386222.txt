Getting links from: https://fsfe.org/news/nl/nl-201201.cs.html
├───OK─── https://fsfe.org/news/2011/news-20111128-02.cs.html
├─BROKEN─ https://fsfe.org/activities/elections/askyourcandidates/example-questions.xhtml (HTTP_404)
├───OK─── https://fsfe.org/news/2011/news-20111201-02.cs.html
├───OK─── https://fsfe.org/news/2011/news-20111220-01.cs.html
├───OK─── https://fsfe.org/donate/letter-2011.cs.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.cs.html (HTTP_404)
Finished! 87 links found. 81 excluded. 2 broken.
