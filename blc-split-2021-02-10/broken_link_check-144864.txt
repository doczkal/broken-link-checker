Getting links from: https://fsfe.org/news/2019/news-20191022-01.en.html
├─BROKEN─ https://fsfe.org/news/2019/about/codeofconduct (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/contribute/web/web.en.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/news-20191002.en.html (HTTP_404)
├───OK─── https://fsfe.org/news/2019/news-20190701-01.en.html
Finished! 219 links found. 215 excluded. 3 broken.
