Getting links from: https://fsfe.org/news/nl/nl-201302.sk.html
├───OK─── https://fsfe.org/news/nl/nl-201302.en.html
├───OK─── https://fsfe.org/activities/ilovefs/2013/index.sk.html
├───OK─── https://fsfe.org/activities/ilovefs/2013/whylovefs.sk.html
├─BROKEN─ https://fsfe.org/activities/ilovefs/2013/banners.sk.html (HTTP_404)
├─BROKEN─ https://fsfe.org/order/promoorder.sk.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.sk.html (HTTP_404)
Finished! 119 links found. 113 excluded. 3 broken.
