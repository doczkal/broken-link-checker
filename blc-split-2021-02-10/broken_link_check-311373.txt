Getting links from: https://fsfe.org/news/nl/nl-201704.uk.html
├───OK─── https://fsfe.org/news/2017/news-20170321-01.uk.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.uk.html#id-fellowship-seats (HTTP_404)
Finished! 103 links found. 101 excluded. 1 broken.
