Getting links from: https://fsfe.org/news/2018/news-20181105-01.nb.html
├───OK─── https://fsfe.org/contribute/contribute
├───OK─── https://fsfe.org/news/2018/news-20180705-01.nb.html
├───OK─── https://fsfe.org/news/2018/news-20180308-01.nb.html
├───OK─── https://fsfe.org/news/2018/graphics/ilovefs-wordcloud.png
├───OK─── https://fsfe.org/news/2017/news-20171116-01.nb.html
├─BROKEN─ https://fsfe.org/about/mancheva/mancheva (HTTP_404)
├───OK─── https://fsfe.org/activities/ftf/activities.nb.html
├─BROKEN─ https://fsfe.org/order/order.htmltshirt-100freedoms-black (HTTP_404)
Finished! 204 links found. 196 excluded. 2 broken.
