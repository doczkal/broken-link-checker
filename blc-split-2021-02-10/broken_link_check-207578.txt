Getting links from: https://fsfe.org/news/nl/nl-201210.et.html
├───OK─── https://fsfe.org/news/nl/nl-201210.de.html
├───OK─── https://fsfe.org/news/nl/nl-201210.el.html
├───OK─── https://fsfe.org/news/nl/nl-201210.fr.html
├───OK─── https://fsfe.org/news/2012/news-20120918-01.et.html
├───OK─── https://fsfe.org/news/2012/news-20120925-01.et.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.et.html (HTTP_404)
Finished! 123 links found. 116 excluded. 2 broken.
