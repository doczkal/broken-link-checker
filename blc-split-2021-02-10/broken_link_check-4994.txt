Getting links from: https://fsfe.org/news/nl/nl-201212.el.html
├───OK─── https://fsfe.org/news/2012/news-20121116-01.el.html
├─BROKEN─ https://fsfe.org/news/nl/lwn.net/Articles/523537/index.el.html (HTTP_404)
├───OK─── https://fsfe.org/news/2012/news-20121112-01.el.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.el.html (HTTP_404)
Finished! 115 links found. 111 excluded. 2 broken.
