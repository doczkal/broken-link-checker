Getting links from: https://fsfe.org/about/people/testimonials.pl.html
├───OK─── https://fsfe.org/about/people/interviews/cryptie.pl.html#video-30s-cryptie
├───OK─── https://fsfe.org/about/people/interviews/lequertier.pl.html#video-30s-vincent
├───OK─── https://fsfe.org/about/people/interviews/zerolo.pl.html#video-30s-tomas
├─BROKEN─ https://fsfe.org/about/people/interviews/bernhard.pl.html#video-30s-bernhard (HTTP_404)
├───OK─── https://fsfe.org/about/people/interviews/weitzhofer.pl.html#interview
├───OK─── https://fsfe.org/about/people/interviews/ockers.pl.html#video-30s-andre
├───OK─── https://fsfe.org/about/people/interviews/mueller.pl.html#interview
├───OK─── https://fsfe.org/about/people/interviews/snow.pl.html#interview
├───OK─── https://fsfe.org/about/people/interviews/gkotsopoulou.pl.html#interview
Finished! 122 links found. 113 excluded. 1 broken.
