Getting links from: https://fsfe.org/freesoftware/education/edu-related-content.bs.html
├───OK─── https://fsfe.org/activities/mankind/lsm2002/index.bs.html
├─BROKEN─ https://fsfe.org/associates/ofset/associate (HTTP_404)
├───OK─── https://fsfe.org/activities/mankind/lsm2002/press-release.bs.html
├───OK─── https://fsfe.org/activities/wsis/cs-benchmarks-03-11-14.bs.html
├───OK─── https://fsfe.org/freesoftware/transcripts/rms-fs-2006-03-09.bs.html
├─BROKEN─ https://fsfe.org/freesoftware/education/news/2012/news-20121217-01 (HTTP_404)
├───OK─── https://fsfe.org/activities/tgs/tgs.bs.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool1.bs.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool2.bs.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool3.bs.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool4.bs.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool5.bs.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool6.bs.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool7.bs.html
Finished! 111 links found. 97 excluded. 2 broken.
