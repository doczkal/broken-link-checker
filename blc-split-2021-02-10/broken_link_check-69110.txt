Getting links from: https://fsfe.org/news/2016/news-20160224-01.et.html#
├───OK─── https://fsfe.org/contribute/spreadtheword.et.html
├───OK─── https://fsfe.org/news/2015/news-20150303-01.et.html
├─BROKEN─ https://fsfe.org/activities/ilovefs/whylovefs/photos/gallery/ilovefs-gallery-thumb-25.jpg (HTTP_404)
├───OK─── https://fsfe.org/activities/policy.et.html
├───OK─── https://fsfe.org/news/2015/news-20150325-01.et.html
├───OK─── https://fsfe.org/activities/pdfreaders/pdfreaders.et.html
├───OK─── https://fsfe.org/news/2015/news-20150506-01.et.html
├───OK─── https://fsfe.org/news/2015/news-20150605-02.et.html
├───OK─── https://fsfe.org/news/2015/news-20150918-02.et.html
├───OK─── https://fsfe.org/news/2015/news-20150817-01.et.html
Finished! 89 links found. 79 excluded. 1 broken.
