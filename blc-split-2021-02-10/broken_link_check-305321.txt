Getting links from: https://fsfe.org/news/nl/nl-201302.nl.html
├───OK─── https://fsfe.org/news/2012/news-20121217-01.nl.html
├───OK─── https://fsfe.org/activities/ilovefs/2013/index.nl.html
├───OK─── https://fsfe.org/activities/ilovefs/2013/whylovefs.nl.html
├─BROKEN─ https://fsfe.org/activities/ilovefs/2013/banners.nl.html (HTTP_404)
├─BROKEN─ https://fsfe.org/order/promoorder.nl.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.nl.html (HTTP_404)
Finished! 119 links found. 113 excluded. 3 broken.
