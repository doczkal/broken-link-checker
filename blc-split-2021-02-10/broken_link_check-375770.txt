Getting links from: https://fsfe.org/contribute/web/tagging.uk.html
├─BROKEN─ https://fsfe.org/uk/uk.uk.html (HTTP_404)
├───OK─── https://fsfe.org/tags/tags.uk.html
├───OK─── https://fsfe.org/tags/tagged.uk.html
Finished! 53 links found. 50 excluded. 1 broken.
