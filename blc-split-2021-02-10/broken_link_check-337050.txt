Getting links from: https://fsfe.org/news/nl/nl-201203.nl.html
├───OK─── https://fsfe.org/news/2012/news-20120228-01.nl.html
├───OK─── https://fsfe.org/news/2012/news-20120214-01.nl.html
├───OK─── https://fsfe.org/news/2012/news-20120204-01.nl.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.nl.html (HTTP_404)
Finished! 119 links found. 115 excluded. 1 broken.
