Getting links from: https://fsfe.org/news/2008/index.de.html
├───OK─── https://fsfe.org/news/2008/news-20081210-01.de.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.de.html (HTTP_404)
├───OK─── https://fsfe.org/news/2008/news-20080305-01.de.html
├───OK─── https://fsfe.org/news/2008/news-20080301-01.de.html
├───OK─── https://fsfe.org/news/2008/news-20080228-01.de.html
├───OK─── https://fsfe.org/news/2008/news-20080222-01.de.html
├───OK─── https://fsfe.org/news/2008/news-20080220-01.de.html
├───OK─── https://fsfe.org/news/2008/news-20080214-01.de.html
├───OK─── https://fsfe.org/news/2008/news-20080118-01.de.html
Finished! 136 links found. 127 excluded. 1 broken.
