Getting links from: https://fsfe.org/news/2007/news-20070630-01.tr.html
├───OK─── https://fsfe.org/activities/ipred2/index.tr.html
├───OK─── https://fsfe.org/activities/igf/index.tr.html
├───OK─── https://fsfe.org/activities/igf/a2k.tr.html
├───OK─── https://fsfe.org/activities/igf/dcos.tr.html
├───OK─── https://fsfe.org/activities/wipo/statement-20050721.tr.html
├─BROKEN─ https://fsfe.org/ftf (HTTP_404)
├───OK─── https://fsfe.org/activities/gplv3/index.tr.html#transcripts
├───OK─── https://fsfe.org/activities/self/index.tr.html
├─BROKEN─ https://fsfe.org/fellows/greve/img/zrh_hoist (HTTP_404)
├─BROKEN─ https://fsfe.org/fellows/meetings/2006_bolzano (HTTP_404)
Finished! 104 links found. 94 excluded. 3 broken.
