Getting links from: https://fsfe.org/news/nl/nl-201205.nl.html
├─BROKEN─ https://fsfe.org/activities/swpat/swapt.nl.html (HTTP_404)
├───OK─── https://fsfe.org/news/2012/news-20120412-02.nl.html
├───OK─── https://fsfe.org/news/2012/news-20120402-01.nl.html
├─BROKEN─ https://fsfe.org/project/os/def.nl.html (HTTP_404)
├───OK─── https://fsfe.org/news/2012/news-20120426-01.nl.html
├───OK─── https://fsfe.org/freesoftware/standards/uk-standards-consultation.nl.html
├───OK─── https://fsfe.org/news/2012/news-20120425-02.nl.html
├───OK─── https://fsfe.org/news/2012/news-20120425-01.nl.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.nl.html (HTTP_404)
Finished! 114 links found. 105 excluded. 3 broken.
