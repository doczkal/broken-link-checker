Getting links from: https://fsfe.org/about/people/roy/roy.da.html
├───OK─── https://fsfe.org/about/team.da.html
├─BROKEN─ https://fsfe.org/fr/index.da.html (HTTP_404)
├─BROKEN─ https://fsfe.org/legal (HTTP_404)
Finished! 80 links found. 77 excluded. 2 broken.
