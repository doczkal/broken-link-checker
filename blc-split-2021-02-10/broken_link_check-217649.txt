Getting links from: https://fsfe.org/news/2007/news-20070630-01.pl.html
├───OK─── https://fsfe.org/activities/ms-vs-eu/index.pl.html
├───OK─── https://fsfe.org/activities/ipred2/index.pl.html
├─BROKEN─ https://fsfe.org/ftf (HTTP_404)
├───OK─── https://fsfe.org/activities/gplv3/index.pl.html#transcripts
├───OK─── https://fsfe.org/activities/self/index.pl.html
├─BROKEN─ https://fsfe.org/fellows/greve/img/zrh_hoist (HTTP_404)
├─BROKEN─ https://fsfe.org/fellows/meetings/2006_bolzano (HTTP_404)
Finished! 104 links found. 97 excluded. 3 broken.
