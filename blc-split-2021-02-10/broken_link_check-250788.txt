Getting links from: https://fsfe.org/news/nl/nl-201210.sk.html
├───OK─── https://fsfe.org/news/nl/nl-201210.de.html
├───OK─── https://fsfe.org/news/nl/nl-201210.el.html
├───OK─── https://fsfe.org/news/nl/nl-201210.fr.html
├───OK─── https://fsfe.org/news/2012/news-20120918-01.sk.html
├───OK─── https://fsfe.org/news/nl/nl-201208.sk.html
├───OK─── https://fsfe.org/news/2012/news-20120925-01.sk.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.sk.html (HTTP_404)
Finished! 123 links found. 115 excluded. 2 broken.
