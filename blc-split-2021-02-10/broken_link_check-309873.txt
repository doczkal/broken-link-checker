Getting links from: https://fsfe.org/contact/index.cs.html
├───OK─── https://fsfe.org/about/people/kirschner/index.cs.html
├─BROKEN─ https://fsfe.org/about/codeofconduct/index.cs.html (HTTP_404)
Finished! 86 links found. 84 excluded. 1 broken.
