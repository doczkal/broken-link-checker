Getting links from: https://fsfe.org/news/2009/news-20090301-01.nb.html
├───OK─── https://fsfe.org/contribute/translators/index.nb.html
├───OK─── https://fsfe.org/index.nb.html
├───OK─── https://fsfe.org/about/about.nb.html
├───OK─── https://fsfe.org/activities/activities.nb.html
├───OK─── https://fsfe.org/contribute/contribute.nb.html
├───OK─── https://fsfe.org/news/2008/news-20081210-01.nb.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.nb.html (HTTP_404)
├───OK─── https://fsfe.org/freesoftware/freesoftware.nb.html
├───OK─── https://fsfe.org/about/js-licences.nb.html
├───OK─── https://fsfe.org/contact/contact.nb.html
├───OK─── https://fsfe.org/about/legal/imprint.nb.html
├───OK─── https://fsfe.org/about/transparency-commitment.nb.html
├───OK─── https://fsfe.org/contribute/web/web.nb.html
Finished! 65 links found. 52 excluded. 1 broken.
