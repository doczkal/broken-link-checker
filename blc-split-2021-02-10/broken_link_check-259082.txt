Getting links from: https://fsfe.org/news/2019/news-20191022-01.ca.html#events2019
├───OK─── https://fsfe.org/news/2019/news-20190515-01.ca.html
├───OK─── https://fsfe.org/news/2019/news-20190515-02.ca.html
├───OK─── https://fsfe.org/news/2019/news-20190326-01
├───OK─── https://fsfe.org/activities/radiodirective
├───OK─── https://fsfe.org/about/people/interviews/grun.ca.html#interview
├─BROKEN─ https://fsfe.org/news/2019/about/codeofconduct (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/contribute/web/web.ca.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/news-20191002.ca.html (HTTP_404)
├───OK─── https://fsfe.org/news/2019/news-20190701-01.ca.html
├───OK─── https://fsfe.org/tags/tagged-sustainability.ca.html
Finished! 222 links found. 212 excluded. 3 broken.
