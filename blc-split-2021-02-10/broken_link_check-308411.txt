Getting links from: https://fsfe.org/freesoftware/education/edu-related-content.nn.html
├───OK─── https://fsfe.org/activities/mankind/lsm2002/index.nn.html
├─BROKEN─ https://fsfe.org/associates/ofset/associate (HTTP_404)
├───OK─── https://fsfe.org/activities/mankind/lsm2002/press-release.nn.html
├───OK─── https://fsfe.org/news/2011/news-20111128-02.nn.html#education
├───OK─── https://fsfe.org/news/2001/article2001-12-17-01.nn.html
├───OK─── https://fsfe.org/activities/wsis/cs-benchmarks-03-11-14.nn.html
├───OK─── https://fsfe.org/freesoftware/transcripts/rms-fs-2006-03-09.nn.html
├───OK─── https://fsfe.org/donate/letter-2011.nn.html
├───OK─── https://fsfe.org/events/2004/FISL/fisl.nn.html
├─BROKEN─ https://fsfe.org/freesoftware/education/news/2012/news-20121217-01 (HTTP_404)
├───OK─── https://fsfe.org/news/nl/nl-201101.nn.html
├───OK─── https://fsfe.org/news/2003/lettera_MIUR-2003-07-16.nn.html
├───OK─── https://fsfe.org/activities/tgs/tgs.nn.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool1.nn.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool2.nn.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool3.nn.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool4.nn.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool5.nn.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool6.nn.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool7.nn.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool8.nn.html
├───OK─── https://fsfe.org/contribute/editors/editors.nn.html
Finished! 111 links found. 89 excluded. 2 broken.
