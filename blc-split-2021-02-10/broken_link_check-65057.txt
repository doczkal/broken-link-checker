Getting links from: https://fsfe.org/news/nl/nl-201209.zh.html
├───OK─── https://fsfe.org/freesoftware/standards/def.zh.html
├───OK─── https://fsfe.org/activities/msooxml/msooxml-questions.zh.html
├───OK─── https://fsfe.org/news/2012/news-20120831-01.zh.html
├───OK─── https://fsfe.org/news/2011/news-20110301-01.zh.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.zh.html (HTTP_404)
Finished! 115 links found. 110 excluded. 1 broken.
