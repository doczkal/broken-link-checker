Getting links from: https://fsfe.org/activities/gplv3/europe-gplv3-conference.da.html
├───OK─── https://fsfe.org/activities/gplv3/gplv3.da.html
├─BROKEN─ https://fsfe.org/activities/gplv3/GPLv3-logo-red.png (HTTP_404)
├───OK─── https://fsfe.org/about/oriordan/oriordan.da.html
├───OK─── https://fsfe.org/about/maffulli/maffulli.da.html
Finished! 103 links found. 99 excluded. 1 broken.
