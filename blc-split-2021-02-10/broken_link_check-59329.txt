Getting links from: https://fsfe.org/contribute/web/tagging.bs.html
├─BROKEN─ https://fsfe.org/uk/uk.bs.html (HTTP_404)
├───OK─── https://fsfe.org/tags/tags.bs.html
├───OK─── https://fsfe.org/tags/tagged.bs.html
├───OK─── https://fsfe.org/contribute/web/css.bs.html
├───OK─── https://fsfe.org/contribute/template.bs.html
Finished! 53 links found. 48 excluded. 1 broken.
