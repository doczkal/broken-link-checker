Getting links from: https://fsfe.org/about/people/roy/roy.de.html
├───OK─── https://fsfe.org/activities/ftf/ftf.de.html
├─BROKEN─ https://fsfe.org/fr/index.de.html (HTTP_404)
├─BROKEN─ https://fsfe.org/legal (HTTP_404)
Finished! 80 links found. 77 excluded. 2 broken.
