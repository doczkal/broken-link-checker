Getting links from: https://fsfe.org/news/2008/index.en.html
├───OK─── https://fsfe.org/news/2008/news-20081215-01.en.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.en.html (HTTP_404)
├───OK─── https://fsfe.org/news/2008/news-20081208-01.en.html
├───OK─── https://fsfe.org/news/2008/gnu-25-years.en.html
├───OK─── https://fsfe.org/news/2008/news-20080305-01.en.html
├───OK─── https://fsfe.org/news/2008/news-20080301-01.en.html
├───OK─── https://fsfe.org/news/2008/news-20080222-01.en.html
├───OK─── https://fsfe.org/news/2008/news-20080214-01.en.html
├───OK─── https://fsfe.org/news/2008/news-20080118-01.en.html
Finished! 135 links found. 126 excluded. 1 broken.
