Getting links from: https://fsfe.org/news/nl/nl-201810.da.html
├─BROKEN─ https://fsfe.org/about/mancheva/mancheva (HTTP_404)
├───OK─── https://fsfe.org/news/2018/news-20181010-01.da.html
├───OK─── https://fsfe.org/news/2018/news-20180913-01.da.html
├───OK─── https://fsfe.org/tags/tagged-microsoft.da.html
├───OK─── https://fsfe.org/tags/tagged-digital-o-mat.da.html
Finished! 93 links found. 88 excluded. 1 broken.
