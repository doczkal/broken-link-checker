Getting links from: https://fsfe.org/news/nl/nl-201202.sv.html
├───OK─── https://fsfe.org/activities/nledu/nledu.sv.html
├───OK─── https://fsfe.org/news/2012/news-20120110-02.sv.html
├───OK─── https://fsfe.org/news/2012/news-20120130-01.sv.html
├───OK─── https://fsfe.org/news/2012/news-20120131-01.sv.html
├───OK─── https://fsfe.org/news/2012/news-20120110-01.sv.html
├───OK─── https://fsfe.org/news/2012/news-20120126-01.sv.html
├─BROKEN─ https://fsfe.org/activities/ilovefs/2012/banners.sv.html (HTTP_404)
├───OK─── https://fsfe.org/activities/ilovefs/2012/unperfekthaus.sv.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.sv.html (HTTP_404)
Finished! 109 links found. 100 excluded. 2 broken.
