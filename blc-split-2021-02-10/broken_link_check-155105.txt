Getting links from: https://fsfe.org/news/nl/nl-201502.sq.html
├───OK─── https://fsfe.org/news/2014/news-20140221-01.sq.html
├─BROKEN─ https://fsfe.org/about/ojasild/ojasild.sq.html (HTTP_404)
├───OK─── https://fsfe.org/about/people/gerloff/gerloff.sq.html
├─BROKEN─ https://fsfe.org/about/ojasild (HTTP_404)
Finished! 93 links found. 89 excluded. 2 broken.
