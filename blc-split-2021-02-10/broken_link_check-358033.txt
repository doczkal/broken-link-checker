Getting links from: https://fsfe.org/contact/index.et.html
├───OK─── https://fsfe.org/about/people/kirschner/index.et.html
├─BROKEN─ https://fsfe.org/about/codeofconduct/index.et.html (HTTP_404)
Finished! 86 links found. 84 excluded. 1 broken.
