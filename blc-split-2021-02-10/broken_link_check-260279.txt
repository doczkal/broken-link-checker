Getting links from: https://fsfe.org/news/2019/news-20191022-01.sl.html
├─BROKEN─ https://fsfe.org/news/2019/about/codeofconduct (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/contribute/web/web.sl.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/news-20191002.sl.html (HTTP_404)
├───OK─── https://fsfe.org/news/2019/news-20190701-01.sl.html
Finished! 222 links found. 218 excluded. 3 broken.
