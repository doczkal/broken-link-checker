Getting links from: https://fsfe.org/news/nl/nl-201704.ru.html
├───OK─── https://fsfe.org/news/2017/news-20170321-01.ru.html
├───OK─── https://fsfe.org/news/2017/news-20170116-01.ru.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.ru.html#id-fellowship-seats (HTTP_404)
Finished! 104 links found. 101 excluded. 1 broken.
