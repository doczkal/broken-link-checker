Getting links from: https://fsfe.org/news/nl/nl-201203.nn.html
├───OK─── https://fsfe.org/news/2012/news-20120228-01.nn.html
├───OK─── https://fsfe.org/news/2012/news-20120223-01.nn.html
├───OK─── https://fsfe.org/news/2012/news-20120214-01.nn.html
├───OK─── https://fsfe.org/news/2012/news-20120210-01.nn.html
├───OK─── https://fsfe.org/news/2012/news-20120204-01.nn.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.nn.html (HTTP_404)
Finished! 119 links found. 113 excluded. 1 broken.
