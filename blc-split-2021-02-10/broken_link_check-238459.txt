Getting links from: https://fsfe.org/news/nl/nl-201401.ar.html
├───OK─── https://fsfe.org/news/nl/nl-201401.de.html
├───OK─── https://fsfe.org/news/nl/nl-201401.sq.html
├─BROKEN─ https://fsfe.org/about/ojasild/ojasild.ar.html (HTTP_404)
Finished! 96 links found. 93 excluded. 1 broken.
