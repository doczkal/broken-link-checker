Getting links from: https://fsfe.org/news/2019/news-20191022-01.es.html
├───OK─── https://fsfe.org/activities/routers/timeline.es.html
├───OK─── https://fsfe.org/news/2019/news-20191007-01.es.html
├───OK─── https://fsfe.org/news/2019/news-20190329-01.es.html
├─BROKEN─ https://fsfe.org/news/2019/about/codeofconduct (HTTP_404)
├───OK─── https://fsfe.org/order/index.es.html
├─BROKEN─ https://fsfe.org/news/2019/contribute/web/web.es.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/news-20191002.es.html (HTTP_404)
├───OK─── https://fsfe.org/news/2019/news-20190701-01.es.html
├───OK─── https://fsfe.org/news/2019/news-20190927-01.es.html
├───OK─── https://fsfe.org/news/podcast/episode-1.es.html
├───OK─── https://fsfe.org/tags/tagged-sustainability.es.html
├───OK─── https://fsfe.org/tags/tagged-ngi.es.html
├───OK─── https://fsfe.org/tags/tagged-community.es.html
Finished! 223 links found. 210 excluded. 3 broken.
