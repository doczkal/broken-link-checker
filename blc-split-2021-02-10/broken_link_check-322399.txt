Getting links from: https://fsfe.org/news/nl/nl-201203.ca.html
├───OK─── https://fsfe.org/news/2012/news-20120228-01.ca.html
├───OK─── https://fsfe.org/news/2012/news-20120223-01.ca.html
├───OK─── https://fsfe.org/news/2012/news-20120214-01.ca.html
├───OK─── https://fsfe.org/news/2012/news-20120210-01.ca.html
├───OK─── https://fsfe.org/news/2012/news-20120204-01.ca.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.ca.html (HTTP_404)
Finished! 119 links found. 113 excluded. 1 broken.
