Getting links from: https://fsfe.org/news/nl/nl-201911.nl.html
├───OK─── https://fsfe.org/news/2019/news-20191113-01.nl.html
├───OK─── https://fsfe.org/activities/ftf/fla.nl.html
├───OK─── https://fsfe.org/news/2019/news-20191028-01.nl.html
├─BROKEN─ https://fsfe.org/nieuws/2019/nieuws-20191120-01.nl.html (HTTP_404)
Finished! 88 links found. 84 excluded. 1 broken.
