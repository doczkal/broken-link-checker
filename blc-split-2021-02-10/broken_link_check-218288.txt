Getting links from: https://fsfe.org/about/people/roy/roy.fr.html
├───OK─── https://fsfe.org/about/team.fr.html
├───OK─── https://fsfe.org/about/people/gerloff/index.fr.html
├─BROKEN─ https://fsfe.org/fr/index.fr.html (HTTP_404)
├─BROKEN─ https://fsfe.org/legal/index.fr.html (HTTP_404)
Finished! 79 links found. 75 excluded. 2 broken.
