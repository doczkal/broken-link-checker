Getting links from: https://fsfe.org/news/nl/nl-201310.de.html
├───OK─── https://fsfe.org/freesoftware/basics/gnuproject.de.html
├───OK─── https://fsfe.org/news/2013/news-20130926-01.de.html
├───OK─── https://fsfe.org/news/2013/news-20130918-01.de.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.de.html (HTTP_404)
Finished! 118 links found. 114 excluded. 1 broken.
