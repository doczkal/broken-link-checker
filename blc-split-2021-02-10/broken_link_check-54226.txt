Getting links from: https://fsfe.org/news/nl/nl-201302.nn.html
├───OK─── https://fsfe.org/news/2012/news-20121211-01.nn.html
├───OK─── https://fsfe.org/news/2012/report-2012.nn.html
├───OK─── https://fsfe.org/activities/ilovefs/2013/index.nn.html
├───OK─── https://fsfe.org/activities/ilovefs/2013/whylovefs.nn.html
├─BROKEN─ https://fsfe.org/activities/ilovefs/2013/banners.nn.html (HTTP_404)
├─BROKEN─ https://fsfe.org/order/promoorder.nn.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.nn.html (HTTP_404)
Finished! 119 links found. 112 excluded. 3 broken.
