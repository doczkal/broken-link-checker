Getting links from: https://fsfe.org/contribute/web/tagging.en.html
├───OK─── https://fsfe.org/contribute/web/index.en.html
├─BROKEN─ https://fsfe.org/uk/uk.en.html (HTTP_404)
├───OK─── https://fsfe.org/tags/tagged.en.html
Finished! 50 links found. 47 excluded. 1 broken.
