Getting links from: https://fsfe.org/news/nl/nl-201205.es.html
├─BROKEN─ https://fsfe.org/activities/swpat/swapt.es.html (HTTP_404)
├─BROKEN─ https://fsfe.org/activities/elections/askyourcandidates/askyourcandidtes.es.html (HTTP_404)
├───OK─── https://fsfe.org/news/2012/news-20120402-01.es.html
├─BROKEN─ https://fsfe.org/project/os/def.es.html (HTTP_404)
├───OK─── https://fsfe.org/freesoftware/standards/uk-standards-consultation.es.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.es.html (HTTP_404)
Finished! 113 links found. 107 excluded. 4 broken.
