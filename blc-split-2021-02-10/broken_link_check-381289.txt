Getting links from: https://fsfe.org/news/2009/news-20091118-01.sr.html
├───OK─── https://fsfe.org/news/2009/news-20091118-01.de.html
├───OK─── https://fsfe.org/news/2009/news-20091118-01.el.html
├───OK─── https://fsfe.org/news/2009/news-20091118-01.en.html
├───OK─── https://fsfe.org/news/2009/news-20091118-01.fr.html
├───OK─── https://fsfe.org/news/2009/news-20091118-01.it.html
├─BROKEN─ https://fsfe.org/com-pkg/com-pkg.sr.html (HTTP_404)
Finished! 67 links found. 61 excluded. 1 broken.
