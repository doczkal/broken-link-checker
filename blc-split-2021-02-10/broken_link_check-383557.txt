Getting links from: https://fsfe.org/contribute/web/tagging.de.html
├───OK─── https://fsfe.org/contribute/web/index.de.html
├─BROKEN─ https://fsfe.org/uk/uk.de.html (HTTP_404)
├───OK─── https://fsfe.org/contribute/template.de.html
Finished! 53 links found. 50 excluded. 1 broken.
