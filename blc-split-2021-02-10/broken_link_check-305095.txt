Getting links from: https://fsfe.org/news/nl/nl-201501.en.html
├───OK─── https://fsfe.org/fellowship/about/fellowship.en.html#youget
├─BROKEN─ https://fsfe.org/about/legal/constitution.en.html#id-fellowship-seats (HTTP_404)
├─BROKEN─ https://fsfe.org/about/ojasild (HTTP_404)
Finished! 85 links found. 82 excluded. 2 broken.
