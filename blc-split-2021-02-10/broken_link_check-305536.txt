Getting links from: https://fsfe.org/contribute/translators/translators.nn.html
├───OK─── https://fsfe.org/contribute/translators/index.nn.html
├───OK─── https://fsfe.org/index.nn.html
├───OK─── https://fsfe.org/about/about.nn.html
├───OK─── https://fsfe.org/activities/activities.nn.html
├───OK─── https://fsfe.org/contribute/contribute.nn.html
├───OK─── https://fsfe.org/press/press.nn.html
├───OK─── https://fsfe.org/news/news.nn.html
├─BROKEN─ https://fsfe.org/evets/events.nn.html (HTTP_404)
├───OK─── https://fsfe.org/news/newsletter.nn.html
├───OK─── https://fsfe.org/contribute/spreadtheword.nn.html
├───OK─── https://fsfe.org/contribute/translators/wordlist.nn.html
├───OK─── https://fsfe.org/about/js-licences.nn.html
├───OK─── https://fsfe.org/contact/contact.nn.html
├───OK─── https://fsfe.org/about/legal/imprint.nn.html
├───OK─── https://fsfe.org/about/transparency-commitment.nn.html
├───OK─── https://fsfe.org/contribute/web/web.nn.html
Finished! 86 links found. 70 excluded. 1 broken.
