Getting links from: https://fsfe.org/news/nl/nl-201106.ca.html
├───OK─── https://fsfe.org/freesoftware/standards/bt-open-letter.ca.html
├───OK─── https://fsfe.org/news/2011/news-20110511-01.ca.html
├───OK─── https://fsfe.org/news/2011/news-20110520-01.ca.html
├─BROKEN─ https://fsfe.org/source/activities/elections/askyourcandidates/askyourcandidates.xhtml (HTTP_404)
Finished! 92 links found. 88 excluded. 1 broken.
