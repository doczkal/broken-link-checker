Getting links from: https://fsfe.org/news/nl/nl-201704.es.html
├───OK─── https://fsfe.org/news/2017/news-20170321-01.es.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.es.html#id-fellowship-seats (HTTP_404)
Finished! 99 links found. 97 excluded. 1 broken.
