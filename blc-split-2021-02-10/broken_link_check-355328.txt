Getting links from: https://fsfe.org/news/nl/nl-201502.cs.html
├───OK─── https://fsfe.org/news/2014/news-20140221-01.cs.html
├─BROKEN─ https://fsfe.org/about/ojasild/ojasild.cs.html (HTTP_404)
├─BROKEN─ https://fsfe.org/about/ojasild (HTTP_404)
Finished! 95 links found. 92 excluded. 2 broken.
