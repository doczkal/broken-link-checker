Getting links from: https://fsfe.org/news/2019/news-20191022-01.ro.html#ngi0
├───OK─── https://fsfe.org/activities/routers/timeline.ro.html
├───OK─── https://fsfe.org/news/2019/news-20190514-01.ro.html
├───OK─── https://fsfe.org/news/2019/news-20190515-01.ro.html
├───OK─── https://fsfe.org/news/2019/news-20190515-02.ro.html
├───OK─── https://fsfe.org/activities/radiodirective/statement.ro.html
├───OK─── https://fsfe.org/about/people/interviews/grun.ro.html#interview
├─BROKEN─ https://fsfe.org/news/2019/about/codeofconduct (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/contribute/web/web.ro.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/news-20191002.ro.html (HTTP_404)
├───OK─── https://fsfe.org/news/2019/news-20190701-01.ro.html
├───OK─── https://fsfe.org/tags/tagged-radiodirective.ro.html
├───OK─── https://fsfe.org/tags/tagged-savecodeshare.ro.html
Finished! 222 links found. 210 excluded. 3 broken.
