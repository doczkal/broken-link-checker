Getting links from: https://fsfe.org/contact/contact.bs.html
├───OK─── https://fsfe.org/about/people/kirschner/index.bs.html
├─BROKEN─ https://fsfe.org/about/codeofconduct/index.bs.html (HTTP_404)
Finished! 86 links found. 84 excluded. 1 broken.
