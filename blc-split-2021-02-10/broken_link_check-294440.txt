Getting links from: https://fsfe.org/news/nl/nl-201302.zh.html
├───OK─── https://fsfe.org/activities/ilovefs/2013/index.zh.html
├───OK─── https://fsfe.org/activities/ilovefs/2013/whylovefs.zh.html
├─BROKEN─ https://fsfe.org/activities/ilovefs/2013/banners.zh.html (HTTP_404)
├─BROKEN─ https://fsfe.org/order/promoorder.zh.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.zh.html (HTTP_404)
Finished! 119 links found. 114 excluded. 3 broken.
