Getting links from: https://fsfe.org/news/nl/nl-201112.mk.html
├───OK─── https://fsfe.org/news/2011/news-20111128-01.mk.html
├───OK─── https://fsfe.org/activities/pdfreaders/buglist.mk.html
├───OK─── https://fsfe.org/activities/pdfreaders/letter.mk.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.mk.html (HTTP_404)
Finished! 108 links found. 104 excluded. 1 broken.
