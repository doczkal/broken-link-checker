Getting links from: https://fsfe.org/news/2017/index.en.html
├───OK─── https://fsfe.org/activities/radiodirective/index.en.html
├───OK─── https://fsfe.org/news/2017/news-20171211-01.en.html
├───OK─── https://fsfe.org/activities/ftf/fla.en.html
├───OK─── https://fsfe.org/news/2017/news-20170906-01.en.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.en.html#id-fellowship-seats (HTTP_404)
├───OK─── https://fsfe.org/news/2017/news-20170116-01.en.html
├───OK─── https://fsfe.org/news/2017/news-20170109-01.en.html
Finished! 297 links found. 289 excluded. 2 broken.
