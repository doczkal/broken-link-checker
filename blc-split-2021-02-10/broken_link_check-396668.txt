Getting links from: https://fsfe.org/contact/index.nl.html
├───OK─── https://fsfe.org/about/people/kirschner/index.nl.html
├─BROKEN─ https://fsfe.org/about/codeofconduct/index.nl.html (HTTP_404)
├───OK─── https://fsfe.org/news/news.nl.rss
Finished! 83 links found. 80 excluded. 1 broken.
