Getting links from: https://fsfe.org/news/nl/nl-201106.mk.html
├───OK─── https://fsfe.org/news/nl/nl-201106.it.html
├───OK─── https://fsfe.org/news/nl/nl-201106.nl.html
├───OK─── https://fsfe.org/activities/ms-vs-eu/timeline.mk.html
├───OK─── https://fsfe.org/news/2011/news-20110520-01.mk.html
├─BROKEN─ https://fsfe.org/source/activities/elections/askyourcandidates/askyourcandidates.xhtml (HTTP_404)
Finished! 92 links found. 87 excluded. 1 broken.
