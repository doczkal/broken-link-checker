Getting links from: https://fsfe.org/news/nl/nl-201308.ru.html#
├───OK─── https://fsfe.org/news/nl/nl-201308.de.html
├─BROKEN─ https://fsfe.org/news/2013/2013-07-26_Open_Letter_to_NEC.ru.html (HTTP_404)
├───OK─── https://fsfe.org/freesoftware/standards/transparency-letter.ru.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.ru.html (HTTP_404)
Finished! 113 links found. 109 excluded. 2 broken.
