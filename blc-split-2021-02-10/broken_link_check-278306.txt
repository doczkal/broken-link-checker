Getting links from: https://fsfe.org/news/nl/nl-201704.pl.html#
├───OK─── https://fsfe.org/news/2017/news-20170328-01.pl.html
├───OK─── https://fsfe.org/associates/associates.pl.html
├───OK─── https://fsfe.org/news/2017/news-20170321-01.pl.html
├───OK─── https://fsfe.org/news/2017/news-20170116-01.pl.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.pl.html#id-fellowship-seats (HTTP_404)
Finished! 103 links found. 98 excluded. 1 broken.
