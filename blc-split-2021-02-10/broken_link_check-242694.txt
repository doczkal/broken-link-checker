Getting links from: https://fsfe.org/news/2017/news-20171107-01.nl.html
├─BROKEN─ https://fsfe.org/about/legal/constitution#id-fellowship-seats (HTTP_404)
├───OK─── https://fsfe.org/tags/tagged-ga.nl.html
├───OK─── https://fsfe.org/tags/tagged-coc.nl.html
Finished! 67 links found. 64 excluded. 1 broken.
