Getting links from: https://fsfe.org/news/nl/nl-201310.sv.html
├───OK─── https://fsfe.org/freesoftware/basics/gnuproject.sv.html
├───OK─── https://fsfe.org/news/2013/news-20130926-01.sv.html
├───OK─── https://fsfe.org/news/2013/news-20130918-01.sv.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.sv.html (HTTP_404)
Finished! 120 links found. 116 excluded. 1 broken.
