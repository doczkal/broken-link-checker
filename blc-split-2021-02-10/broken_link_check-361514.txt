Getting links from: https://fsfe.org/news/2018/news-20180526-01.hu.html
├───OK─── https://fsfe.org/news/2018/news-20180526-01.fr.html
├─BROKEN─ https://fsfe.org/about/legal/constitution#id-fellowship-seats (HTTP_404)
├───OK─── https://fsfe.org/tags/tagged-ga.hu.html
├───OK─── https://fsfe.org/tags/tagged-fellowship.hu.html
Finished! 67 links found. 63 excluded. 1 broken.
