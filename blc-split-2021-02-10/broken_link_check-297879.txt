Getting links from: https://fsfe.org/freesoftware/education/edu-related-content.sk.html
├───OK─── https://fsfe.org/activities/mankind/lsm2002/index.sk.html
├─BROKEN─ https://fsfe.org/associates/ofset/associate (HTTP_404)
├───OK─── https://fsfe.org/activities/mankind/lsm2002/press-release.sk.html
├───OK─── https://fsfe.org/freesoftware/transcripts/rms-fs-2006-03-09.sk.html
├─BROKEN─ https://fsfe.org/freesoftware/education/news/2012/news-20121217-01 (HTTP_404)
├───OK─── https://fsfe.org/activities/tgs/tgs.sk.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool1.sk.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool2.sk.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool3.sk.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool4.sk.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool5.sk.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool6.sk.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool7.sk.html
Finished! 111 links found. 98 excluded. 2 broken.
