Getting links from: https://fsfe.org/news/nl/nl-201203.cs.html
├───OK─── https://fsfe.org/news/2012/news-20120228-01.cs.html
├───OK─── https://fsfe.org/news/2012/news-20120210-01.cs.html
├───OK─── https://fsfe.org/news/2012/news-20120204-01.cs.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.cs.html (HTTP_404)
Finished! 119 links found. 115 excluded. 1 broken.
