Getting links from: https://fsfe.org/about/people/testimonials.ar.html
├───OK─── https://fsfe.org/about/people/interviews/cryptie.ar.html#video-30s-cryptie
├───OK─── https://fsfe.org/about/people/interviews/zerolo.ar.html#video-30s-tomas
├─BROKEN─ https://fsfe.org/about/people/interviews/bernhard.ar.html#video-30s-bernhard (HTTP_404)
├───OK─── https://fsfe.org/about/people/interviews/weitzhofer.ar.html#interview
├───OK─── https://fsfe.org/about/people/interviews/grun.ar.html#interview
├───OK─── https://fsfe.org/about/people/interviews/ockers.ar.html#video-30s-andre
├───OK─── https://fsfe.org/about/people/interviews/mueller.ar.html#interview
├───OK─── https://fsfe.org/about/people/interviews/snow.ar.html#interview
├───OK─── https://fsfe.org/about/people/interviews/gkotsopoulou.ar.html#interview
Finished! 123 links found. 114 excluded. 1 broken.
