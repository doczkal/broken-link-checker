Getting links from: https://fsfe.org/news/nl/nl-201106.sr.html
├───OK─── https://fsfe.org/activities/ms-vs-eu/timeline.sr.html
├───OK─── https://fsfe.org/freesoftware/standards/bt-open-letter.sr.html
├───OK─── https://fsfe.org/news/2011/news-20110511-01.sr.html
├───OK─── https://fsfe.org/news/2011/news-20110520-01.sr.html
├─BROKEN─ https://fsfe.org/source/activities/elections/askyourcandidates/askyourcandidates.xhtml (HTTP_404)
Finished! 92 links found. 87 excluded. 1 broken.
