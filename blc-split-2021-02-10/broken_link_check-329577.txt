Getting links from: https://fsfe.org/news/2008/index.tr.html
├───OK─── https://fsfe.org/news/2008/news-20081215-01.tr.html
├───OK─── https://fsfe.org/news/2008/news-20081210-01.tr.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.tr.html (HTTP_404)
├───OK─── https://fsfe.org/news/2008/news-20081208-01.tr.html
├───OK─── https://fsfe.org/news/2008/gnu-25-years.tr.html
├───OK─── https://fsfe.org/news/2008/news-20080305-01.tr.html
├───OK─── https://fsfe.org/news/2008/news-20080301-01.tr.html
├───OK─── https://fsfe.org/news/2008/news-20080222-01.tr.html
├───OK─── https://fsfe.org/news/2008/news-20080214-01.tr.html
├───OK─── https://fsfe.org/news/2008/news-20080118-01.tr.html
Finished! 138 links found. 128 excluded. 1 broken.
