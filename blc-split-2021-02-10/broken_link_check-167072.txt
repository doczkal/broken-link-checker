Getting links from: https://fsfe.org/activities/igf/wgig.fr.html
├───OK─── https://fsfe.org/activities/igf/wgig.el.html
├───OK─── https://fsfe.org/activities/igf/wgig.nl.html
├─BROKEN─ https://fsfe.org/activities/igf/WGIG-WP-Cybercrime-Comments.pdf (HTTP_404)
├─BROKEN─ https://fsfe.org/activities/igf/WGIG-WP-IPR-Comments.pdf (HTTP_404)
Finished! 62 links found. 58 excluded. 2 broken.
