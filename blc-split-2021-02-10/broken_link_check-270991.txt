Getting links from: https://fsfe.org/news/nl/nl-201704.nb.html#
├───OK─── https://fsfe.org/news/2017/news-20170321-01.nb.html
├───OK─── https://fsfe.org/news/2017/news-20170116-01.nb.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.nb.html#id-fellowship-seats (HTTP_404)
Finished! 103 links found. 100 excluded. 1 broken.
