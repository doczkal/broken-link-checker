Getting links from: https://fsfe.org/news/2013/index.sk.html
├───OK─── https://fsfe.org/news/2013/news-20131213-01.sk.html
├───OK─── https://fsfe.org/timeline/timeline.sk.html
├───OK─── https://fsfe.org/news/2013/news-20130927-01.sk.html
├───OK─── https://fsfe.org/tags/tagged-gnu.sk.html
├───OK─── https://fsfe.org/tags/tagged-united-nations.sk.html
├───OK─── https://fsfe.org/news/2013/news-20130920-01.sk.html
├─BROKEN─ https://fsfe.org/news/2013/2013-07-26_Open_Letter_to_NEC.sk.html (HTTP_404)
├───OK─── https://fsfe.org/tags/tagged-ee.sk.html
├───OK─── https://fsfe.org/news/2013/news-20130712-01.sk.html
├───OK─── https://fsfe.org/news/2013/news-20130625-02.sk.html
├───OK─── https://fsfe.org/tags/tagged-tr.sk.html
├───OK─── https://fsfe.org/tags/tagged-google.sk.html
├───OK─── https://fsfe.org/news/nl/nl-201305.sk.html
├───OK─── https://fsfe.org/news/2013/news-20130424-01.sk.html
├───OK─── https://fsfe.org/tags/tagged-ro.sk.html
├───OK─── https://fsfe.org/news/2013/news-20130319-01.sk.html
├───OK─── https://fsfe.org/news/2013/news-20130226-01.sk.html
Finished! 289 links found. 272 excluded. 1 broken.
