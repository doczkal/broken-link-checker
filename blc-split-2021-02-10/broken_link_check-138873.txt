Getting links from: https://fsfe.org/news/2019/news-20191022-01.it.html
├───OK─── https://fsfe.org/news/2019/news-20191022-01.en.html
├───OK─── https://fsfe.org/news/2019/news-20191022-01.nl.html
├─BROKEN─ https://fsfe.org/news/2019/about/codeofconduct (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/contribute/web/web.it.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/news-20191002.it.html (HTTP_404)
Finished! 222 links found. 217 excluded. 3 broken.
