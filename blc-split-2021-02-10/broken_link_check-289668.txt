Getting links from: https://fsfe.org/news/nl/nl-201810.es.html
├───OK─── https://fsfe.org/news/2018/news-20181023-02.es.html
├─BROKEN─ https://fsfe.org/about/mancheva/mancheva (HTTP_404)
├───OK─── https://fsfe.org/news/2018/news-20181010-01.es.html
Finished! 94 links found. 91 excluded. 1 broken.
