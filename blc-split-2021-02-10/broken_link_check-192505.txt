Getting links from: https://fsfe.org/news/2018/news-20180526-01.sl.html
├───OK─── https://fsfe.org/about/people/avatars/albers.jpg
├─BROKEN─ https://fsfe.org/about/legal/constitution#id-fellowship-seats (HTTP_404)
Finished! 67 links found. 65 excluded. 1 broken.
