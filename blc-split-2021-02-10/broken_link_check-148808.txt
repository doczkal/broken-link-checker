Getting links from: https://fsfe.org/news/nl/nl-201704.ca.html
├───OK─── https://fsfe.org/news/2017/news-20170328-01.ca.html
├───OK─── https://fsfe.org/associates/associates.ca.html
├───OK─── https://fsfe.org/activities/radiodirective/index.ca.html
├───OK─── https://fsfe.org/news/2017/news-20170315-01.ca.html
├───OK─── https://fsfe.org/activities/ilovefs/whylovefs/gallery.ca.html
├───OK─── https://fsfe.org/news/2017/graphics/ilovefs-roses.jpg
├───OK─── https://fsfe.org/news/2017/news-20170321-01.ca.html
├───OK─── https://fsfe.org/news/2017/news-20170116-01.ca.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.ca.html#id-fellowship-seats (HTTP_404)
Finished! 103 links found. 94 excluded. 1 broken.
