Getting links from: https://fsfe.org/news/nl/nl-201202.tr.html
├───OK─── https://fsfe.org/news/2012/news-20120110-02.tr.html
├───OK─── https://fsfe.org/news/2012/news-20120130-01.tr.html
├───OK─── https://fsfe.org/activities/ilovefs/2012/index.tr.html
├─BROKEN─ https://fsfe.org/activities/ilovefs/2012/banners.tr.html (HTTP_404)
├───OK─── https://fsfe.org/activities/ilovefs/2012/unperfekthaus.tr.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.tr.html (HTTP_404)
Finished! 109 links found. 103 excluded. 2 broken.
