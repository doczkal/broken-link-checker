Getting links from: https://fsfe.org/news/nl/nl-201202.el.html
├───OK─── https://fsfe.org/activities/nledu/nledu.el.html
├───OK─── https://fsfe.org/news/2012/news-20120130-01.el.html
├───OK─── https://fsfe.org/news/2012/news-20120131-01.el.html
├───OK─── https://fsfe.org/news/2012/news-20120110-01.el.html
├─BROKEN─ https://fsfe.org/activities/ilovefs/2012/banners.el.html (HTTP_404)
├───OK─── https://fsfe.org/activities/ilovefs/2012/unperfekthaus.el.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.el.html (HTTP_404)
Finished! 107 links found. 100 excluded. 2 broken.
