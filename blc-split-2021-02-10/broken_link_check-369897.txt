Getting links from: https://fsfe.org/news/2007/news-20070630-01.el.html
├───OK─── https://fsfe.org/news/2007/news-20070630-01.en.html
├───OK─── https://fsfe.org/activities/ipred2/index.el.html
├─BROKEN─ https://fsfe.org/ftf (HTTP_404)
├───OK─── https://fsfe.org/activities/gplv3/index.el.html#transcripts
├───OK─── https://fsfe.org/activities/self/index.el.html
├─BROKEN─ https://fsfe.org/fellows/greve/img/zrh_hoist (HTTP_404)
├─BROKEN─ https://fsfe.org/fellows/meetings/2006_bolzano (HTTP_404)
Finished! 102 links found. 95 excluded. 3 broken.
