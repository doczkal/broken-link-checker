Getting links from: https://fsfe.org/contribute/web/tagging.ar.html
├─BROKEN─ https://fsfe.org/uk/uk.ar.html (HTTP_404)
├───OK─── https://fsfe.org/tags/tagged.ar.html
├───OK─── https://fsfe.org/contribute/web/css.ar.html
├───OK─── https://fsfe.org/contribute/template.ar.html
Finished! 54 links found. 50 excluded. 1 broken.
