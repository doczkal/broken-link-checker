Getting links from: https://fsfe.org/news/nl/nl-201309.mk.html
├───OK─── https://fsfe.org/news/nl/nl-201309.el.html
├───OK─── https://fsfe.org/news/nl/nl-201309.fr.html
├───OK─── https://fsfe.org/activities/msooxml/msooxml-interoperability.mk.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.mk.html (HTTP_404)
Finished! 117 links found. 113 excluded. 1 broken.
