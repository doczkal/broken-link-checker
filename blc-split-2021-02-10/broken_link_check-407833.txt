Getting links from: https://fsfe.org/news/2007/news-20070630-01.de.html#
├───OK─── https://fsfe.org/activities/ms-vs-eu/index.de.html
├───OK─── https://fsfe.org/activities/ipred2/index.de.html
├───OK─── https://fsfe.org/activities/wsis/index.de.html
├───OK─── https://fsfe.org/activities/wipo/index.de.html
├───OK─── https://fsfe.org/activities/wipo/statement-20050721.de.html
├─BROKEN─ https://fsfe.org/ftf (HTTP_404)
├───OK─── https://fsfe.org/activities/gplv3/index.de.html#transcripts
├───OK─── https://fsfe.org/activities/self/index.de.html
├─BROKEN─ https://fsfe.org/fellows/greve/img/zrh_hoist (HTTP_404)
├─BROKEN─ https://fsfe.org/fellows/meetings/2006_bolzano (HTTP_404)
├───OK─── https://fsfe.org/contribute/index.de.html
Finished! 104 links found. 93 excluded. 3 broken.
