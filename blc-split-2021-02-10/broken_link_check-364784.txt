Getting links from: https://fsfe.org/news/nl/nl-201501.sq.html
├───OK─── https://fsfe.org/fellowship/index.sq.html
├───OK─── https://fsfe.org/fellowship/about/fellowship.sq.html#youget
├─BROKEN─ https://fsfe.org/about/legal/constitution.sq.html#id-fellowship-seats (HTTP_404)
├─BROKEN─ https://fsfe.org/about/ojasild (HTTP_404)
Finished! 86 links found. 82 excluded. 2 broken.
