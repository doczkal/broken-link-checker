Getting links from: https://fsfe.org/freesoftware/education/edu-related-content.sr.html
├───OK─── https://fsfe.org/activities/mankind/lsm2002/index.sr.html
├─BROKEN─ https://fsfe.org/associates/ofset/associate (HTTP_404)
├───OK─── https://fsfe.org/activities/mankind/lsm2002/press-release.sr.html
├───OK─── https://fsfe.org/activities/policy.sr.html
├───OK─── https://fsfe.org/news/2011/news-20111128-02.sr.html#education
├───OK─── https://fsfe.org/news/2001/article2001-12-17-01.sr.html
├───OK─── https://fsfe.org/freesoftware/transcripts/rms-fs-2006-03-09.sr.html
├───OK─── https://fsfe.org/events/2004/FISL/fisl.sr.html
├───OK─── https://fsfe.org/events/events.sr.html#event-20120426
├─BROKEN─ https://fsfe.org/freesoftware/education/news/2012/news-20121217-01 (HTTP_404)
├───OK─── https://fsfe.org/freesoftware/standards/guardian-open-letter.sr.html
├───OK─── https://fsfe.org/news/nl/nl-201101.sr.html
├───OK─── https://fsfe.org/news/2003/lettera_MIUR-2003-07-16.sr.html
├───OK─── https://fsfe.org/activities/tgs/tgs.sr.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool1.sr.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool2.sr.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool3.sr.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool4.sr.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool5.sr.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool6.sr.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool7.sr.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool8.sr.html
Finished! 111 links found. 89 excluded. 2 broken.
