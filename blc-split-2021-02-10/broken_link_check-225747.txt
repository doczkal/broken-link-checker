Getting links from: https://fsfe.org/news/nl/nl-202005.es.html
├───OK─── https://fsfe.org/news/podcast/episode-special-1.es.html
├───OK─── https://fsfe.org/news/2020/news-20200427-01.es.html
├─BROKEN─ https://fsfe.org/https:/librezoom.net/lz19-die-nexte-bitte/index.es.html (HTTP_404)
├───OK─── https://fsfe.org/tags/tagged-limux.es.html
Finished! 94 links found. 90 excluded. 1 broken.
