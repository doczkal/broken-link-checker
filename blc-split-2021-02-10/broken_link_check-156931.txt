Getting links from: https://fsfe.org/contact/contact.nb.html
├───OK─── https://fsfe.org/about/people/kirschner/index.nb.html
├─BROKEN─ https://fsfe.org/about/codeofconduct/index.nb.html (HTTP_404)
Finished! 86 links found. 84 excluded. 1 broken.
