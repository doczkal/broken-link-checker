Getting links from: https://fsfe.org/activities/gplv3/europe-gplv3-conference.sr.html
├───OK─── https://fsfe.org/activities/gplv3/europe-gplv3-conference.nl.html
├─BROKEN─ https://fsfe.org/activities/gplv3/GPLv3-logo-red.png (HTTP_404)
├───OK─── https://fsfe.org/about/oriordan/oriordan.sr.html
├───OK─── https://fsfe.org/about/maffulli/maffulli.sr.html
├───OK─── https://fsfe.org/activities/gplv3/barcelona-summaries#greve
Finished! 103 links found. 98 excluded. 1 broken.
