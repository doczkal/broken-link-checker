Getting links from: https://fsfe.org/about/people/testimonials.et.html#guenther
├───OK─── https://fsfe.org/about/people/interviews/cryptie.et.html#video-30s-cryptie
├───OK─── https://fsfe.org/about/people/interviews/zerolo.et.html#video-30s-tomas
├─BROKEN─ https://fsfe.org/about/people/interviews/bernhard.et.html#video-30s-bernhard (HTTP_404)
├───OK─── https://fsfe.org/about/people/interviews/weitzhofer.et.html#interview
├───OK─── https://fsfe.org/about/people/interviews/grun.et.html#interview
├───OK─── https://fsfe.org/about/people/interviews/ockers.et.html#video-30s-andre
├───OK─── https://fsfe.org/about/people/interviews/mueller.et.html#interview
├───OK─── https://fsfe.org/about/people/interviews/snow.et.html#interview
├───OK─── https://fsfe.org/about/people/interviews/gkotsopoulou.et.html#interview
Finished! 122 links found. 113 excluded. 1 broken.
