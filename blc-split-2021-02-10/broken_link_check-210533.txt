Getting links from: https://fsfe.org/activities/gplv3/europe-gplv3-conference.pt.html
├─BROKEN─ https://fsfe.org/activities/gplv3/GPLv3-logo-red.png (HTTP_404)
├───OK─── https://fsfe.org/about/oriordan/oriordan.pt.html
├───OK─── https://fsfe.org/about/maffulli/maffulli.pt.html
├───OK─── https://fsfe.org/activities/gplv3/barcelona-rms-transcript.pt.html
├───OK─── https://fsfe.org/activities/gplv3/barcelona-moglen-transcript.pt.html
Finished! 101 links found. 96 excluded. 1 broken.
