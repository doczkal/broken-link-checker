Getting links from: https://fsfe.org/news/nl/nl-201305.et.html
├───OK─── https://fsfe.org/news/nl/nl-201305.de.html
├───OK─── https://fsfe.org/news/2013/news-20130424-01.et.html
├───OK─── https://fsfe.org/news/2013/news-20130422-01.et.html
├───OK─── https://fsfe.org/news/2013/news-20130319-01.et.html
├───OK─── https://fsfe.org/news/2013/news-20130423-02.et.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.et.html (HTTP_404)
├───OK─── https://fsfe.org/about/people/albers
Finished! 107 links found. 100 excluded. 1 broken.
