Getting links from: https://fsfe.org/news/nl/nl-201212.hu.html
├───OK─── https://fsfe.org/news/2012/news-20121101-02.hu.html
├───OK─── https://fsfe.org/news/2012/news-20121116-01.hu.html
├─BROKEN─ https://fsfe.org/news/nl/lwn.net/Articles/523537/index.hu.html (HTTP_404)
├───OK─── https://fsfe.org/news/2012/news-20121112-01.hu.html
├───OK─── https://fsfe.org/news/2010/news-20100324-01.hu.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.hu.html (HTTP_404)
Finished! 115 links found. 109 excluded. 2 broken.
