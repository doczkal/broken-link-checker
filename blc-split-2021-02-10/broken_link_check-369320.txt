Getting links from: https://fsfe.org/about/people/testimonials.mk.html#kroah-hartman
├───OK─── https://fsfe.org/about/people/interviews/cryptie.mk.html#video-30s-cryptie
├───OK─── https://fsfe.org/about/people/interviews/lequertier.mk.html#video-30s-vincent
├───OK─── https://fsfe.org/about/people/interviews/zerolo.mk.html#video-30s-tomas
├─BROKEN─ https://fsfe.org/about/people/interviews/bernhard.mk.html#video-30s-bernhard (HTTP_404)
├───OK─── https://fsfe.org/about/people/interviews/weitzhofer.mk.html#interview
├───OK─── https://fsfe.org/about/people/interviews/grun.mk.html#interview
├───OK─── https://fsfe.org/about/people/interviews/ockers.mk.html#video-30s-andre
├───OK─── https://fsfe.org/about/people/interviews/mueller.mk.html#interview
├───OK─── https://fsfe.org/about/people/interviews/snow.mk.html#interview
├───OK─── https://fsfe.org/about/people/interviews/gkotsopoulou.mk.html#interview
Finished! 122 links found. 112 excluded. 1 broken.
