Getting links from: https://fsfe.org/donate/letter-2009.sr.html
├───OK─── https://fsfe.org/activities/ftf/ftf.sr.html
├─BROKEN─ https://fsfe.org/com-pkg/com-pkg.sr.html (HTTP_404)
├─BROKEN─ https://fsfe.org/com-pkg/com-pkg.sr.html (HTTP_404)
├───OK─── https://fsfe.org/contact/local.sr.html
├───OK─── https://fsfe.org/about/people/gerloff/gerloff.sr.html
Finished! 71 links found. 66 excluded. 2 broken.
