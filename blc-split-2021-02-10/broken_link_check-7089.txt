Getting links from: https://fsfe.org/news/nl/nl-201308.sq.html
├─BROKEN─ https://fsfe.org/news/2013/2013-07-26_Open_Letter_to_NEC.sq.html (HTTP_404)
├───OK─── https://fsfe.org/news/2013/news-20130712-01.sq.html
├───OK─── https://fsfe.org/freesoftware/standards/transparency-letter.sq.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.sq.html (HTTP_404)
Finished! 112 links found. 108 excluded. 2 broken.
