Getting links from: https://fsfe.org/contribute/spreadtheword.fr.html
├───OK─── https://fsfe.org/contribute/promopics/fsfe-sticker-thumb.png
├───OK─── https://fsfe.org/contribute/promopics/free-software-multilanguage-logocolours.png
├───OK─── https://fsfe.org/contribute/promopics/free-software-multilanguage-purple.png
├───OK─── https://fsfe.org/contribute/promopics/hacking-100-freedom-sticker-blue.png
├───OK─── https://fsfe.org/contribute/promopics/hacking-100-freedom-sticker-darkblue.png
├───OK─── https://fsfe.org/contribute/promopics/hacking-100-freedom-sticker-green.png
├───OK─── https://fsfe.org/contribute/promopics/freiheit-thumb.png
├───OK─── https://fsfe.org/contribute/promopics/100-freedoms-sticker-black.png
├───OK─── https://fsfe.org/contribute/promopics/100-freedoms-sticker-orange.png
├───OK─── https://fsfe.org/contribute/promopics/gnupg-leaflet-en-thumb.png
├───OK─── https://fsfe.org/contribute/promopics/drm_thumb.png
├───OK─── https://fsfe.org/contribute/promopics/pmpc_brochure_cover.jpg
├───OK─── https://fsfe.org/contribute/promopics/pmpc-logo-sticker-thumb.png
├───OK─── https://fsfe.org/contribute/promopics/pmpc_postcard_front_thumb.png
├───OK─── https://fsfe.org/contribute/promopics/pmpc_poster_combined_thumb.png
├───OK─── https://fsfe.org/contribute/promopics/fya-sticker-2012-3cm.png
├───OK─── https://fsfe.org/contribute/promopics/ilovefs-sticker_thumb.png
├─BROKEN─ https://fsfe.org/contribute/activities/ilovefs/index.fr.html (HTTP_404)
├───OK─── https://fsfe.org/contribute/promopics/ilovefs_flyer_A7_thumb.png
├───OK─── https://fsfe.org/contribute/promopics/ilovefs_poster_A1_thumb.png
├───OK─── https://fsfe.org/contribute/promopics/ilovefs-balloon-combined-thumb.png
├───OK─── https://fsfe.org/contribute/promopics/thereisnocloud-bluecolor-preview.png
├───OK─── https://fsfe.org/contribute/promopics/thereisnocloud-bw-preview.png
├───OK─── https://fsfe.org/contribute/promopics/thereisnocloud-postcard-thumb.png
├───OK─── https://fsfe.org/contribute/promopics/thereisnocloud-poster-thumb.png
├───OK─── https://fsfe.org/contribute/promopics/a2.mu_thumb.png
├───OK─── https://fsfe.org/contribute/promopics/open_formats_poster_ENthumb.png
Finished! 275 links found. 248 excluded. 1 broken.
