Getting links from: https://fsfe.org/activities/igf/wgig.fr.html
├─BROKEN─ https://fsfe.org/activities/igf/WGIG-WP-Cybercrime-Comments.pdf (HTTP_404)
├─BROKEN─ https://fsfe.org/activities/igf/WGIG-WP-IPR-Comments.pdf (HTTP_404)
├───OK─── https://fsfe.org/activities/igf/index.fr.html
Finished! 62 links found. 59 excluded. 2 broken.
