Getting links from: https://fsfe.org/news/nl/nl-201210.fi.html#
├───OK─── https://fsfe.org/news/nl/nl-201210.cs.html
├───OK─── https://fsfe.org/news/nl/nl-201210.it.html
├───OK─── https://fsfe.org/news/2012/news-20120925-01.fi.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.fi.html (HTTP_404)
Finished! 124 links found. 119 excluded. 2 broken.
