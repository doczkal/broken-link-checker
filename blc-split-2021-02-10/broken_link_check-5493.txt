Getting links from: https://fsfe.org/news/nl/nl-201205.fi.html
├─BROKEN─ https://fsfe.org/activities/swpat/swapt.fi.html (HTTP_404)
├─BROKEN─ https://fsfe.org/project/os/def.fi.html (HTTP_404)
├───OK─── https://fsfe.org/freesoftware/standards/uk-standards-consultation.fi.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.fi.html (HTTP_404)
Finished! 115 links found. 111 excluded. 3 broken.
