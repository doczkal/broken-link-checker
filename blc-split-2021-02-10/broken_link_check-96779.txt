Getting links from: https://fsfe.org/news/nl/nl-201302.tr.html#
├───OK─── https://fsfe.org/news/2012/news-20121211-01.tr.html
├───OK─── https://fsfe.org/news/2012/report-2012.tr.html
├───OK─── https://fsfe.org/activities/ilovefs/2013/index.tr.html
├───OK─── https://fsfe.org/activities/ilovefs/2013/whylovefs.tr.html
├─BROKEN─ https://fsfe.org/activities/ilovefs/2013/banners.tr.html (HTTP_404)
├─BROKEN─ https://fsfe.org/order/promoorder.tr.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.tr.html (HTTP_404)
Finished! 119 links found. 112 excluded. 3 broken.
