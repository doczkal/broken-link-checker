Getting links from: https://fsfe.org/news/nl/nl-201501.fr.html
├───OK─── https://fsfe.org/fellowship/about/fellowship.fr.html#youget
├─BROKEN─ https://fsfe.org/about/legal/constitution.fr.html#id-fellowship-seats (HTTP_404)
├───OK─── https://fsfe.org/news/2014/news-20141212-01.fr.html
├─BROKEN─ https://fsfe.org/about/ojasild (HTTP_404)
Finished! 86 links found. 82 excluded. 2 broken.
