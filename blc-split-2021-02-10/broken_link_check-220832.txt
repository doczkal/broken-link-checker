Getting links from: https://fsfe.org/about/people/roy/index.pt.html
├───OK─── https://fsfe.org/about/team.pt.html
├───OK─── https://fsfe.org/contribute/internship.pt.html
├─BROKEN─ https://fsfe.org/fr/index.pt.html (HTTP_404)
├─BROKEN─ https://fsfe.org/legal (HTTP_404)
├───OK─── https://fsfe.org/fellowship/index.pt.html
Finished! 81 links found. 76 excluded. 2 broken.
