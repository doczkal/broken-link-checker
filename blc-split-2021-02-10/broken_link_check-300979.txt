Getting links from: https://fsfe.org/news/2019/news-20191022-01.fr.html#ngi0
├───OK─── https://fsfe.org/news/2019/news-20190514-01.fr.html
├───OK─── https://fsfe.org/about/people/interviews/grun.fr.html#interview
├─BROKEN─ https://fsfe.org/news/2019/about/codeofconduct (HTTP_404)
├───OK─── https://fsfe.org/order/index.fr.html
├─BROKEN─ https://fsfe.org/news/2019/contribute/web/web.fr.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/news-20191002.fr.html (HTTP_404)
├───OK─── https://fsfe.org/news/2019/news-20190701-01.fr.html
├───OK─── https://fsfe.org/news/2019/news-20191014-01.fr.html
├───OK─── https://fsfe.org/news/2019/news-20190927-01.fr.html
Finished! 222 links found. 213 excluded. 3 broken.
