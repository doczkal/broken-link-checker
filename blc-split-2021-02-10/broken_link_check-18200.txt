Getting links from: https://fsfe.org/contact/index.et.html
├───OK─── https://fsfe.org/about/index.et.html
├─BROKEN─ https://fsfe.org/about/codeofconduct/index.et.html (HTTP_404)
├───OK─── https://fsfe.org/news/news.et.rss
├───OK─── https://fsfe.org/news/newsletter.et.html
Finished! 86 links found. 82 excluded. 1 broken.
