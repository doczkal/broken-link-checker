Getting links from: https://fsfe.org/freesoftware/education/edu-related-content.ca.html
├───OK─── https://fsfe.org/about/people/avatars/arnold.jpg
├───OK─── https://fsfe.org/activities/mankind/lsm2002/index.ca.html
├─BROKEN─ https://fsfe.org/associates/ofset/associate (HTTP_404)
├───OK─── https://fsfe.org/activities/mankind/lsm2002/press-release.ca.html
├───OK─── https://fsfe.org/news/2011/news-20111128-02.ca.html#education
├───OK─── https://fsfe.org/news/2001/article2001-12-17-01.ca.html
├───OK─── https://fsfe.org/freesoftware/transcripts/rms-fs-2006-03-09.ca.html
├───OK─── https://fsfe.org/events/2004/FISL/fisl.ca.html
├─BROKEN─ https://fsfe.org/freesoftware/education/news/2012/news-20121217-01 (HTTP_404)
├───OK─── https://fsfe.org/freesoftware/standards/guardian-open-letter.ca.html
├───OK─── https://fsfe.org/news/nl/nl-201101.ca.html
├───OK─── https://fsfe.org/news/2003/lettera_MIUR-2003-07-16.ca.html
├───OK─── https://fsfe.org/activities/tgs/tgs.ca.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool1.ca.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool2.ca.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool3.ca.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool4.ca.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool5.ca.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool6.ca.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool7.ca.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool8.ca.html
Finished! 111 links found. 90 excluded. 2 broken.
