Getting links from: https://fsfe.org/news/2009/news-20090620-01.pl.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.pl.html (HTTP_404)
├─BROKEN─ https://fsfe.org/about/holz/holz.pl.html (HTTP_404)
├───OK─── https://fsfe.org/news/2009/news-20090227-01.pl.html
├─BROKEN─ https://fsfe.org/activities/self/SELF-LegalPolicy-0.9_1.pdf (HTTP_404)
├───OK─── https://fsfe.org/activities/stacs/london.pl.html
├───OK─── https://fsfe.org/activities/stacs/belgrade.pl.html
Finished! 127 links found. 121 excluded. 3 broken.
