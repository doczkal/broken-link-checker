Getting links from: https://fsfe.org/news/2009/news-20090620-01.el.html
├───OK─── https://fsfe.org/news/2009/news-20090620-01.fr.html
├───OK─── https://fsfe.org/news/news.el.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.el.html (HTTP_404)
├─BROKEN─ https://fsfe.org/about/holz/holz.el.html (HTTP_404)
├───OK─── https://fsfe.org/activities/ms-vs-eu/ms-vs-eu.el.html
├─BROKEN─ https://fsfe.org/activities/self/SELF-LegalPolicy-0.9_1.pdf (HTTP_404)
Finished! 125 links found. 119 excluded. 3 broken.
