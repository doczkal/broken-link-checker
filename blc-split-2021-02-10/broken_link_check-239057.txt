Getting links from: https://fsfe.org/news/2008/index.ar.html
├───OK─── https://fsfe.org/news/2008/news-20081215-01.ar.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.ar.html (HTTP_404)
├───OK─── https://fsfe.org/news/2008/news-20081208-01.ar.html
├───OK─── https://fsfe.org/news/2008/gnu-25-years.ar.html
├───OK─── https://fsfe.org/news/2008/news-20080305-01.ar.html
├───OK─── https://fsfe.org/news/2008/news-20080301-01.ar.html
├───OK─── https://fsfe.org/news/2008/news-20080222-01.ar.html
├───OK─── https://fsfe.org/news/2008/news-20080214-01.ar.html
├───OK─── https://fsfe.org/news/2008/news-20080118-01.ar.html
Finished! 139 links found. 130 excluded. 1 broken.
