Getting links from: https://fsfe.org/about/people/mehl/index.es.html
├───OK─── https://fsfe.org/activities/radiodirective/index.es.html
├─BROKEN─ https://fsfe.org/de (HTTP_404)
├───OK─── https://fsfe.org/activities/ilovefs/index.es.html
├───OK─── https://fsfe.org/activities/android/index.es.html
├───OK─── https://fsfe.org/activities/drm/index.es.html
├───OK─── https://fsfe.org/contribute/web/index.es.html
├─BROKEN─ https://fsfe.org/de (HTTP_404)
├───OK─── https://fsfe.org/contribute/internship.es.html
├─BROKEN─ https://fsfe.org/de (HTTP_404)
Finished! 82 links found. 73 excluded. 3 broken.
