Getting links from: https://fsfe.org/news/2012/report-2012.nb.html#
├─BROKEN─ https://fsfe.org/news/2012/blogs.fsfe.org/samtuke/%3Fp%3D255 (HTTP_404)
├───OK─── https://fsfe.org/news/2012/news-20120627-01.nb.html
├───OK─── https://fsfe.org/news/2012/news-20121208-01.nb.html
├───OK─── https://fsfe.org/news/2012/news-20120425-02.nb.html
├───OK─── https://fsfe.org/news/2012/news-20120619-01.nb.html
├───OK─── https://fsfe.org/news/2012/news-20121112-01.nb.html
Finished! 93 links found. 87 excluded. 1 broken.
