Getting links from: https://fsfe.org/news/nl/nl-201408.sv.html
├───OK─── https://fsfe.org/news/nl/nl-201408.de.html
├───OK─── https://fsfe.org/news/nl/nl-201408.it.html
├───OK─── https://fsfe.org/news/nl/nl-201408.nl.html
├───OK─── https://fsfe.org/news/nl/nl-201408.sq.html
├─BROKEN─ https://fsfe.org/about/ojasild/ojasild.sv.html (HTTP_404)
Finished! 102 links found. 97 excluded. 1 broken.
