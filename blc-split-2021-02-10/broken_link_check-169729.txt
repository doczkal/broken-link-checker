Getting links from: https://fsfe.org/news/2008/news-20081208-01.zh.html
├───OK─── https://fsfe.org/news/2008/news-20081208-01.de.html
├───OK─── https://fsfe.org/news/2008/news-20081208-01.el.html
├───OK─── https://fsfe.org/news/2008/news-20081208-01.en.html
├───OK─── https://fsfe.org/news/2008/news-20081208-01.nl.html
├─BROKEN─ https://fsfe.org/ftf/index.zh.html (HTTP_404)
├─BROKEN─ https://fsfe.org/ftf (HTTP_404)
Finished! 64 links found. 58 excluded. 2 broken.
