Getting links from: https://fsfe.org/activities/android/artwork.el.html
├───OK─── https://fsfe.org/activities/android/artwork/leaflets/en/leaflet-fya-front.en.png
├─BROKEN─ https://fsfe.org/activities/android/artwork/leaflets/en/leaflet-fya-front.en.svg (HTTP_404)
├───OK─── https://fsfe.org/activities/android/artwork/leaflets/en/leaflet-fya-inside.en.png
├─BROKEN─ https://fsfe.org/activities/android/artwork/leaflets/en/leaflet-fya-front.en.svg (HTTP_404)
├───OK─── https://fsfe.org/activities/android/artwork/leaflets/fi/leaflet-fya-front.fi.png
├─BROKEN─ https://fsfe.org/activities/android/artwork/leaflets/fi/leaflet-fya-front.fi.svg (HTTP_404)
├───OK─── https://fsfe.org/activities/android/artwork/leaflets/fi/leaflet-fya-inside.fi.png
├─BROKEN─ https://fsfe.org/activities/android/artwork/leaflets/fi/leaflet-fya-front.fi.svg (HTTP_404)
├───OK─── https://fsfe.org/activities/android/artwork/leaflets/leaflet-free-your-android-fdroid-text.el.html
├───OK─── https://fsfe.org/activities/android/artwork/DIN_A1_de.png
├───OK─── https://fsfe.org/activities/android/artwork/DIN_A1_el.png
├───OK─── https://fsfe.org/activities/android/artwork/DIN_A1_fr.png
├───OK─── https://fsfe.org/activities/android/artwork/DIN_A1_es.png
Finished! 106 links found. 93 excluded. 4 broken.
