Getting links from: https://fsfe.org/news/nl/nl-202005.el.html
├───OK─── https://fsfe.org/news/2020/news-20200427-01.el.html
├─BROKEN─ https://fsfe.org/https:/librezoom.net/lz19-die-nexte-bitte/index.el.html (HTTP_404)
Finished! 98 links found. 96 excluded. 1 broken.
