Getting links from: https://fsfe.org/about/people/testimonials.tr.html#
├───OK─── https://fsfe.org/about/people/interviews/cryptie.tr.html#video-30s-cryptie
├───OK─── https://fsfe.org/about/people/interviews/lequertier.tr.html#video-30s-vincent
├───OK─── https://fsfe.org/about/people/interviews/zerolo.tr.html#video-30s-tomas
├─BROKEN─ https://fsfe.org/about/people/interviews/bernhard.tr.html#video-30s-bernhard (HTTP_404)
├───OK─── https://fsfe.org/about/people/interviews/weitzhofer.tr.html#interview
├───OK─── https://fsfe.org/about/people/interviews/grun.tr.html#interview
├───OK─── https://fsfe.org/about/people/interviews/ockers.tr.html#video-30s-andre
├───OK─── https://fsfe.org/about/people/interviews/mueller.tr.html#interview
├───OK─── https://fsfe.org/about/people/interviews/snow.tr.html#interview
├───OK─── https://fsfe.org/about/people/interviews/gkotsopoulou.tr.html#interview
Finished! 122 links found. 112 excluded. 1 broken.
