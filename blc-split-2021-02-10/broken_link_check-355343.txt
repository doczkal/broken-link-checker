Getting links from: https://fsfe.org/news/nl/nl-201309.cs.html
├───OK─── https://fsfe.org/news/2013/news-20130612-01.cs.html
├───OK─── https://fsfe.org/activities/msooxml/msooxml-interoperability.cs.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.cs.html (HTTP_404)
Finished! 117 links found. 114 excluded. 1 broken.
