Getting links from: https://fsfe.org/news/nl/nl-201408.ca.html
├───OK─── https://fsfe.org/news/nl/nl-201408.en.html
├───OK─── https://fsfe.org/about/people/roy/roy.ca.html
├─BROKEN─ https://fsfe.org/about/ojasild/ojasild.ca.html (HTTP_404)
Finished! 102 links found. 99 excluded. 1 broken.
