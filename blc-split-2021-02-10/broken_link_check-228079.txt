Getting links from: https://fsfe.org/news/nl/nl-201201.ca.html
├─BROKEN─ https://fsfe.org/activities/elections/askyourcandidates/example-questions.xhtml (HTTP_404)
├───OK─── https://fsfe.org/news/2011/news-20111201-02.ca.html
├───OK─── https://fsfe.org/news/2011/news-20111213-01.ca.html
├───OK─── https://fsfe.org/news/2011/news-20111220-01.ca.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.ca.html (HTTP_404)
Finished! 87 links found. 82 excluded. 2 broken.
