Getting links from: https://fsfe.org/news/nl/nl-201308.ca.html#
├───OK─── https://fsfe.org/news/2013/news-20130729-01.ca.html
├───OK─── https://fsfe.org/news/2013/news-20130730-01.ca.html
├─BROKEN─ https://fsfe.org/news/2013/2013-07-26_Open_Letter_to_NEC.ca.html (HTTP_404)
├───OK─── https://fsfe.org/freesoftware/standards/transparency-letter.ca.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.ca.html (HTTP_404)
Finished! 112 links found. 107 excluded. 2 broken.
