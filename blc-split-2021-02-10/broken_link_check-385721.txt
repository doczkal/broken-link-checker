Getting links from: https://fsfe.org/news/nl/nl-201110.ca.html#
├───OK─── https://fsfe.org/news/nl/nl-201110.el.html
├───OK─── https://fsfe.org/news/nl/nl-201110.en.html
├───OK─── https://fsfe.org/news/nl/nl-201110.fr.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.ca.html (HTTP_404)
Finished! 101 links found. 97 excluded. 1 broken.
