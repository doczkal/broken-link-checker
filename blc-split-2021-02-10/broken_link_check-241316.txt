Getting links from: https://fsfe.org/about/people/roy/roy.fr.html
├───OK─── https://fsfe.org/about/team.fr.html
├─BROKEN─ https://fsfe.org/fr/index.fr.html (HTTP_404)
├─BROKEN─ https://fsfe.org/legal/index.fr.html (HTTP_404)
Finished! 79 links found. 76 excluded. 2 broken.
