Getting links from: https://fsfe.org/news/2007/news-20070630-01.ru.html
├───OK─── https://fsfe.org/activities/ms-vs-eu/index.ru.html
├───OK─── https://fsfe.org/activities/ipred2/index.ru.html
├───OK─── https://fsfe.org/activities/wsis/index.ru.html
├───OK─── https://fsfe.org/activities/igf/index.ru.html
├───OK─── https://fsfe.org/activities/igf/a2k.ru.html
├───OK─── https://fsfe.org/activities/igf/dcos.ru.html
├───OK─── https://fsfe.org/activities/wipo/statement-20050721.ru.html
├─BROKEN─ https://fsfe.org/ftf (HTTP_404)
├───OK─── https://fsfe.org/activities/self/index.ru.html
├─BROKEN─ https://fsfe.org/fellows/greve/img/zrh_hoist (HTTP_404)
├─BROKEN─ https://fsfe.org/fellows/meetings/2006_bolzano (HTTP_404)
Finished! 105 links found. 94 excluded. 3 broken.
