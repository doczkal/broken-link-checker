Getting links from: https://fsfe.org/about/people/testimonials.de.html
├───OK─── https://fsfe.org/about/people/interviews/cryptie.de.html#video-30s-cryptie
├───OK─── https://fsfe.org/about/people/interviews/lequertier.de.html#video-30s-vincent
├───OK─── https://fsfe.org/about/people/interviews/zerolo.de.html#video-30s-tomas
├─BROKEN─ https://fsfe.org/about/people/interviews/bernhard.de.html#video-30s-bernhard (HTTP_404)
├───OK─── https://fsfe.org/about/people/interviews/weitzhofer.de.html#interview
├───OK─── https://fsfe.org/about/people/interviews/grun.de.html#interview
├───OK─── https://fsfe.org/about/people/interviews/ockers.de.html#video-30s-andre
├───OK─── https://fsfe.org/about/people/interviews/mueller.de.html#interview
├───OK─── https://fsfe.org/about/people/interviews/snow.de.html#interview
├───OK─── https://fsfe.org/about/people/interviews/gkotsopoulou.de.html#interview
Finished! 120 links found. 110 excluded. 1 broken.
