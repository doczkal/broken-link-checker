Getting links from: https://fsfe.org/news/nl/nl-201205.ca.html
├─BROKEN─ https://fsfe.org/activities/swpat/swapt.ca.html (HTTP_404)
├───OK─── https://fsfe.org/news/2012/news-20120402-01.ca.html
├─BROKEN─ https://fsfe.org/project/os/def.ca.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.ca.html (HTTP_404)
Finished! 114 links found. 110 excluded. 3 broken.
