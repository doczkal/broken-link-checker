Getting links from: https://fsfe.org/news/2019/news-20191022-01.da.html#ngi0
├───OK─── https://fsfe.org/activities/radiodirective/radiodirective.da.html
├───OK─── https://fsfe.org/news/2019/news-20190827-01.da.html
├───OK─── https://fsfe.org/activities/radiodirective/statement.da.html
├───OK─── https://fsfe.org/news/2019/news-20191007-01.da.html
├───OK─── https://fsfe.org/news/2019/news-20190329-01.da.html
├───OK─── https://fsfe.org/about/people/interviews/grun.da.html#interview
├─BROKEN─ https://fsfe.org/news/2019/about/codeofconduct (HTTP_404)
├───OK─── https://fsfe.org/contribute/index.da.html
├───OK─── https://fsfe.org/events/index.da.html
├─BROKEN─ https://fsfe.org/news/2019/contribute/web/web.da.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/news-20191002.da.html (HTTP_404)
├───OK─── https://fsfe.org/news/2019/news-20190701-01.da.html
├───OK─── https://fsfe.org/news/2019/news-20190927-01.da.html
├───OK─── https://fsfe.org/tags/tagged-radiodirective.da.html
├───OK─── https://fsfe.org/tags/tagged-savecodeshare.da.html
Finished! 222 links found. 207 excluded. 3 broken.
