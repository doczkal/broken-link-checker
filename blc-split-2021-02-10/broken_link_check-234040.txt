Getting links from: https://fsfe.org/news/2007/news-20070630-01.nl.html
├───OK─── https://fsfe.org/activities/ipred2/index.nl.html
├───OK─── https://fsfe.org/activities/igf/index.nl.html
├───OK─── https://fsfe.org/activities/wipo/statement-20050721.nl.html
├─BROKEN─ https://fsfe.org/ftf (HTTP_404)
├───OK─── https://fsfe.org/activities/self/index.nl.html
├─BROKEN─ https://fsfe.org/fellows/greve/img/zrh_hoist (HTTP_404)
├─BROKEN─ https://fsfe.org/fellows/meetings/2006_bolzano (HTTP_404)
Finished! 100 links found. 93 excluded. 3 broken.
