Getting links from: https://fsfe.org/events/2009/index.fi.html
├───OK─── https://fsfe.org/about/people/kirschner/kirschner.fi.html
├───OK─── https://fsfe.org/about/people/kirschner/index.fi.html
├─BROKEN─ https://fsfe.org/about/holz (HTTP_404)
├───OK─── https://fsfe.org/news/2009/ga2009.fi.html
├─BROKEN─ https://fsfe.org/ftf (HTTP_404)
├───OK─── https://fsfe.org/freesoftware/standards/standards.fi.html
Finished! 202 links found. 196 excluded. 2 broken.
