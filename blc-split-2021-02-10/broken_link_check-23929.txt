Getting links from: https://fsfe.org/news/nl/nl-201201.en.html
├─BROKEN─ https://fsfe.org/activities/elections/askyourcandidates/example-questions.xhtml (HTTP_404)
├───OK─── https://fsfe.org/news/2011/news-20111220-01.en.html
├───OK─── https://fsfe.org/donate/letter-2011.en.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.en.html (HTTP_404)
Finished! 84 links found. 80 excluded. 2 broken.
