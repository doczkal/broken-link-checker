Getting links from: https://fsfe.org/news/nl/nl-201310.el.html
├───OK─── https://fsfe.org/news/nl/nl-201310.es.html
├───OK─── https://fsfe.org/freesoftware/basics/gnuproject.el.html
├───OK─── https://fsfe.org/news/2013/news-20130926-01.el.html
├───OK─── https://fsfe.org/news/2013/news-20130918-01.el.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.el.html (HTTP_404)
Finished! 118 links found. 113 excluded. 1 broken.
