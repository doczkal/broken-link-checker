Getting links from: https://fsfe.org/news/2008/index.sk.html
├───OK─── https://fsfe.org/news/2008/news-20081215-01.sk.html
├───OK─── https://fsfe.org/news/2008/news-20081210-01.sk.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.sk.html (HTTP_404)
├───OK─── https://fsfe.org/news/2008/news-20081208-01.sk.html
├───OK─── https://fsfe.org/freesoftware/standards/ps.sk.html
├───OK─── https://fsfe.org/news/2008/gnu-25-years.sk.html
├───OK─── https://fsfe.org/news/2008/news-20080305-01.sk.html
├───OK─── https://fsfe.org/news/2008/news-20080301-01.sk.html
├───OK─── https://fsfe.org/news/2008/news-20080228-01.sk.html
├───OK─── https://fsfe.org/news/2008/news-20080222-01.sk.html
├───OK─── https://fsfe.org/news/2008/news-20080214-01.sk.html
├───OK─── https://fsfe.org/news/2008/news-20080118-01.sk.html
Finished! 138 links found. 126 excluded. 1 broken.
