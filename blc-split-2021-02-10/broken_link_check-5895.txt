Getting links from: https://fsfe.org/news/nl/nl-201302.nb.html
├───OK─── https://fsfe.org/activities/ilovefs/2013/index.nb.html
├───OK─── https://fsfe.org/activities/ilovefs/2013/whylovefs.nb.html
├─BROKEN─ https://fsfe.org/activities/ilovefs/2013/banners.nb.html (HTTP_404)
├─BROKEN─ https://fsfe.org/order/promoorder.nb.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.nb.html (HTTP_404)
Finished! 119 links found. 114 excluded. 3 broken.
