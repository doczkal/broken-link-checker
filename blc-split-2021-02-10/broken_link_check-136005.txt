Getting links from: https://fsfe.org/news/nl/nl-201207.et.html
├───OK─── https://fsfe.org/activities/pdfreaders/pdfsprint.et.html
├───OK─── https://fsfe.org/news/2012/news-20120607-01.et.html
├───OK─── https://fsfe.org/contribute/editors/editors.et.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.et.html (HTTP_404)
Finished! 102 links found. 98 excluded. 1 broken.
