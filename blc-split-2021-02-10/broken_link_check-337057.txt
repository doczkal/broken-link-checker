Getting links from: https://fsfe.org/news/nl/nl-201105.nl.html
├───OK─── https://fsfe.org/activities/swpat/letter-20101222.nl.html
├───OK─── https://fsfe.org/activities/swpat/letter-20110406.nl.html
├───OK─── https://fsfe.org/activities/swpat/novell-cptn.nl.html
├─BROKEN─ https://fsfe.org/join.nl.html (HTTP_404)
├───OK─── https://fsfe.org/activities/pdfreaders/follow-up.nl.html
Finished! 103 links found. 98 excluded. 1 broken.
