Getting links from: https://fsfe.org/news/nl/nl-201106.fi.html#
├───OK─── https://fsfe.org/news/nl/nl-201106.cs.html
├───OK─── https://fsfe.org/news/nl/nl-201106.it.html
├───OK─── https://fsfe.org/news/2011/news-20110520-01.fi.html
├─BROKEN─ https://fsfe.org/source/activities/elections/askyourcandidates/askyourcandidates.xhtml (HTTP_404)
Finished! 93 links found. 89 excluded. 1 broken.
