Getting links from: https://fsfe.org/about/people/testimonials.bs.html#kroah-hartman
├───OK─── https://fsfe.org/about/people/interviews/cryptie.bs.html#video-30s-cryptie
├───OK─── https://fsfe.org/about/people/interviews/lequertier.bs.html#video-30s-vincent
├───OK─── https://fsfe.org/about/people/interviews/zerolo.bs.html#video-30s-tomas
├─BROKEN─ https://fsfe.org/about/people/interviews/bernhard.bs.html#video-30s-bernhard (HTTP_404)
├───OK─── https://fsfe.org/about/people/interviews/weitzhofer.bs.html#interview
├───OK─── https://fsfe.org/about/people/interviews/grun.bs.html#interview
├───OK─── https://fsfe.org/about/people/interviews/ockers.bs.html#video-30s-andre
├───OK─── https://fsfe.org/about/people/interviews/mueller.bs.html#interview
├───OK─── https://fsfe.org/about/people/interviews/snow.bs.html#interview
├───OK─── https://fsfe.org/about/people/interviews/gkotsopoulou.bs.html#interview
Finished! 122 links found. 112 excluded. 1 broken.
