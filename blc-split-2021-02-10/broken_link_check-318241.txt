Getting links from: https://fsfe.org/news/nl/nl-201309.nb.html
├───OK─── https://fsfe.org/activities/swpat/swpat.nb.html
├───OK─── https://fsfe.org/news/2013/news-20130612-01.nb.html
├───OK─── https://fsfe.org/activities/msooxml/msooxml-interoperability.nb.html
├───OK─── https://fsfe.org/activities/ms-vs-eu/ms-vs-eu.nb.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.nb.html (HTTP_404)
Finished! 117 links found. 112 excluded. 1 broken.
