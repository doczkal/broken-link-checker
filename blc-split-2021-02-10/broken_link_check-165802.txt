Getting links from: https://fsfe.org/news/2016/news-20160224-01.uk.html
├───OK─── https://fsfe.org/news/2016/news-20160224-01.nl.html
├───OK─── https://fsfe.org/news/2016/graphics/v2015martin.jpg
├───OK─── https://fsfe.org/contribute/promopics/fdroid-flyer_thumb.png
├─BROKEN─ https://fsfe.org/activities/ilovefs/whylovefs/photos/gallery/ilovefs-gallery-thumb-25.jpg (HTTP_404)
├───OK─── https://fsfe.org/news/2016/graphics/dfd-venezuela-merida-05.jpg
├───OK─── https://fsfe.org/news/2016/graphics/router.jpg
Finished! 89 links found. 83 excluded. 1 broken.
