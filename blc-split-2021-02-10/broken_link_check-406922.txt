Getting links from: https://fsfe.org/activities/swpat/swpat.de.html
├───OK─── https://fsfe.org/activities/swpat/letter-20101222.de.html
├─BROKEN─ https://fsfe.org/activities/swpat/documents/iprip.de.html (HTTP_404)
├───OK─── https://fsfe.org/activities/swpat/fsfe-patstrat-response.de.html
├───OK─── https://fsfe.org/activities/swpat/memorandum.de.html
Finished! 86 links found. 82 excluded. 1 broken.
