Getting links from: https://fsfe.org/news/2012/report-2012.sk.html
├───OK─── https://fsfe.org/news/2012/report-1212/karsten.jpg
├───OK─── https://fsfe.org/news/2012/report-1212/chorlton.jpg
├───OK─── https://fsfe.org/news/2012/report-1212/Torsten.jpg
├─BROKEN─ https://fsfe.org/news/2012/blogs.fsfe.org/samtuke/%3Fp%3D255 (HTTP_404)
├───OK─── https://fsfe.org/news/2012/report-1212/FYA.jpg
├───OK─── https://fsfe.org/news/2012/report-1212/viennabooth1.jpg
├───OK─── https://fsfe.org/news/2012/report-1212/fsfe-ilovefs.png
Finished! 93 links found. 86 excluded. 1 broken.
