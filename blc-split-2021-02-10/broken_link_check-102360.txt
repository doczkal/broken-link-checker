Getting links from: https://fsfe.org/news/nl/nl-201207.el.html
├───OK─── https://fsfe.org/activities/pdfreaders/pdfsprint.el.html
├───OK─── https://fsfe.org/news/2012/news-20120616-01.el.html
├───OK─── https://fsfe.org/news/2012/news-20120607-01.el.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.el.html (HTTP_404)
Finished! 100 links found. 96 excluded. 1 broken.
