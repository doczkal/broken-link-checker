Getting links from: https://fsfe.org/news/2013/news-20130730-01.cs.html
├───OK─── https://fsfe.org/news/2013/news-20130730-01.de.html
├───OK─── https://fsfe.org/news/2013/news-20130730-01.pt.html
├─BROKEN─ https://fsfe.org/news/2013/2013-07-26_Open_Letter_to_NEC.cs.html (HTTP_404)
Finished! 67 links found. 64 excluded. 1 broken.
