Getting links from: https://fsfe.org/about/funds/2001.ar.html
├───OK─── https://fsfe.org/about/funds/2001.en.html
├───OK─── https://fsfe.org/about/funds/2001.ru.html
├───OK─── https://fsfe.org/help/thankgnus-2001.ar.html
├─BROKEN─ https://fsfe.org/about/legal/de/de.ar.html (HTTP_404)
Finished! 84 links found. 80 excluded. 1 broken.
