Getting links from: https://fsfe.org/news/2008/news-20080118-01.it.html
├───OK─── https://fsfe.org/news/2008/news-20080118-01.el.html
├───OK─── https://fsfe.org/news/2008/news-20080118-01.en.html
├───OK─── https://fsfe.org/news/2008/news-20080118-01.es.html
├───OK─── https://fsfe.org/news/2008/news-20080118-01.nl.html
├─BROKEN─ https://fsfe.org/ftf/index.it.html (HTTP_404)
Finished! 57 links found. 52 excluded. 1 broken.
