Getting links from: https://fsfe.org/news/2019/news-20191022-01.el.html#ngi0
├───OK─── https://fsfe.org/news/2019/news-20190515-01.el.html
├───OK─── https://fsfe.org/news/2019/news-20190515-02.el.html
├───OK─── https://fsfe.org/about/people/interviews/grun.el.html#interview
├─BROKEN─ https://fsfe.org/news/2019/about/codeofconduct (HTTP_404)
├───OK─── https://fsfe.org/contribute/index.el.html
├─BROKEN─ https://fsfe.org/news/2019/contribute/web/web.el.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/news-20191002.el.html (HTTP_404)
├───OK─── https://fsfe.org/news/2019/news-20190701-01.el.html
├───OK─── https://fsfe.org/news/2019/news-20190927-01.el.html
├───OK─── https://fsfe.org/tags/tagged-radiodirective.el.html
Finished! 222 links found. 212 excluded. 3 broken.
