Getting links from: https://fsfe.org/news/2009/news-20090620-01.sv.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.sv.html (HTTP_404)
├─BROKEN─ https://fsfe.org/about/holz/holz.sv.html (HTTP_404)
├───OK─── https://fsfe.org/activities/ms-vs-eu/application-1.0.pdf
├───OK─── https://fsfe.org/activities/ms-vs-eu/monti-samba-submission.pdf
├─BROKEN─ https://fsfe.org/activities/self/SELF-LegalPolicy-0.9_1.pdf (HTTP_404)
├───OK─── https://fsfe.org/activities/stacs/london.sv.html
├───OK─── https://fsfe.org/activities/stacs/belgrade.sv.html
Finished! 127 links found. 120 excluded. 3 broken.
