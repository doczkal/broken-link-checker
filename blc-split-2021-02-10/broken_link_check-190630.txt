Getting links from: https://fsfe.org/news/nl/nl-201810.tr.html
├───OK─── https://fsfe.org/news/2018/news-20181023-02.tr.html
├─BROKEN─ https://fsfe.org/about/mancheva/mancheva (HTTP_404)
├───OK─── https://fsfe.org/news/2018/news-20181010-01.tr.html
├───OK─── https://fsfe.org/tags/tagged-digital-o-mat.tr.html
Finished! 93 links found. 89 excluded. 1 broken.
