Getting links from: https://fsfe.org/news/nl/nl-201206.et.html
├───OK─── https://fsfe.org/news/2012/news-20120509-02.et.html
├───OK─── https://fsfe.org/news/2012/news-20120528-01.et.html
├───OK─── https://fsfe.org/news/2012/news-20120525-01.et.html
├───OK─── https://fsfe.org/activities/pdfreaders/parliamentary-questions-eu.et.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.et.html (HTTP_404)
Finished! 103 links found. 98 excluded. 1 broken.
