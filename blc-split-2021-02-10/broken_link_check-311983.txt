Getting links from: https://fsfe.org/news/nl/nl-201205.et.html
├─BROKEN─ https://fsfe.org/activities/swpat/swapt.et.html (HTTP_404)
├─BROKEN─ https://fsfe.org/project/os/def.et.html (HTTP_404)
├───OK─── https://fsfe.org/news/2012/news-20120426-01.et.html
├───OK─── https://fsfe.org/freesoftware/standards/uk-standards-consultation.et.html
├───OK─── https://fsfe.org/news/2012/news-20120425-02.et.html
├───OK─── https://fsfe.org/news/2012/news-20120425-01.et.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.et.html (HTTP_404)
Finished! 114 links found. 107 excluded. 3 broken.
