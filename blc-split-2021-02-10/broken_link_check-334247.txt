Getting links from: https://fsfe.org/news/2019/news-20191022-01.tr.html
├───OK─── https://fsfe.org/activities/routers/timeline.tr.html
├───OK─── https://fsfe.org/news/2016/news-20160725-01.tr.html
├───OK─── https://fsfe.org/news/2019/news-20190827-01.tr.html
├───OK─── https://fsfe.org/activities/radiodirective/statement.tr.html
├───OK─── https://fsfe.org/news/2019/news-20191007-01.tr.html
├───OK─── https://fsfe.org/news/2019/news-20190329-01.tr.html
├───OK─── https://fsfe.org/about/people/interviews/grun.tr.html#interview
├─BROKEN─ https://fsfe.org/news/2019/about/codeofconduct (HTTP_404)
├───OK─── https://fsfe.org/events/index.tr.html
├─BROKEN─ https://fsfe.org/news/2019/contribute/web/web.tr.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/news-20191002.tr.html (HTTP_404)
├───OK─── https://fsfe.org/news/2019/news-20190701-01.tr.html
├───OK─── https://fsfe.org/graphics/infomaterial-450px.png
├───OK─── https://fsfe.org/news/2019/news-20190927-01.tr.html
├───OK─── https://fsfe.org/tags/tagged-radiodirective.tr.html
Finished! 222 links found. 207 excluded. 3 broken.
