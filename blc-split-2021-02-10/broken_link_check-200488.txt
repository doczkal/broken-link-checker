Getting links from: https://fsfe.org/freesoftware/education/edu-related-content.bg.html
├───OK─── https://fsfe.org/activities/mankind/lsm2002/index.bg.html
├─BROKEN─ https://fsfe.org/associates/ofset/associate (HTTP_404)
├───OK─── https://fsfe.org/activities/mankind/lsm2002/press-release.bg.html
├───OK─── https://fsfe.org/news/2001/article2001-12-17-01.bg.html
├───OK─── https://fsfe.org/freesoftware/transcripts/rms-fs-2006-03-09.bg.html
├───OK─── https://fsfe.org/events/2004/FISL/fisl.bg.html
├─BROKEN─ https://fsfe.org/freesoftware/education/news/2012/news-20121217-01 (HTTP_404)
├───OK─── https://fsfe.org/news/2003/lettera_MIUR-2003-07-16.bg.html
├───OK─── https://fsfe.org/activities/tgs/tgs.bg.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool1.bg.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool2.bg.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool3.bg.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool4.bg.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool5.bg.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool6.bg.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool7.bg.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool8.bg.html
├───OK─── https://fsfe.org/contribute/editors/editors.bg.html
Finished! 111 links found. 93 excluded. 2 broken.
