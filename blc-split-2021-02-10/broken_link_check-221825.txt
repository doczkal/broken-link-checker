Getting links from: https://fsfe.org/news/2019/news-20191022-01.sv.html
├───OK─── https://fsfe.org/about/people/interviews/grun.sv.html#interview
├─BROKEN─ https://fsfe.org/news/2019/about/codeofconduct (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/contribute/web/web.sv.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/news-20191002.sv.html (HTTP_404)
├───OK─── https://fsfe.org/news/2019/news-20190701-01.sv.html
Finished! 222 links found. 217 excluded. 3 broken.
