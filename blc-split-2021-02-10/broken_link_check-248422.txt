Getting links from: https://fsfe.org/about/people/roy/roy.pt.html
├───OK─── https://fsfe.org/activities/ftf/ftf.pt.html
├─BROKEN─ https://fsfe.org/fr/index.pt.html (HTTP_404)
├─BROKEN─ https://fsfe.org/legal (HTTP_404)
Finished! 81 links found. 78 excluded. 2 broken.
