Getting links from: https://fsfe.org/news/nl/nl-201205.uk.html
├─BROKEN─ https://fsfe.org/activities/swpat/swapt.uk.html (HTTP_404)
├─BROKEN─ https://fsfe.org/project/os/def.uk.html (HTTP_404)
├───OK─── https://fsfe.org/freesoftware/standards/uk-standards-consultation.uk.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.uk.html (HTTP_404)
Finished! 114 links found. 110 excluded. 3 broken.
