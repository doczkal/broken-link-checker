Getting links from: https://fsfe.org/news/2013/index.sk.html
├───OK─── https://fsfe.org/news/2013/index.en.html
├───OK─── https://fsfe.org/news/2013/news-20131213-01.sk.html
├─BROKEN─ https://fsfe.org/news/2013/2013-07-26_Open_Letter_to_NEC.sk.html (HTTP_404)
├───OK─── https://fsfe.org/news/2013/news-20130226-01.sk.html
Finished! 289 links found. 285 excluded. 1 broken.
