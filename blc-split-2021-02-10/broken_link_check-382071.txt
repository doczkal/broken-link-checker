Getting links from: https://fsfe.org/news/nl/nl-201310.en.html
├───OK─── https://fsfe.org/freesoftware/basics/gnuproject.en.html
├───OK─── https://fsfe.org/news/2013/news-20130926-01.en.html
├───OK─── https://fsfe.org/news/2013/news-20130918-01.en.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.en.html (HTTP_404)
├───OK─── https://fsfe.org/events/events.en.rss
Finished! 117 links found. 112 excluded. 1 broken.
