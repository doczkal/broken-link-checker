Getting links from: https://fsfe.org/news/2008/index.it.html
├───OK─── https://fsfe.org/news/2008/news-20081215-01.it.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.it.html (HTTP_404)
├───OK─── https://fsfe.org/news/2008/news-20081208-01.it.html
├───OK─── https://fsfe.org/tags/tagged-enterprise.it.html
├───OK─── https://fsfe.org/news/2008/gnu-25-years.it.html
├───OK─── https://fsfe.org/news/2008/news-20080305-01.it.html
├───OK─── https://fsfe.org/news/2008/news-20080301-01.it.html
├───OK─── https://fsfe.org/news/2008/news-20080222-01.it.html
├───OK─── https://fsfe.org/news/2008/news-20080220-01.it.html
├───OK─── https://fsfe.org/news/2008/news-20080214-01.it.html
├───OK─── https://fsfe.org/news/2008/news-20080118-01.it.html
Finished! 138 links found. 127 excluded. 1 broken.
