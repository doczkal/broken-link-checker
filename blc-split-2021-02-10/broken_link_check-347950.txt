Getting links from: https://fsfe.org/news/2007/news-20070630-01.sq.html
├───OK─── https://fsfe.org/news/2007/news-20070630-01.en.html
├───OK─── https://fsfe.org/activities/ms-vs-eu/index.sq.html
├───OK─── https://fsfe.org/activities/ipred2/index.sq.html
├───OK─── https://fsfe.org/activities/wsis/index.sq.html
├───OK─── https://fsfe.org/activities/igf/index.sq.html
├───OK─── https://fsfe.org/activities/igf/a2k.sq.html
├───OK─── https://fsfe.org/activities/igf/dcos.sq.html
├───OK─── https://fsfe.org/activities/wipo/index.sq.html
├───OK─── https://fsfe.org/activities/wipo/statement-20050721.sq.html
├─BROKEN─ https://fsfe.org/ftf (HTTP_404)
├───OK─── https://fsfe.org/activities/gplv3/index.sq.html#transcripts
├───OK─── https://fsfe.org/activities/self/index.sq.html
├─BROKEN─ https://fsfe.org/fellows/greve/img/zrh_hoist (HTTP_404)
├─BROKEN─ https://fsfe.org/fellows/meetings/2006_bolzano (HTTP_404)
├───OK─── https://fsfe.org/order/index.sq.html
├───OK─── https://fsfe.org/associates/index.sq.html
Finished! 104 links found. 88 excluded. 3 broken.
