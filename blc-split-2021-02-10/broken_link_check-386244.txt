Getting links from: https://fsfe.org/news/2018/news-20181105-01.cs.html
├───OK─── https://fsfe.org/news/2018/news-20180705-01.cs.html
├───OK─── https://fsfe.org/news/2018/news-20180601-01.cs.html
├───OK─── https://fsfe.org/news/2018/news-20180917-01.cs.html
├───OK─── https://fsfe.org/news/2018/news-20180308-01.cs.html
├───OK─── https://fsfe.org/news/2017/news-20171116-01.cs.html
├─BROKEN─ https://fsfe.org/about/mancheva/mancheva (HTTP_404)
├───OK─── https://fsfe.org/activities/ftf/activities.cs.html
├─BROKEN─ https://fsfe.org/order/order.htmltshirt-100freedoms-black (HTTP_404)
├───OK─── https://fsfe.org/tags/tagged-annual-report.cs.html
Finished! 204 links found. 195 excluded. 2 broken.
