Getting links from: https://fsfe.org/news/nl/nl-201203.sr.html
├───OK─── https://fsfe.org/news/2012/news-20120228-01.sr.html
├───OK─── https://fsfe.org/news/2012/news-20120223-01.sr.html
├───OK─── https://fsfe.org/activities/ilovefs/2012/whylovefs.sr.html
├───OK─── https://fsfe.org/news/2012/news-20120210-01.sr.html
├───OK─── https://fsfe.org/news/2012/news-20120204-01.sr.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.sr.html (HTTP_404)
Finished! 119 links found. 113 excluded. 1 broken.
