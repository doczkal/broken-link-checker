Getting links from: https://fsfe.org/about/people/testimonials.es.html
├───OK─── https://fsfe.org/about/people/interviews/cryptie.es.html#video-30s-cryptie
├───OK─── https://fsfe.org/about/people/interviews/lequertier.es.html#video-30s-vincent
├───OK─── https://fsfe.org/about/people/interviews/zerolo.es.html#video-30s-tomas
├─BROKEN─ https://fsfe.org/about/people/interviews/bernhard.es.html#video-30s-bernhard (HTTP_404)
├───OK─── https://fsfe.org/about/people/interviews/weitzhofer.es.html#interview
├───OK─── https://fsfe.org/about/people/interviews/ockers.es.html#video-30s-andre
├───OK─── https://fsfe.org/about/people/interviews/mueller.es.html#interview
├───OK─── https://fsfe.org/about/people/interviews/snow.es.html#interview
├───OK─── https://fsfe.org/about/people/interviews/gkotsopoulou.es.html#interview
Finished! 122 links found. 113 excluded. 1 broken.
