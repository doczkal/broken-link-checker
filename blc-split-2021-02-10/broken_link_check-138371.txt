Getting links from: https://fsfe.org/news/nl/nl-201310.fi.html
├───OK─── https://fsfe.org/news/nl/nl-201310.de.html
├───OK─── https://fsfe.org/freesoftware/basics/gnuproject.fi.html
├───OK─── https://fsfe.org/news/2013/news-20130926-01.fi.html
├───OK─── https://fsfe.org/news/2013/news-20130918-01.fi.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.fi.html (HTTP_404)
Finished! 121 links found. 116 excluded. 1 broken.
