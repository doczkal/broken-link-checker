Getting links from: https://fsfe.org/news/nl/nl-201205.bs.html
├─BROKEN─ https://fsfe.org/activities/swpat/swapt.bs.html (HTTP_404)
├─BROKEN─ https://fsfe.org/project/os/def.bs.html (HTTP_404)
├───OK─── https://fsfe.org/freesoftware/standards/uk-standards-consultation.bs.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.bs.html (HTTP_404)
Finished! 114 links found. 110 excluded. 3 broken.
