Getting links from: https://fsfe.org/news/nl/nl-201502.sv.html
├───OK─── https://fsfe.org/news/2014/news-20140221-01.sv.html
├─BROKEN─ https://fsfe.org/about/ojasild/ojasild.sv.html (HTTP_404)
├───OK─── https://fsfe.org/about/people/gerloff/gerloff.sv.html
├─BROKEN─ https://fsfe.org/about/ojasild (HTTP_404)
Finished! 95 links found. 91 excluded. 2 broken.
