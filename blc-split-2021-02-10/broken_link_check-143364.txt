Getting links from: https://fsfe.org/news/nl/nl-201205.sr.html
├─BROKEN─ https://fsfe.org/activities/swpat/swapt.sr.html (HTTP_404)
├───OK─── https://fsfe.org/news/2012/news-20120412-02.sr.html
├───OK─── https://fsfe.org/news/2012/news-20120402-01.sr.html
├─BROKEN─ https://fsfe.org/project/os/def.sr.html (HTTP_404)
├───OK─── https://fsfe.org/news/2012/news-20120426-01.sr.html
├───OK─── https://fsfe.org/freesoftware/standards/uk-standards-consultation.sr.html
├───OK─── https://fsfe.org/news/2012/news-20120425-02.sr.html
├───OK─── https://fsfe.org/news/2012/news-20120425-01.sr.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.sr.html (HTTP_404)
Finished! 114 links found. 105 excluded. 3 broken.
