Getting links from: https://fsfe.org/news/nl/nl-201704.et.html
├───OK─── https://fsfe.org/associates/associates.et.html
├───OK─── https://fsfe.org/news/2017/news-20170315-01.et.html
├───OK─── https://fsfe.org/activities/ilovefs/whylovefs/gallery.et.html
├───OK─── https://fsfe.org/news/2017/news-20170321-01.et.html
├───OK─── https://fsfe.org/news/2017/news-20170116-01.et.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.et.html#id-fellowship-seats (HTTP_404)
Finished! 103 links found. 97 excluded. 1 broken.
