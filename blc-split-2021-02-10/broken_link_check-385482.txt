Getting links from: https://fsfe.org/about/localteams#netherlands
├───OK─── https://fsfe.org/about/index.en.html
├───OK─── https://fsfe.org/about/legal/legal.en.html
├───OK─── https://fsfe.org/about/people/kirschner/index.en.html
├─BROKEN─ https://fsfe.org/about/codeofconduct/index.en.html (HTTP_404)
Finished! 83 links found. 79 excluded. 1 broken.
