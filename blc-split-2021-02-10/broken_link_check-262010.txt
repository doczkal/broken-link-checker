Getting links from: https://fsfe.org/about/people/testimonials.hu.html#kroah-hartman
├───OK─── https://fsfe.org/about/people/interviews/cryptie.hu.html#video-30s-cryptie
├───OK─── https://fsfe.org/about/people/interviews/lequertier.hu.html#video-30s-vincent
├───OK─── https://fsfe.org/about/people/interviews/zerolo.hu.html#video-30s-tomas
├─BROKEN─ https://fsfe.org/about/people/interviews/bernhard.hu.html#video-30s-bernhard (HTTP_404)
├───OK─── https://fsfe.org/about/people/interviews/weitzhofer.hu.html#interview
├───OK─── https://fsfe.org/about/people/interviews/grun.hu.html#interview
├───OK─── https://fsfe.org/about/people/interviews/ockers.hu.html#video-30s-andre
├───OK─── https://fsfe.org/about/people/interviews/mueller.hu.html#interview
├───OK─── https://fsfe.org/about/people/interviews/snow.hu.html#interview
├───OK─── https://fsfe.org/about/people/interviews/gkotsopoulou.hu.html#interview
Finished! 122 links found. 112 excluded. 1 broken.
