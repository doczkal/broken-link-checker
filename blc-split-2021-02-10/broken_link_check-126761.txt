Getting links from: https://fsfe.org/activities/gplv3/gplv3.ca.html
├───OK─── https://fsfe.org/activities/gplv3/fisl-rms-transcript.ca.html
├───OK─── https://fsfe.org/activities/gplv3/diff-draft1-draft2.ca.html
├───OK─── https://fsfe.org/activities/gplv3/diff-gplv2-draft2.ca.html
├───OK─── https://fsfe.org/activities/gplv3/bangalore-rms-transcript.ca.html
├─BROKEN─ https://fsfe.org/en/fellows/ciaran/weblog/alan_cox_5_minutes_on_gplv3_plus_comments (HTTP_404)
├───OK─── https://fsfe.org/activities/gplv3/torino-rms-transcript.ca.html
├───OK─── https://fsfe.org/activities/gplv3/drm-and-gplv3.ca.html
├───OK─── https://fsfe.org/activities/gplv3/patents-and-gplv3.ca.html
Finished! 100 links found. 92 excluded. 1 broken.
