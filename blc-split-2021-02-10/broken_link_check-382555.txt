Getting links from: https://fsfe.org/news/2009/news-20090620-01.tr.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.tr.html (HTTP_404)
├─BROKEN─ https://fsfe.org/about/holz/holz.tr.html (HTTP_404)
├─BROKEN─ https://fsfe.org/activities/self/SELF-LegalPolicy-0.9_1.pdf (HTTP_404)
├───OK─── https://fsfe.org/activities/stacs/london.tr.html
├───OK─── https://fsfe.org/activities/stacs/belgrade.tr.html
Finished! 127 links found. 122 excluded. 3 broken.
