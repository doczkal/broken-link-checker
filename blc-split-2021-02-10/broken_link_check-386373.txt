Getting links from: https://fsfe.org/news/nl/nl-201501.pt.html
├───OK─── https://fsfe.org/news/2014/news-20141218-02.pt.html
├───OK─── https://fsfe.org/fellowship/about/fellowship.pt.html#youget
├─BROKEN─ https://fsfe.org/about/legal/constitution.pt.html#id-fellowship-seats (HTTP_404)
├─BROKEN─ https://fsfe.org/about/ojasild (HTTP_404)
Finished! 89 links found. 85 excluded. 2 broken.
