Getting links from: https://fsfe.org/news/nl/nl-201310.sr.html
├───OK─── https://fsfe.org/freesoftware/basics/gnuproject.sr.html
├───OK─── https://fsfe.org/news/2013/news-20130926-01.sr.html
├───OK─── https://fsfe.org/news/2013/news-20130918-01.sr.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.sr.html (HTTP_404)
Finished! 120 links found. 116 excluded. 1 broken.
