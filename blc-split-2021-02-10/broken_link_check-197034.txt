Getting links from: https://fsfe.org/news/2013/news-20130730-01.et.html
├───OK─── https://fsfe.org/contribute/translators/index.et.html
├───OK─── https://fsfe.org/index.et.html
├───OK─── https://fsfe.org/about/about.et.html
├───OK─── https://fsfe.org/activities/activities.et.html
├───OK─── https://fsfe.org/contribute/contribute.et.html
├───OK─── https://fsfe.org/press/press.et.html
├─BROKEN─ https://fsfe.org/news/2013/2013-07-26_Open_Letter_to_NEC.et.html (HTTP_404)
├───OK─── https://fsfe.org/tags/tagged-ee.et.html
├───OK─── https://fsfe.org/tags/tagged-electronic-voting.et.html
├───OK─── https://fsfe.org/about/js-licences.et.html
├───OK─── https://fsfe.org/contact/contact.et.html
├───OK─── https://fsfe.org/about/legal/imprint.et.html
├───OK─── https://fsfe.org/about/transparency-commitment.et.html
├───OK─── https://fsfe.org/contribute/web/web.et.html
Finished! 68 links found. 54 excluded. 1 broken.
