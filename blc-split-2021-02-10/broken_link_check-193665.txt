Getting links from: https://fsfe.org/news/nl/nl-201502.sr.html
├───OK─── https://fsfe.org/news/nl/nl-201502.sq.html
├─BROKEN─ https://fsfe.org/about/ojasild/ojasild.sr.html (HTTP_404)
├─BROKEN─ https://fsfe.org/about/ojasild (HTTP_404)
Finished! 95 links found. 92 excluded. 2 broken.
