Getting links from: https://fsfe.org/activities/igf/wgig.en.html
├─BROKEN─ https://fsfe.org/activities/igf/WGIG-WP-Cybercrime-Comments.pdf (HTTP_404)
├─BROKEN─ https://fsfe.org/activities/igf/WGIG-WP-IPR-Comments.pdf (HTTP_404)
├───OK─── https://fsfe.org/activities/igf/index.en.html
Finished! 59 links found. 56 excluded. 2 broken.
