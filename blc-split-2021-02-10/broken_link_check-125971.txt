Getting links from: https://fsfe.org/about/funds/2005.sq.html
├───OK─── https://fsfe.org/about/funds/2005.de.html
├───OK─── https://fsfe.org/about/funds/2005.el.html
├───OK─── https://fsfe.org/about/funds/2005.fr.html
├───OK─── https://fsfe.org/help/thankgnus-2005.sq.html
├─BROKEN─ https://fsfe.org/about/legal/de/de.sq.html (HTTP_404)
Finished! 87 links found. 82 excluded. 1 broken.
