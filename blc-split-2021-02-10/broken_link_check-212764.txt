Getting links from: https://fsfe.org/activities/gplv3/europe-gplv3-conference.nb.html
├───OK─── https://fsfe.org/activities/gplv3/europe-gplv3-conference.ca.html
├───OK─── https://fsfe.org/activities/gplv3/europe-gplv3-conference.de.html
├───OK─── https://fsfe.org/activities/gplv3/europe-gplv3-conference.en.html
├───OK─── https://fsfe.org/activities/gplv3/europe-gplv3-conference.fr.html
├───OK─── https://fsfe.org/activities/gplv3/europe-gplv3-conference.it.html
├─BROKEN─ https://fsfe.org/activities/gplv3/GPLv3-logo-red.png (HTTP_404)
├───OK─── https://fsfe.org/about/oriordan/oriordan.nb.html
├───OK─── https://fsfe.org/about/maffulli/maffulli.nb.html
├───OK─── https://fsfe.org/activities/gplv3/fsfe-gplv3-georg-greve.vorbis.ogg.torrent
├───OK─── https://fsfe.org/activities/gplv3/fsfe-gplv3-georg-greve.theora.ogg.torrent
├───OK─── https://fsfe.org/activities/gplv3/fsfe-gplv3-current-fsfe-projects.vorbis.ogg.torrent
├───OK─── https://fsfe.org/activities/gplv3/fsfe-gplv3-current-fsfe-projects.theora.ogg.torrent
├───OK─── https://fsfe.org/activities/gplv3/fsfe-gplv3-awareness-and-adoption.vorbis.ogg.torrent
├───OK─── https://fsfe.org/activities/gplv3/fsfe-gplv3-awareness-and-adoption.theora.ogg.torrent
├───OK─── https://fsfe.org/activities/gplv3/fsfe-gplv3-pablo-machon.vorbis.ogg.torrent
├───OK─── https://fsfe.org/activities/gplv3/fsfe-gplv3-pablo-machon.theora.ogg.torrent
├───OK─── https://fsfe.org/activities/gplv3/fsfe-gplv3-discussion-committees.vorbis.ogg.torrent
├───OK─── https://fsfe.org/activities/gplv3/fsfe-gplv3-discussion-committees.theora.ogg.torrent
├───OK─── https://fsfe.org/activities/gplv3/fsfe-gplv3-drm-enforcement.vorbis.ogg.torrent
├───OK─── https://fsfe.org/activities/gplv3/fsfe-gplv3-drm-enforcement.theora.ogg.torrent
Finished! 103 links found. 83 excluded. 1 broken.
