Getting links from: https://fsfe.org/about/people/mehl/mehl.cs.html
├───OK─── https://fsfe.org/activities/radiodirective/index.cs.html
├─BROKEN─ https://fsfe.org/de (HTTP_404)
├───OK─── https://fsfe.org/activities/drm/index.cs.html
├─BROKEN─ https://fsfe.org/de (HTTP_404)
├───OK─── https://fsfe.org/contribute/internship.cs.html
├─BROKEN─ https://fsfe.org/de (HTTP_404)
Finished! 82 links found. 76 excluded. 3 broken.
