Getting links from: https://fsfe.org/news/nl/nl-201105.hu.html
├───OK─── https://fsfe.org/activities/swpat/letter-20110406.hu.html
├───OK─── https://fsfe.org/activities/swpat/novell-cptn.hu.html
├─BROKEN─ https://fsfe.org/join.hu.html (HTTP_404)
├───OK─── https://fsfe.org/activities/pdfreaders/follow-up.hu.html
├───OK─── https://fsfe.org/about/tuke/tuke.hu.html
Finished! 103 links found. 98 excluded. 1 broken.
