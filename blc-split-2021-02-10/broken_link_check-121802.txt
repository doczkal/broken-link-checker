Getting links from: https://fsfe.org/news/nl/nl-201302.ro.html#
├───OK─── https://fsfe.org/activities/ilovefs/2013/index.ro.html
├───OK─── https://fsfe.org/activities/ilovefs/2013/whylovefs.ro.html
├─BROKEN─ https://fsfe.org/activities/ilovefs/2013/banners.ro.html (HTTP_404)
├─BROKEN─ https://fsfe.org/order/promoorder.ro.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.ro.html (HTTP_404)
Finished! 119 links found. 114 excluded. 3 broken.
