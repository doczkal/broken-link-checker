Getting links from: https://fsfe.org/news/nl/nl-201105.it.html
├───OK─── https://fsfe.org/activities/swpat/letter-20110406.it.html
├─BROKEN─ https://fsfe.org/join.it.html (HTTP_404)
├───OK─── https://fsfe.org/news/2011/news-20110418-01.it.html
├───OK─── https://fsfe.org/about/people/roy/roy.it.html
├───OK─── https://fsfe.org/about/tuke/tuke.it.html
Finished! 100 links found. 95 excluded. 1 broken.
