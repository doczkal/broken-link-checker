Getting links from: https://fsfe.org/news/nl/nl-201105.sl.html
├───OK─── https://fsfe.org/activities/swpat/letter-20110406.sl.html
├───OK─── https://fsfe.org/activities/swpat/novell-cptn.sl.html
├─BROKEN─ https://fsfe.org/join.sl.html (HTTP_404)
├───OK─── https://fsfe.org/activities/pdfreaders/follow-up.sl.html
├───OK─── https://fsfe.org/about/tuke/tuke.sl.html
Finished! 103 links found. 98 excluded. 1 broken.
