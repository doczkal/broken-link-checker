Getting links from: https://fsfe.org/news/nl/nl-201704.sk.html
├───OK─── https://fsfe.org/news/nl/nl-201704.en.html
├───OK─── https://fsfe.org/news/2017/news-20170321-01.sk.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.sk.html#id-fellowship-seats (HTTP_404)
Finished! 103 links found. 100 excluded. 1 broken.
