Getting links from: https://fsfe.org/about/people/roy/roy.fi.html
├───OK─── https://fsfe.org/activities/ftf/ftf.fi.html
├─BROKEN─ https://fsfe.org/fr/index.fi.html (HTTP_404)
├─BROKEN─ https://fsfe.org/legal (HTTP_404)
Finished! 81 links found. 78 excluded. 2 broken.
