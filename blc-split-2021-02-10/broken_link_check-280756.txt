Getting links from: https://fsfe.org/activities/swpat/swpat.ru.html
├───OK─── https://fsfe.org/activities/ipred2/ipred2.ru.html
├─BROKEN─ https://fsfe.org/activities/swpat/documents/iprip.ru.html (HTTP_404)
├───OK─── https://fsfe.org/activities/gplv3/gplv3.ru.html
├───OK─── https://fsfe.org/activities/swpat/fsfe-patstrat-response.ru.html
├───OK─── https://fsfe.org/activities/swpat/second-reading-bullets.ru.html
├───OK─── https://fsfe.org/activities/swpat/memorandum.ru.html
Finished! 83 links found. 77 excluded. 1 broken.
