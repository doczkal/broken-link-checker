Getting links from: https://fsfe.org/news/nl/nl-201302.uk.html
├───OK─── https://fsfe.org/activities/ilovefs/2013/index.uk.html
├───OK─── https://fsfe.org/activities/ilovefs/2013/whylovefs.uk.html
├─BROKEN─ https://fsfe.org/activities/ilovefs/2013/banners.uk.html (HTTP_404)
├─BROKEN─ https://fsfe.org/order/promoorder.uk.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.uk.html (HTTP_404)
Finished! 119 links found. 114 excluded. 3 broken.
