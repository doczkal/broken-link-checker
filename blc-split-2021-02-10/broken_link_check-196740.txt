Getting links from: https://fsfe.org/news/nl/nl-201912.nl.html
├───OK─── https://fsfe.org/news/newsletter.nl.html
├─BROKEN─ https://fsfe.org/nieuws/2019/nieuws-20191022-01.nl.html (HTTP_404)
├───OK─── https://fsfe.org/events/index.nl.html#id-the-fsfe-assembly-at-the-chaos-communication-congress-36c3-in-leipzig-germany
├─BROKEN─ https://fsfe.org/nieuws/2019/nieuws-20191205-01.nl.html (HTTP_404)
├───OK─── https://fsfe.org/tags/tagged-community.nl.html
Finished! 98 links found. 93 excluded. 2 broken.
