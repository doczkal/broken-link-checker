Getting links from: https://fsfe.org/news/nl/nl-201302.ca.html
├───OK─── https://fsfe.org/news/2012/news-20121211-01.ca.html
├───OK─── https://fsfe.org/news/2012/news-20121217-01.ca.html
├───OK─── https://fsfe.org/news/2012/report-2012.ca.html
├───OK─── https://fsfe.org/activities/ilovefs/2013/index.ca.html
├───OK─── https://fsfe.org/activities/ilovefs/2013/whylovefs.ca.html
├─BROKEN─ https://fsfe.org/activities/ilovefs/2013/banners.ca.html (HTTP_404)
├─BROKEN─ https://fsfe.org/order/promoorder.ca.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.ca.html (HTTP_404)
Finished! 119 links found. 111 excluded. 3 broken.
