Getting links from: https://fsfe.org/news/nl/nl-201008.fr.html
├─BROKEN─ https://fsfe.org/about/costa/costa.fr.html (HTTP_404)
├───OK─── https://fsfe.org/freesoftware/transcripts/rms-2009-05-22-eliberatica.fr.html
├───OK─── https://fsfe.org/news/2010/news-20100702-01.fr.html
├───OK─── https://fsfe.org/events/events.fr.rss
├───OK─── https://fsfe.org/tags/tagged-rmll.fr.html
Finished! 93 links found. 88 excluded. 1 broken.
