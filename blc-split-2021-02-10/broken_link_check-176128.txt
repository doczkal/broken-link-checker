Getting links from: https://fsfe.org/news/nl/nl-201310.nl.html
├───OK─── https://fsfe.org/freesoftware/basics/gnuproject.nl.html
├───OK─── https://fsfe.org/news/2013/news-20130926-01.nl.html
├───OK─── https://fsfe.org/news/2013/news-20130918-01.nl.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.nl.html (HTTP_404)
Finished! 120 links found. 116 excluded. 1 broken.
