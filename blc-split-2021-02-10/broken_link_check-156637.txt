Getting links from: https://fsfe.org/news/2007/news-20070630-01.ar.html
├───OK─── https://fsfe.org/news/2007/news-20070630-01.el.html
├───OK─── https://fsfe.org/news/2007/news-20070630-01.nl.html
├───OK─── https://fsfe.org/activities/ipred2/index.ar.html
├───OK─── https://fsfe.org/activities/igf/a2k.ar.html
├───OK─── https://fsfe.org/activities/igf/dcos.ar.html
├─BROKEN─ https://fsfe.org/ftf (HTTP_404)
├───OK─── https://fsfe.org/activities/gplv3/index.ar.html#transcripts
├───OK─── https://fsfe.org/activities/self/index.ar.html
├─BROKEN─ https://fsfe.org/fellows/greve/img/zrh_hoist (HTTP_404)
├─BROKEN─ https://fsfe.org/fellows/meetings/2006_bolzano (HTTP_404)
Finished! 105 links found. 95 excluded. 3 broken.
