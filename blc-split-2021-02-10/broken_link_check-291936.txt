Getting links from: https://fsfe.org/news/2015/news-20150303-01.sr.html
├───OK─── https://fsfe.org/news/2015/news-20150303-01.es.html
├───OK─── https://fsfe.org/news/2015/news-20150303-01.nl.html
├─BROKEN─ https://fsfe.org/activities/ilovefs/whylovefs/photos/gallery/ilovefs-gallery-thumb-39.jpg (HTTP_404)
├─BROKEN─ https://fsfe.org/activities/ilovefs/whylovefs/photos/gallery/ilovefs-gallery-thumb-35.jpg (HTTP_404)
├─BROKEN─ https://fsfe.org/activities/ilovefs/whylovefs/photos/gallery/ilovefs-gallery-thumb-25.jpg (HTTP_404)
├───OK─── https://fsfe.org/news/2015/graphics/robo-romeo.jpg
├───OK─── https://fsfe.org/news/2015/graphics/ilovefs-comic-jaime-le-logiciel-libre-thumb.png
├───OK─── https://fsfe.org/activities/ilovefs/whylovefs/whylovefs.sr.html
├───OK─── https://fsfe.org/order/order.sr.html
Finished! 104 links found. 95 excluded. 3 broken.
