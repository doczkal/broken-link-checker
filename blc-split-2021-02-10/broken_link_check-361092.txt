Getting links from: https://fsfe.org/news/nl/nl-201501.ro.html
├───OK─── https://fsfe.org/fellowship/about/fellowship.ro.html#youget
├─BROKEN─ https://fsfe.org/about/legal/constitution.ro.html#id-fellowship-seats (HTTP_404)
├───OK─── https://fsfe.org/news/2014/news-20141219-01.ro.html
├───OK─── https://fsfe.org/news/2014/news-20141212-01.ro.html
├─BROKEN─ https://fsfe.org/about/ojasild (HTTP_404)
Finished! 88 links found. 83 excluded. 2 broken.
