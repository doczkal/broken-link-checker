Getting links from: https://fsfe.org/news/nl/nl-201308.zh.html
├─BROKEN─ https://fsfe.org/news/2013/2013-07-26_Open_Letter_to_NEC.zh.html (HTTP_404)
├───OK─── https://fsfe.org/news/2013/news-20130712-01.zh.html
├───OK─── https://fsfe.org/freesoftware/standards/transparency-letter.zh.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.zh.html (HTTP_404)
Finished! 112 links found. 108 excluded. 2 broken.
