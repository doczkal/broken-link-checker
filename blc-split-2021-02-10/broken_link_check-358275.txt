Getting links from: https://fsfe.org/news/nl/nl-201112.nb.html
├───OK─── https://fsfe.org/news/2011/news-20111128-01.nb.html
├───OK─── https://fsfe.org/activities/pdfreaders/buglist.nb.html
├───OK─── https://fsfe.org/activities/pdfreaders/letter.nb.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.nb.html (HTTP_404)
Finished! 108 links found. 104 excluded. 1 broken.
