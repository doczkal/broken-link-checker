Getting links from: https://fsfe.org/news/2017/index.nl.html
├───OK─── https://fsfe.org/news/2017/news-20171207-02.nl.html
├───OK─── https://fsfe.org/tags/tagged-coc.nl.html
├───OK─── https://fsfe.org/news/2017/news-20171024-01.nl.html
├───OK─── https://fsfe.org/activities/ftf/fla.nl.html
├───OK─── https://fsfe.org/tags/tagged-fla.nl.html
├───OK─── https://fsfe.org/tags/tagged-koalition-freies-wissen.nl.html
├───OK─── https://fsfe.org/news/2017/graphics/Digitalomat_Logo.png
├─BROKEN─ https://fsfe.org/about/legal/constitution.nl.html#id-fellowship-seats (HTTP_404)
Finished! 302 links found. 293 excluded. 2 broken.
