Getting links from: https://fsfe.org/news/nl/nl-201810.ru.html
├───OK─── https://fsfe.org/news/2018/news-20181023-02.ru.html
├─BROKEN─ https://fsfe.org/about/mancheva/mancheva (HTTP_404)
├───OK─── https://fsfe.org/news/2018/news-20181010-01.ru.html
├───OK─── https://fsfe.org/news/2018/news-20180913-01.ru.html
Finished! 94 links found. 90 excluded. 1 broken.
