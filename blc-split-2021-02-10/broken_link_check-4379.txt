Getting links from: https://fsfe.org/news/2011/news-20110401-01.en.html
├─BROKEN─ https://fsfe.org/order/2010/images/pen-blue-small.jpg (HTTP_404)
├─BROKEN─ https://fsfe.org/order/2010/images/pen-green-small.jpg (HTTP_404)
├───OK─── https://fsfe.org/graphics/aprilpen1.jpg
├───OK─── https://fsfe.org/graphics/aprilpen2.jpg
├───OK─── https://fsfe.org/graphics/aprilpen3.jpg
├───OK─── https://fsfe.org/graphics/aprilpen4.jpg
Finished! 60 links found. 54 excluded. 2 broken.
