Getting links from: https://fsfe.org/activities/igf/sovsoft.ca.html
├───OK─── https://fsfe.org/about/people/greve/greve.ca.html
├───OK─── https://fsfe.org/activities/wsis/index.ca.html
├─BROKEN─ https://fsfe.org/activities/igf/ref3 (HTTP_404)
├─BROKEN─ https://fsfe.org/activities/igf/4 (HTTP_404)
├───OK─── https://fsfe.org/activities/ms-vs-eu/index.ca.html
├───OK─── https://fsfe.org/activities/wipo/fser.ca.html
Finished! 91 links found. 85 excluded. 2 broken.
