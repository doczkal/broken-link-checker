Getting links from: https://fsfe.org/news/2013/index.bs.html
├───OK─── https://fsfe.org/news/2013/news-20131213-01.bs.html
├───OK─── https://fsfe.org/news/2013/news-20130927-01.bs.html
├───OK─── https://fsfe.org/news/2013/news-20130920-01.bs.html
├─BROKEN─ https://fsfe.org/news/2013/2013-07-26_Open_Letter_to_NEC.bs.html (HTTP_404)
├───OK─── https://fsfe.org/news/2013/news-20130327-02.bs.html
├───OK─── https://fsfe.org/news/2013/news-20130226-01.bs.html
Finished! 289 links found. 283 excluded. 1 broken.
