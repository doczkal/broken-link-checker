Getting links from: https://fsfe.org/news/nl/nl-201310.es.html
├───OK─── https://fsfe.org/freesoftware/basics/gnuproject.es.html
├───OK─── https://fsfe.org/news/2013/news-20130926-01.es.html
├───OK─── https://fsfe.org/news/2013/news-20130918-01.es.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.es.html (HTTP_404)
Finished! 119 links found. 115 excluded. 1 broken.
