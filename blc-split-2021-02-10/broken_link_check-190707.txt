Getting links from: https://fsfe.org/news/nl/nl-201105.tr.html
├───OK─── https://fsfe.org/activities/swpat/letter-20101222.tr.html
├───OK─── https://fsfe.org/activities/swpat/letter-20110406.tr.html
├───OK─── https://fsfe.org/activities/swpat/novell-cptn.tr.html
├─BROKEN─ https://fsfe.org/join.tr.html (HTTP_404)
├───OK─── https://fsfe.org/activities/pdfreaders/follow-up.tr.html
├───OK─── https://fsfe.org/news/2011/news-20110418-01.tr.html
├───OK─── https://fsfe.org/about/tuke/tuke.tr.html
Finished! 103 links found. 96 excluded. 1 broken.
