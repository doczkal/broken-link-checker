Getting links from: https://fsfe.org/news/2008/index.en.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.en.html (HTTP_404)
├───OK─── https://fsfe.org/news/2008/news-20080220-01.en.html
Finished! 135 links found. 133 excluded. 1 broken.
