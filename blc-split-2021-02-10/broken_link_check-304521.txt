Getting links from: https://fsfe.org/news/2019/news-20191022-01.ca.html
├───OK─── https://fsfe.org/contribute/promopics/ilovefs-sticker-444px.jpg
├─BROKEN─ https://fsfe.org/news/2019/about/codeofconduct (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/contribute/web/web.ca.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/news-20191002.ca.html (HTTP_404)
├───OK─── https://fsfe.org/order/2017/kidstshirt-fork-red-front-large-350px.jpg
Finished! 222 links found. 217 excluded. 3 broken.
