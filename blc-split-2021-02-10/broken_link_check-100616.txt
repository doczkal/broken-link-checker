Getting links from: https://fsfe.org/about/funds/2001.hr.html
├───OK─── https://fsfe.org/about/funds/2001.en.html
├───OK─── https://fsfe.org/about/funds/2001.fr.html
├───OK─── https://fsfe.org/about/funds/2001.it.html
├───OK─── https://fsfe.org/about/funds/2001.nl.html
├───OK─── https://fsfe.org/about/funds/2001.ru.html
├───OK─── https://fsfe.org/help/thankgnus-2001.hr.html
├───OK─── https://fsfe.org/about/legal/index.hr.html
├─BROKEN─ https://fsfe.org/about/legal/de/de.hr.html (HTTP_404)
├───OK─── https://fsfe.org/about/people/greve/index.hr.html
Finished! 83 links found. 74 excluded. 1 broken.
