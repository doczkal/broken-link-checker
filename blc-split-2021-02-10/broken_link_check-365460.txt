Getting links from: https://fsfe.org/news/2016/news-20160224-01.en.html
├─BROKEN─ https://fsfe.org/activities/ilovefs/whylovefs/photos/gallery/ilovefs-gallery-thumb-25.jpg (HTTP_404)
├───OK─── https://fsfe.org/news/2015/news-20150325-01.en.html
├───OK─── https://fsfe.org/activities/pdfreaders/pdfreaders.en.html
├───OK─── https://fsfe.org/news/2015/news-20150605-02.en.html
├───OK─── https://fsfe.org/news/2015/news-20150817-01.en.html
Finished! 86 links found. 81 excluded. 1 broken.
