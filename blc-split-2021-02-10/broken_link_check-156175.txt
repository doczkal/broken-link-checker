Getting links from: https://fsfe.org/news/nl/nl-201302.ar.html
├───OK─── https://fsfe.org/activities/ilovefs/2013/index.ar.html
├───OK─── https://fsfe.org/activities/ilovefs/2013/whylovefs.ar.html
├─BROKEN─ https://fsfe.org/activities/ilovefs/2013/banners.ar.html (HTTP_404)
├─BROKEN─ https://fsfe.org/order/promoorder.ar.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.ar.html (HTTP_404)
Finished! 120 links found. 115 excluded. 3 broken.
