Getting links from: https://fsfe.org/news/nl/nl-201203.mk.html
├───OK─── https://fsfe.org/news/nl/nl-201203.de.html
├───OK─── https://fsfe.org/news/nl/nl-201203.el.html
├───OK─── https://fsfe.org/news/nl/nl-201203.it.html
├───OK─── https://fsfe.org/news/2012/news-20120228-01.mk.html
├───OK─── https://fsfe.org/activities/ilovefs/2012/whylovefs.mk.html
├───OK─── https://fsfe.org/news/2012/news-20120204-01.mk.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.mk.html (HTTP_404)
Finished! 119 links found. 112 excluded. 1 broken.
