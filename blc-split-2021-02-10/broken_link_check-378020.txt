Getting links from: https://fsfe.org/news/nl/nl-201308.nn.html
├───OK─── https://fsfe.org/news/nl/nl-201308.en.html
├─BROKEN─ https://fsfe.org/news/2013/2013-07-26_Open_Letter_to_NEC.nn.html (HTTP_404)
├───OK─── https://fsfe.org/news/2013/news-20130712-01.nn.html
├───OK─── https://fsfe.org/freesoftware/standards/transparency-letter.nn.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.nn.html (HTTP_404)
Finished! 112 links found. 107 excluded. 2 broken.
