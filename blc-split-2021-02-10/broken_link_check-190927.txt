Getting links from: https://fsfe.org/about/funds/2007.tr.html
├───OK─── https://fsfe.org/about/funds/2007.de.html
├───OK─── https://fsfe.org/about/funds/2007.fr.html
├───OK─── https://fsfe.org/about/funds/2007.it.html
├───OK─── https://fsfe.org/about/funds/2007.nl.html
├───OK─── https://fsfe.org/help/thankgnus-2007.tr.html
├───OK─── https://fsfe.org/about/legal/index.tr.html
├─BROKEN─ https://fsfe.org/about/legal/de/de.tr.html (HTTP_404)
├───OK─── https://fsfe.org/about/people/greve/index.tr.html
Finished! 87 links found. 79 excluded. 1 broken.
