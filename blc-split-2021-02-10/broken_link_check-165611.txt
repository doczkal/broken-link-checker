Getting links from: https://fsfe.org/news/nl/nl-201306.uk.html
├───OK─── https://fsfe.org/news/nl/nl-201306.fr.html
├───OK─── https://fsfe.org/events/2013/linuxtag-2013.uk.html
├───OK─── https://fsfe.org/freesoftware/legal/flashingdevices.uk.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.uk.html (HTTP_404)
Finished! 117 links found. 113 excluded. 1 broken.
