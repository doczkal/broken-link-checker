Getting links from: https://fsfe.org/contribute/translators/translators.nb.html
├───OK─── https://fsfe.org/contribute/translators/index.nb.html
├───OK─── https://fsfe.org/index.nb.html
├───OK─── https://fsfe.org/about/about.nb.html
├───OK─── https://fsfe.org/activities/activities.nb.html
├───OK─── https://fsfe.org/contribute/contribute.nb.html
├───OK─── https://fsfe.org/about/index.nb.html
├───OK─── https://fsfe.org/about/principles.nb.html
├─BROKEN─ https://fsfe.org/contribute/translators/web.nb.html (HTTP_404)
├───OK─── https://fsfe.org/about/js-licences.nb.html
├───OK─── https://fsfe.org/about/legal/imprint.nb.html
├───OK─── https://fsfe.org/about/transparency-commitment.nb.html
├───OK─── https://fsfe.org/contribute/web/web.nb.html
Finished! 91 links found. 79 excluded. 1 broken.
