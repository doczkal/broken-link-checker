Getting links from: https://fsfe.org/news/2009/news-20090620-01.es.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.es.html (HTTP_404)
├─BROKEN─ https://fsfe.org/about/holz/holz.es.html (HTTP_404)
├─BROKEN─ https://fsfe.org/activities/self/SELF-LegalPolicy-0.9_1.pdf (HTTP_404)
├───OK─── https://fsfe.org/activities/stacs/london.es.html
├───OK─── https://fsfe.org/news/newsletter.es.html
Finished! 128 links found. 123 excluded. 3 broken.
