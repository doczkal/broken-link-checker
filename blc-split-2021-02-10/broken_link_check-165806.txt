Getting links from: https://fsfe.org/news/2009/news-20090620-01.uk.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.uk.html (HTTP_404)
├─BROKEN─ https://fsfe.org/about/holz/holz.uk.html (HTTP_404)
├─BROKEN─ https://fsfe.org/activities/self/SELF-LegalPolicy-0.9_1.pdf (HTTP_404)
├───OK─── https://fsfe.org/activities/stacs/london.uk.html
├───OK─── https://fsfe.org/activities/stacs/belgrade.uk.html
Finished! 127 links found. 122 excluded. 3 broken.
