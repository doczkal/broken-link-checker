Getting links from: https://fsfe.org/news/2016/news-20160224-01.it.html
├───OK─── https://fsfe.org/news/2016/news-20160224-01.nl.html
├───OK─── https://fsfe.org/news/2016/graphics/v2015martin.jpg
├─BROKEN─ https://fsfe.org/activities/ilovefs/whylovefs/photos/gallery/ilovefs-gallery-thumb-25.jpg (HTTP_404)
├───OK─── https://fsfe.org/news/2016/graphics/dfd-venezuela-merida-05.jpg
├───OK─── https://fsfe.org/news/2016/graphics/router.jpg
Finished! 89 links found. 84 excluded. 1 broken.
