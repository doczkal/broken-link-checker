Getting links from: https://fsfe.org/news/2007/news-20070630-01.sv.html
├───OK─── https://fsfe.org/activities/ms-vs-eu/index.sv.html
├───OK─── https://fsfe.org/activities/ipred2/index.sv.html
├───OK─── https://fsfe.org/activities/wsis/index.sv.html
├───OK─── https://fsfe.org/activities/igf/index.sv.html
├───OK─── https://fsfe.org/activities/igf/a2k.sv.html
├───OK─── https://fsfe.org/activities/igf/dcos.sv.html
├───OK─── https://fsfe.org/activities/wipo/index.sv.html
├───OK─── https://fsfe.org/activities/wipo/statement-20050721.sv.html
├─BROKEN─ https://fsfe.org/ftf (HTTP_404)
├───OK─── https://fsfe.org/activities/gplv3/index.sv.html#transcripts
├───OK─── https://fsfe.org/activities/self/index.sv.html
├─BROKEN─ https://fsfe.org/fellows/greve/img/zrh_hoist (HTTP_404)
├─BROKEN─ https://fsfe.org/fellows/meetings/2006_bolzano (HTTP_404)
Finished! 104 links found. 91 excluded. 3 broken.
