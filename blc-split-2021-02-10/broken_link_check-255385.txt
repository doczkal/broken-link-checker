Getting links from: https://fsfe.org/contact/index.sr.html
├───OK─── https://fsfe.org/about/people/kirschner/index.sr.html
├─BROKEN─ https://fsfe.org/about/codeofconduct/index.sr.html (HTTP_404)
Finished! 86 links found. 84 excluded. 1 broken.
