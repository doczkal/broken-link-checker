Getting links from: https://fsfe.org/news/2017/index.it.html
├───OK─── https://fsfe.org/news/2017/news-20171211-01.it.html
├───OK─── https://fsfe.org/news/2017/news-20171207-02.it.html
├───OK─── https://fsfe.org/news/2017/news-20170911-01.it.html
├───OK─── https://fsfe.org/news/2017/news-20170906-01.it.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.it.html#id-fellowship-seats (HTTP_404)
Finished! 300 links found. 294 excluded. 2 broken.
