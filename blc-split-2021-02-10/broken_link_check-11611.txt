Getting links from: https://fsfe.org/news/nl/nl-201105.cs.html
├───OK─── https://fsfe.org/activities/swpat/letter-20101222.cs.html
├───OK─── https://fsfe.org/activities/swpat/letter-20110406.cs.html
├───OK─── https://fsfe.org/activities/swpat/novell-cptn.cs.html
├─BROKEN─ https://fsfe.org/join.cs.html (HTTP_404)
├───OK─── https://fsfe.org/activities/pdfreaders/follow-up.cs.html
├───OK─── https://fsfe.org/about/tuke/tuke.cs.html
Finished! 103 links found. 97 excluded. 1 broken.
