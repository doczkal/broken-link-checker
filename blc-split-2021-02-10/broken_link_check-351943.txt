Getting links from: https://fsfe.org/news/nl/nl-201106.nl.html
├───OK─── https://fsfe.org/activities/ms-vs-eu/timeline.nl.html
├───OK─── https://fsfe.org/news/2011/news-20110511-01.nl.html
├───OK─── https://fsfe.org/news/2011/news-20110520-01.nl.html
├─BROKEN─ https://fsfe.org/source/activities/elections/askyourcandidates/askyourcandidates.xhtml (HTTP_404)
├───OK─── https://fsfe.org/events/events.nl.rss
Finished! 90 links found. 85 excluded. 1 broken.
