Getting links from: https://fsfe.org/news/nl/nl-201205.ar.html
├─BROKEN─ https://fsfe.org/activities/swpat/swapt.ar.html (HTTP_404)
├─BROKEN─ https://fsfe.org/project/os/def.ar.html (HTTP_404)
├───OK─── https://fsfe.org/freesoftware/standards/uk-standards-consultation.ar.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.ar.html (HTTP_404)
Finished! 115 links found. 111 excluded. 3 broken.
