Getting links from: https://fsfe.org/news/nl/nl-201105.nn.html
├───OK─── https://fsfe.org/activities/swpat/letter-20101222.nn.html
├───OK─── https://fsfe.org/activities/swpat/letter-20110406.nn.html
├───OK─── https://fsfe.org/activities/swpat/novell-cptn.nn.html
├─BROKEN─ https://fsfe.org/join.nn.html (HTTP_404)
├───OK─── https://fsfe.org/activities/pdfreaders/follow-up.nn.html
├───OK─── https://fsfe.org/about/tuke/tuke.nn.html
Finished! 103 links found. 97 excluded. 1 broken.
