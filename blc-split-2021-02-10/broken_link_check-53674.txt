Getting links from: https://fsfe.org/news/nl/nl-201207.nl.html
├───OK─── https://fsfe.org/activities/pdfreaders/pdfsprint.nl.html
├───OK─── https://fsfe.org/news/2012/news-20120616-01.nl.html
├───OK─── https://fsfe.org/news/2012/news-20120607-01.nl.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.nl.html (HTTP_404)
Finished! 102 links found. 98 excluded. 1 broken.
