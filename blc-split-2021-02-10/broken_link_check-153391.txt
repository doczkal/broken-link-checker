Getting links from: https://fsfe.org/news/nl/nl-201202.bs.html
├───OK─── https://fsfe.org/news/2012/news-20120131-01.bs.html
├─BROKEN─ https://fsfe.org/activities/ilovefs/2012/banners.bs.html (HTTP_404)
├───OK─── https://fsfe.org/activities/ilovefs/2012/unperfekthaus.bs.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.bs.html (HTTP_404)
Finished! 109 links found. 105 excluded. 2 broken.
