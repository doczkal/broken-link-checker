Getting links from: https://fsfe.org/freesoftware/standards/bt-open-letter.et.html
├─BROKEN─ https://fsfe.org/uk/index.et.html (HTTP_404)
├───OK─── https://fsfe.org/activities/whyfs/whyfs.et.html
Finished! 74 links found. 72 excluded. 1 broken.
