Getting links from: https://fsfe.org/news/2008/news-20081210-01.hu.html
├───OK─── https://fsfe.org/news/2008/news-20081210-01.de.html
├───OK─── https://fsfe.org/news/2008/news-20081210-01.el.html
├───OK─── https://fsfe.org/news/2008/news-20081210-01.es.html
├───OK─── https://fsfe.org/news/2008/news-20081210-01.fr.html
├───OK─── https://fsfe.org/news/2008/news-20081210-01.it.html
├───OK─── https://fsfe.org/news/2008/news-20081210-01.nl.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.hu.html (HTTP_404)
Finished! 68 links found. 61 excluded. 1 broken.
