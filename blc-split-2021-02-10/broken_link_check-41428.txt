Getting links from: https://fsfe.org/news/nl/nl-201208.cs.html
├───OK─── https://fsfe.org/news/nl/nl-201208.en.html
├───OK─── https://fsfe.org/news/2012/news-20120730-01.cs.html
├───OK─── https://fsfe.org/activities/elections/askyourcandidates/askyourcandidates.cs.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.cs.html (HTTP_404)
Finished! 98 links found. 94 excluded. 1 broken.
