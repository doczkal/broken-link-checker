Getting links from: https://fsfe.org/news/nl/nl-201308.sr.html
├───OK─── https://fsfe.org/news/2013/news-20130729-01.sr.html
├───OK─── https://fsfe.org/news/2013/news-20130730-01.sr.html
├─BROKEN─ https://fsfe.org/news/2013/2013-07-26_Open_Letter_to_NEC.sr.html (HTTP_404)
├───OK─── https://fsfe.org/news/2013/news-20130712-01.sr.html
├───OK─── https://fsfe.org/freesoftware/standards/transparency-letter.sr.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.sr.html (HTTP_404)
Finished! 112 links found. 106 excluded. 2 broken.
