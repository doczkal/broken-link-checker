Getting links from: https://fsfe.org/about/people/mehl/mehl.bs.html
├───OK─── https://fsfe.org/activities/radiodirective/index.bs.html
├─BROKEN─ https://fsfe.org/de (HTTP_404)
├───OK─── https://fsfe.org/activities/routers/index.bs.html
├───OK─── https://fsfe.org/activities/android/index.bs.html
├───OK─── https://fsfe.org/activities/drm/index.bs.html
├───OK─── https://fsfe.org/contribute/web/index.bs.html
├─BROKEN─ https://fsfe.org/de (HTTP_404)
├─BROKEN─ https://fsfe.org/de (HTTP_404)
Finished! 82 links found. 74 excluded. 3 broken.
