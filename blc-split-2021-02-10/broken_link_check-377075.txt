Getting links from: https://fsfe.org/news/2018/news-20180215-01.ro.html#
├─BROKEN─ https://fsfe.org/freesoftware/standards/standards.html%3E (HTTP_404)
├───OK─── https://fsfe.org/activities/elections/index.ro.html
├───OK─── https://fsfe.org/tags/tagged-policy.ro.html
├───OK─── https://fsfe.org/contact/contact.ro.html
Finished! 73 links found. 69 excluded. 1 broken.
