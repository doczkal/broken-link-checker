Getting links from: https://fsfe.org/news/nl/nl-201305.fr.html
├───OK─── https://fsfe.org/news/2013/news-20130424-01.fr.html
├───OK─── https://fsfe.org/news/2013/news-20130422-01.fr.html
├───OK─── https://fsfe.org/news/2013/news-20130319-01.fr.html
├───OK─── https://fsfe.org/news/2013/news-20130423-02.fr.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.fr.html (HTTP_404)
Finished! 107 links found. 102 excluded. 1 broken.
