Getting links from: https://fsfe.org/news/nl/nl-201201.ar.html
├─BROKEN─ https://fsfe.org/activities/elections/askyourcandidates/example-questions.xhtml (HTTP_404)
├───OK─── https://fsfe.org/news/2011/news-20111201-02.ar.html
├───OK─── https://fsfe.org/news/2011/news-20111220-01.ar.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.ar.html (HTTP_404)
Finished! 88 links found. 84 excluded. 2 broken.
