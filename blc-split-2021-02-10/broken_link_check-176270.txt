Getting links from: https://fsfe.org/news/nl/nl-201810.fi.html
├───OK─── https://fsfe.org/news/2018/news-20181023-02.fi.html
├─BROKEN─ https://fsfe.org/about/mancheva/mancheva (HTTP_404)
├───OK─── https://fsfe.org/news/2018/news-20181010-01.fi.html
Finished! 94 links found. 91 excluded. 1 broken.
