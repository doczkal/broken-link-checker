Getting links from: https://fsfe.org/news/nl/nl-201202.cs.html
├───OK─── https://fsfe.org/activities/nledu/nledu.cs.html
├───OK─── https://fsfe.org/news/2012/news-20120110-02.cs.html
├───OK─── https://fsfe.org/news/2012/news-20120130-01.cs.html
├───OK─── https://fsfe.org/news/2012/news-20120131-01.cs.html
├───OK─── https://fsfe.org/news/2012/news-20120110-01.cs.html
├───OK─── https://fsfe.org/news/2012/news-20120126-01.cs.html
├─BROKEN─ https://fsfe.org/activities/ilovefs/2012/banners.cs.html (HTTP_404)
├───OK─── https://fsfe.org/activities/ilovefs/2012/unperfekthaus.cs.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.cs.html (HTTP_404)
Finished! 109 links found. 100 excluded. 2 broken.
