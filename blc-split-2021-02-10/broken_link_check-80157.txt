Getting links from: https://fsfe.org/contribute/web/tagging.sr.html#
├─BROKEN─ https://fsfe.org/uk/uk.sr.html (HTTP_404)
├───OK─── https://fsfe.org/tags/tagged.sr.html
├───OK─── https://fsfe.org/contribute/web/css.sr.html
├───OK─── https://fsfe.org/contribute/template.sr.html
Finished! 53 links found. 49 excluded. 1 broken.
