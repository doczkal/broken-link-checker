Getting links from: https://fsfe.org/about/people/mehl/mehl.en.html
├───OK─── https://fsfe.org/activities/radiodirective/index.en.html
├─BROKEN─ https://fsfe.org/de (HTTP_404)
├───OK─── https://fsfe.org/activities/routers/index.en.html
├───OK─── https://fsfe.org/activities/android/index.en.html
├─BROKEN─ https://fsfe.org/de (HTTP_404)
├─BROKEN─ https://fsfe.org/de (HTTP_404)
Finished! 79 links found. 73 excluded. 3 broken.
