Getting links from: https://fsfe.org/news/nl/nl-201212.uk.html
├───OK─── https://fsfe.org/news/2012/news-20121116-01.uk.html
├─BROKEN─ https://fsfe.org/news/nl/lwn.net/Articles/523537/index.uk.html (HTTP_404)
├───OK─── https://fsfe.org/news/2012/news-20121112-01.uk.html
├───OK─── https://fsfe.org/events/index.uk.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.uk.html (HTTP_404)
Finished! 115 links found. 110 excluded. 2 broken.
