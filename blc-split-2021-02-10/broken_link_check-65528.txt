Getting links from: https://fsfe.org/news/2017/index.zh.html
├───OK─── https://fsfe.org/news/2017/news-20171211-01.zh.html
├───OK─── https://fsfe.org/news/2017/news-20171206-01.zh.html
├───OK─── https://fsfe.org/news/2017/news-20171207-02.zh.html
├───OK─── https://fsfe.org/news/2017/news-20170911-01.zh.html
├───OK─── https://fsfe.org/news/2017/news-20170906-01.zh.html
├───OK─── https://fsfe.org/news/2017/news-20170726-01.zh.html
├───OK─── https://fsfe.org/news/2017/news-20170425-01.zh.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.zh.html#id-fellowship-seats (HTTP_404)
Finished! 300 links found. 291 excluded. 2 broken.
