Getting links from: https://fsfe.org/news/nl/nl-201105.da.html
├───OK─── https://fsfe.org/news/nl/nl-201105.el.html
├───OK─── https://fsfe.org/news/nl/nl-201105.it.html
├───OK─── https://fsfe.org/activities/swpat/letter-20110406.da.html
├───OK─── https://fsfe.org/activities/swpat/novell-cptn.da.html
├─BROKEN─ https://fsfe.org/join.da.html (HTTP_404)
Finished! 103 links found. 98 excluded. 1 broken.
