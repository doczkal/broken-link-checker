Getting links from: https://fsfe.org/news/nl/nl-201205.it.html
├─BROKEN─ https://fsfe.org/activities/swpat/swapt.it.html (HTTP_404)
├─BROKEN─ https://fsfe.org/project/os/def.it.html (HTTP_404)
├───OK─── https://fsfe.org/freesoftware/standards/uk-standards-consultation.it.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.it.html (HTTP_404)
Finished! 114 links found. 110 excluded. 3 broken.
