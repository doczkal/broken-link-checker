Getting links from: https://fsfe.org/news/nl/nl-201704.fi.html
├───OK─── https://fsfe.org/news/2017/news-20170328-01.fi.html
├───OK─── https://fsfe.org/news/2017/news-20170315-01.fi.html
├───OK─── https://fsfe.org/activities/ilovefs/whylovefs/gallery.fi.html
├───OK─── https://fsfe.org/news/2017/news-20170321-01.fi.html
├───OK─── https://fsfe.org/news/2017/news-20170116-01.fi.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.fi.html#id-fellowship-seats (HTTP_404)
Finished! 104 links found. 98 excluded. 1 broken.
