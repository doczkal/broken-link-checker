Getting links from: https://fsfe.org/news/2009/news-20090620-01.ar.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.ar.html (HTTP_404)
├─BROKEN─ https://fsfe.org/about/holz/holz.ar.html (HTTP_404)
├───OK─── https://fsfe.org/about/funds/2008.ar.html
├─BROKEN─ https://fsfe.org/activities/self/SELF-LegalPolicy-0.9_1.pdf (HTTP_404)
├───OK─── https://fsfe.org/activities/igf/igf.ar.html
├───OK─── https://fsfe.org/activities/stacs/stacs.ar.html
├───OK─── https://fsfe.org/activities/stacs/london.ar.html
├───OK─── https://fsfe.org/activities/stacs/belgrade.ar.html
Finished! 128 links found. 120 excluded. 3 broken.
