Getting links from: https://fsfe.org/news/nl/nl-201408.el.html
├───OK─── https://fsfe.org/news/nl/nl-201408.fr.html
├─BROKEN─ https://fsfe.org/news/nl/fellowship.fsfe.org/join (HTTP_404)
├─BROKEN─ https://fsfe.org/about/ojasild/ojasild.el.html (HTTP_404)
Finished! 103 links found. 100 excluded. 2 broken.
