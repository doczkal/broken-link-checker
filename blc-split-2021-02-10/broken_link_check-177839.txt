Getting links from: https://fsfe.org/news/nl/nl-201105.nb.html
├───OK─── https://fsfe.org/activities/swpat/letter-20101222.nb.html
├───OK─── https://fsfe.org/activities/swpat/letter-20110406.nb.html
├───OK─── https://fsfe.org/activities/swpat/novell-cptn.nb.html
├─BROKEN─ https://fsfe.org/join.nb.html (HTTP_404)
├───OK─── https://fsfe.org/activities/pdfreaders/follow-up.nb.html
├───OK─── https://fsfe.org/news/2011/news-20110418-01.nb.html
├───OK─── https://fsfe.org/about/tuke/tuke.nb.html
Finished! 103 links found. 96 excluded. 1 broken.
