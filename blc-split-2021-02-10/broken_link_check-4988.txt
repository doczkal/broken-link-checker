Getting links from: https://fsfe.org/news/nl/nl-201308.el.html
├─BROKEN─ https://fsfe.org/news/2013/2013-07-26_Open_Letter_to_NEC.el.html (HTTP_404)
├───OK─── https://fsfe.org/news/2013/news-20130712-01.el.html
├───OK─── https://fsfe.org/freesoftware/standards/transparency-letter.el.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.el.html (HTTP_404)
Finished! 110 links found. 106 excluded. 2 broken.
