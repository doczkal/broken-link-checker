Getting links from: https://fsfe.org/about/funds/2002.nb.html
├───OK─── https://fsfe.org/about/funds/2002.fr.html
├───OK─── https://fsfe.org/about/funds/2002.it.html
├───OK─── https://fsfe.org/help/thankgnus-2002.nb.html
├─BROKEN─ https://fsfe.org/about/legal/de/de.nb.html (HTTP_404)
Finished! 80 links found. 76 excluded. 1 broken.
