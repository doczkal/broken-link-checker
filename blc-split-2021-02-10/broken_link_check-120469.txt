Getting links from: https://fsfe.org/freesoftware/education/edu-related-content.pt.html
├───OK─── https://fsfe.org/activities/mankind/lsm2002/index.pt.html
├─BROKEN─ https://fsfe.org/associates/ofset/associate (HTTP_404)
├───OK─── https://fsfe.org/activities/mankind/lsm2002/press-release.pt.html
├───OK─── https://fsfe.org/freesoftware/transcripts/rms-fs-2006-03-09.pt.html
├───OK─── https://fsfe.org/events/2004/FISL/fisl.pt.html
├─BROKEN─ https://fsfe.org/freesoftware/education/news/2012/news-20121217-01 (HTTP_404)
├───OK─── https://fsfe.org/news/2003/lettera_MIUR-2003-07-16.pt.html
├───OK─── https://fsfe.org/activities/tgs/tgs.pt.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool1.pt.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool2.pt.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool3.pt.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool4.pt.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool5.pt.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool6.pt.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool7.pt.html
├───OK─── https://fsfe.org/activities/tgs/tagatschool8.pt.html
Finished! 112 links found. 96 excluded. 2 broken.
