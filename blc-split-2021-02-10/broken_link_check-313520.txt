Getting links from: https://fsfe.org/contact/contact.nl.html
├───OK─── https://fsfe.org/about/team.nl.html
├─BROKEN─ https://fsfe.org/about/codeofconduct/index.nl.html (HTTP_404)
Finished! 83 links found. 81 excluded. 1 broken.
