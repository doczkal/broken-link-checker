Getting links from: https://fsfe.org/news/2019/news-20191022-01.cs.html
├───OK─── https://fsfe.org/about/people/interviews/grun.cs.html#interview
├─BROKEN─ https://fsfe.org/news/2019/about/codeofconduct (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/contribute/web/web.cs.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/news-20191002.cs.html (HTTP_404)
├───OK─── https://fsfe.org/news/2019/news-20190701-01.cs.html
Finished! 222 links found. 217 excluded. 3 broken.
