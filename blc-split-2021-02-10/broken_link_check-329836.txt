Getting links from: https://fsfe.org/about/funds/2007.it.html
├───OK─── https://fsfe.org/help/thankgnus-2007.it.html
├─BROKEN─ https://fsfe.org/about/legal/de/de.it.html (HTTP_404)
├───OK─── https://fsfe.org/about/people/greve/index.it.html
Finished! 85 links found. 82 excluded. 1 broken.
