Getting links from: https://fsfe.org/news/nl/nl-201203.de.html
├───OK─── https://fsfe.org/news/2012/news-20120223-01.de.html
├───OK─── https://fsfe.org/activities/ilovefs/2012/whylovefs.de.html
├───OK─── https://fsfe.org/news/2012/news-20120214-01.de.html
├───OK─── https://fsfe.org/news/2012/news-20120210-01.de.html
├───OK─── https://fsfe.org/news/2012/news-20120204-01.de.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.de.html (HTTP_404)
Finished! 119 links found. 113 excluded. 1 broken.
