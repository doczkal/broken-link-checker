Getting links from: https://fsfe.org/news/nl/nl-201205.pl.html
├─BROKEN─ https://fsfe.org/activities/swpat/swapt.pl.html (HTTP_404)
├───OK─── https://fsfe.org/news/2012/news-20120412-02.pl.html
├───OK─── https://fsfe.org/news/2012/news-20120402-01.pl.html
├─BROKEN─ https://fsfe.org/project/os/def.pl.html (HTTP_404)
├───OK─── https://fsfe.org/news/2012/news-20120426-01.pl.html
├───OK─── https://fsfe.org/freesoftware/standards/uk-standards-consultation.pl.html
├───OK─── https://fsfe.org/news/2012/news-20120425-02.pl.html
├───OK─── https://fsfe.org/news/2012/news-20120425-01.pl.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.pl.html (HTTP_404)
Finished! 114 links found. 105 excluded. 3 broken.
