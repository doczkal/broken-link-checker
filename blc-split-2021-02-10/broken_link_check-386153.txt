Getting links from: https://fsfe.org/news/nl/nl-201810.cs.html
├───OK─── https://fsfe.org/news/2018/news-20181023-02.cs.html
├─BROKEN─ https://fsfe.org/about/mancheva/mancheva (HTTP_404)
├───OK─── https://fsfe.org/news/2018/news-20181010-01.cs.html
├───OK─── https://fsfe.org/news/2018/news-20180913-01.cs.html
├───OK─── https://fsfe.org/tags/tagged-microsoft.cs.html
├───OK─── https://fsfe.org/tags/tagged-digital-o-mat.cs.html
Finished! 93 links found. 87 excluded. 1 broken.
