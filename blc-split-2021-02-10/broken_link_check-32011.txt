Getting links from: https://fsfe.org/freesoftware/education/edu-related-content.es.html
├─BROKEN─ https://fsfe.org/associates/ofset/associate (HTTP_404)
├───OK─── https://fsfe.org/news/2011/news-20111128-02.es.html#education
├───OK─── https://fsfe.org/freesoftware/transcripts/rms-fs-2006-03-09.es.html
├─BROKEN─ https://fsfe.org/freesoftware/education/news/2012/news-20121217-01 (HTTP_404)
Finished! 112 links found. 108 excluded. 2 broken.
