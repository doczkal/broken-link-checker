Getting links from: https://fsfe.org/news/2009/ga2009.mk.html
├───OK─── https://fsfe.org/news/2009/ga2009.de.html
├───OK─── https://fsfe.org/news/2009/ga2009.el.html
├───OK─── https://fsfe.org/news/2009/ga2009.es.html
├───OK─── https://fsfe.org/news/2009/ga2009.fr.html
├───OK─── https://fsfe.org/news/2009/ga2009.nl.html
├─BROKEN─ https://fsfe.org/about/members.mk.html (HTTP_404)
Finished! 69 links found. 63 excluded. 1 broken.
