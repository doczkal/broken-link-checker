Getting links from: https://fsfe.org/news/nl/nl-201501.pl.html
├───OK─── https://fsfe.org/news/nl/nl-201501.fr.html
├───OK─── https://fsfe.org/fellowship/about/fellowship.pl.html#youget
├─BROKEN─ https://fsfe.org/about/legal/constitution.pl.html#id-fellowship-seats (HTTP_404)
├─BROKEN─ https://fsfe.org/about/ojasild (HTTP_404)
Finished! 88 links found. 84 excluded. 2 broken.
