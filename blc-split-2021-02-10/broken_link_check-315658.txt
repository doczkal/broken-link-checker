Getting links from: https://fsfe.org/news/nl/nl-201605.ru.html#
├───OK─── https://fsfe.org/news/nl/nl-201605.de.html
├───OK─── https://fsfe.org/news/nl/nl-201605.en.html
├───OK─── https://fsfe.org/news/nl/nl-201605.nl.html
├───OK─── https://fsfe.org/news/nl/nl-201605.sq.html
├───OK─── https://fsfe.org/news/2016/news-20160502-01
├───OK─── https://fsfe.org/activities/radiodirective/statement
├─BROKEN─ https://fsfe.org/news/2016/news-20160406 (HTTP_404)
├───OK─── https://fsfe.org/about/transparency-commitment
Finished! 92 links found. 84 excluded. 1 broken.
