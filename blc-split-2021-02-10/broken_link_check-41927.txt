Getting links from: https://fsfe.org/news/nl/nl-201201.da.html
├─BROKEN─ https://fsfe.org/activities/elections/askyourcandidates/example-questions.xhtml (HTTP_404)
├───OK─── https://fsfe.org/news/2011/news-20111220-01.da.html
├───OK─── https://fsfe.org/donate/letter-2011.da.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.da.html (HTTP_404)
Finished! 87 links found. 83 excluded. 2 broken.
