Getting links from: https://fsfe.org/news/2016/news-20160224-01.da.html
├─BROKEN─ https://fsfe.org/activities/ilovefs/whylovefs/photos/gallery/ilovefs-gallery-thumb-25.jpg (HTTP_404)
├───OK─── https://fsfe.org/activities/policy.da.html
├───OK─── https://fsfe.org/news/2015/news-20150325-01.da.html
├───OK─── https://fsfe.org/activities/pdfreaders/pdfreaders.da.html
Finished! 89 links found. 85 excluded. 1 broken.
