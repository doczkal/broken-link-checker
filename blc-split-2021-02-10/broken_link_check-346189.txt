Getting links from: https://fsfe.org/activities/igf/sovsoft.el.html
├───OK─── https://fsfe.org/activities/igf/sovsoft.ca.html
├───OK─── https://fsfe.org/activities/igf/sovsoft.en.html
├───OK─── https://fsfe.org/activities/igf/SovereignSoftware.pdf
├─BROKEN─ https://fsfe.org/activities/igf/ref3 (HTTP_404)
├─BROKEN─ https://fsfe.org/activities/igf/4 (HTTP_404)
├───OK─── https://fsfe.org/activities/wipo/fser.el.html
Finished! 91 links found. 85 excluded. 2 broken.
