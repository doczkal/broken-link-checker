Getting links from: https://fsfe.org/about/people/kirschner/kirschner.sl.html
├───OK─── https://fsfe.org/about/people/kirschner/kirschner.de.html
├───OK─── https://fsfe.org/about/people/kirschner/kirschner.el.html
├───OK─── https://fsfe.org/about/people/kirschner/kirschner.it.html
├───OK─── https://fsfe.org/about/people/kirschner/kirschner.nl.html
├─BROKEN─ https://fsfe.org/de/index.sl.html (HTTP_404)
├───OK─── https://fsfe.org/fellowship/index.sl.html
├─BROKEN─ https://fsfe.org/de/index.sl.html (HTTP_404)
├─BROKEN─ https://fsfe.org/de/index.sl.html (HTTP_404)
Finished! 72 links found. 64 excluded. 3 broken.
