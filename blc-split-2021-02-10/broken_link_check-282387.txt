Getting links from: https://fsfe.org/news/2008/index.pl.html
├───OK─── https://fsfe.org/news/2008/news-20081215-01.pl.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.pl.html (HTTP_404)
├───OK─── https://fsfe.org/news/2008/news-20081208-01.pl.html
├───OK─── https://fsfe.org/news/2008/gnu-25-years.pl.html
├───OK─── https://fsfe.org/news/2008/news-20080305-01.pl.html
├───OK─── https://fsfe.org/news/2008/news-20080301-01.pl.html
├───OK─── https://fsfe.org/news/2008/news-20080222-01.pl.html
├───OK─── https://fsfe.org/news/2008/news-20080214-01.pl.html
├───OK─── https://fsfe.org/news/2008/news-20080118-01.pl.html
Finished! 138 links found. 129 excluded. 1 broken.
