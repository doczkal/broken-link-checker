Getting links from: https://fsfe.org/about/people/mehl/mehl.tr.html
├───OK─── https://fsfe.org/about/people/mehl/mehl.de.html
├───OK─── https://fsfe.org/about/people/mehl/mehl.nl.html
├───OK─── https://fsfe.org/activities/radiodirective/index.tr.html
├─BROKEN─ https://fsfe.org/de (HTTP_404)
├───OK─── https://fsfe.org/activities/drm/index.tr.html
├─BROKEN─ https://fsfe.org/de (HTTP_404)
├─BROKEN─ https://fsfe.org/de (HTTP_404)
Finished! 82 links found. 75 excluded. 3 broken.
