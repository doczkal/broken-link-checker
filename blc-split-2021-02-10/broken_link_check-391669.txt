Getting links from: https://fsfe.org/contribute/web/tagging.pl.html
├───OK─── https://fsfe.org/contribute/web/index.pl.html
├─BROKEN─ https://fsfe.org/uk/uk.pl.html (HTTP_404)
├───OK─── https://fsfe.org/tags/tagged.pl.html
├───OK─── https://fsfe.org/contribute/web/css.pl.html
├───OK─── https://fsfe.org/contribute/template.pl.html
Finished! 53 links found. 48 excluded. 1 broken.
