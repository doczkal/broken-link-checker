Getting links from: https://fsfe.org/news/2008/news-20080118-01.zh.html
├───OK─── https://fsfe.org/news/2008/news-20080118-01.el.html
├───OK─── https://fsfe.org/news/2008/news-20080118-01.en.html
├───OK─── https://fsfe.org/news/2008/news-20080118-01.es.html
├───OK─── https://fsfe.org/news/2008/news-20080118-01.it.html
├───OK─── https://fsfe.org/news/2008/news-20080118-01.nl.html
├─BROKEN─ https://fsfe.org/ftf/index.zh.html (HTTP_404)
Finished! 56 links found. 50 excluded. 1 broken.
