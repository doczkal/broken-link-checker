Getting links from: https://fsfe.org/news/2013/index.bg.html
├───OK─── https://fsfe.org/news/2013/news-20131213-01.bg.html
├───OK─── https://fsfe.org/news/2013/news-20130927-01.bg.html
├───OK─── https://fsfe.org/news/2013/news-20130920-01.bg.html
├─BROKEN─ https://fsfe.org/news/2013/2013-07-26_Open_Letter_to_NEC.bg.html (HTTP_404)
├───OK─── https://fsfe.org/news/2013/news-20130327-02.bg.html
├───OK─── https://fsfe.org/news/2013/news-20130226-01.bg.html
Finished! 289 links found. 283 excluded. 1 broken.
