Getting links from: https://fsfe.org/news/nl/nl-201212.sl.html
├───OK─── https://fsfe.org/news/2012/news-20121101-02.sl.html
├───OK─── https://fsfe.org/news/2012/news-20121116-01.sl.html
├─BROKEN─ https://fsfe.org/news/nl/lwn.net/Articles/523537/index.sl.html (HTTP_404)
├───OK─── https://fsfe.org/news/2012/news-20121112-01.sl.html
├───OK─── https://fsfe.org/news/2010/news-20100324-01.sl.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.sl.html (HTTP_404)
Finished! 115 links found. 109 excluded. 2 broken.
