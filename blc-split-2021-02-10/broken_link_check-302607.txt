Getting links from: https://fsfe.org/news/2019/news-20191022-01.pl.html#ngi0
├───OK─── https://fsfe.org/activities/radiodirective/statement.pl.html
├───OK─── https://fsfe.org/about/people/interviews/grun.pl.html#interview
├─BROKEN─ https://fsfe.org/news/2019/about/codeofconduct (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/contribute/web/web.pl.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/news-20191002.pl.html (HTTP_404)
├───OK─── https://fsfe.org/news/2019/news-20190701-01.pl.html
├───OK─── https://fsfe.org/tags/tagged-radiodirective.pl.html
├───OK─── https://fsfe.org/tags/tagged-savecodeshare.pl.html
Finished! 222 links found. 214 excluded. 3 broken.
