Getting links from: https://fsfe.org/about/people/kirschner/kirschner.ca.html
├───OK─── https://fsfe.org/contribute/translators/index.ca.html
├───OK─── https://fsfe.org/index.ca.html
├───OK─── https://fsfe.org/about/about.ca.html
├───OK─── https://fsfe.org/activities/activities.ca.html
├───OK─── https://fsfe.org/contribute/contribute.ca.html
├─BROKEN─ https://fsfe.org/de/index.ca.html (HTTP_404)
├─BROKEN─ https://fsfe.org/de/index.ca.html (HTTP_404)
├─BROKEN─ https://fsfe.org/de/index.ca.html (HTTP_404)
├───OK─── https://fsfe.org/about/js-licences.ca.html
├───OK─── https://fsfe.org/contact/contact.ca.html
├───OK─── https://fsfe.org/about/legal/imprint.ca.html
├───OK─── https://fsfe.org/about/transparency-commitment.ca.html
├───OK─── https://fsfe.org/contribute/web/web.ca.html
└───OK─── https://fsfe.org/contribute/translators/translators.ca.html
Finished! 72 links found. 58 excluded. 3 broken.
