Getting links from: https://fsfe.org/contribute/web/tagging.hr.html
├───OK─── https://fsfe.org/contribute/web/index.hr.html
├─BROKEN─ https://fsfe.org/uk/uk.hr.html (HTTP_404)
├───OK─── https://fsfe.org/tags/tags.hr.html
├───OK─── https://fsfe.org/tags/tagged.hr.html
Finished! 53 links found. 49 excluded. 1 broken.
