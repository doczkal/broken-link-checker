Getting links from: https://fsfe.org/news/nl/nl-201308.ar.html
├───OK─── https://fsfe.org/news/2013/news-20130729-01.ar.html
├───OK─── https://fsfe.org/news/2013/news-20130730-01.ar.html
├─BROKEN─ https://fsfe.org/news/2013/2013-07-26_Open_Letter_to_NEC.ar.html (HTTP_404)
├───OK─── https://fsfe.org/news/2013/news-20130712-01.ar.html
├───OK─── https://fsfe.org/freesoftware/standards/transparency-letter.ar.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.ar.html (HTTP_404)
Finished! 113 links found. 107 excluded. 2 broken.
