Getting links from: https://fsfe.org/about/people/testimonials.fi.html#kroah-hartman
├───OK─── https://fsfe.org/about/people/interviews/cryptie.fi.html#video-30s-cryptie
├───OK─── https://fsfe.org/about/people/interviews/lequertier.fi.html#video-30s-vincent
├───OK─── https://fsfe.org/about/people/interviews/zerolo.fi.html#video-30s-tomas
├─BROKEN─ https://fsfe.org/about/people/interviews/bernhard.fi.html#video-30s-bernhard (HTTP_404)
├───OK─── https://fsfe.org/about/people/interviews/weitzhofer.fi.html#interview
├───OK─── https://fsfe.org/about/people/interviews/grun.fi.html#interview
├───OK─── https://fsfe.org/about/people/interviews/ockers.fi.html#video-30s-andre
├───OK─── https://fsfe.org/about/people/interviews/mueller.fi.html#interview
├───OK─── https://fsfe.org/about/people/interviews/snow.fi.html#interview
├───OK─── https://fsfe.org/about/people/interviews/gkotsopoulou.fi.html#interview
Finished! 122 links found. 112 excluded. 1 broken.
