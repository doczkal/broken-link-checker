Getting links from: https://fsfe.org/news/nl/nl-201205.el.html
├─BROKEN─ https://fsfe.org/activities/swpat/swapt.el.html (HTTP_404)
├─BROKEN─ https://fsfe.org/project/os/def.el.html (HTTP_404)
├───OK─── https://fsfe.org/events/events.el.html
Finished! 112 links found. 109 excluded. 2 broken.
