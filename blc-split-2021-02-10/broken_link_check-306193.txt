Getting links from: https://fsfe.org/news/nl/nl-201804.ar.html
├───OK─── https://fsfe.org/news/nl/nl-201804.es.html
├───OK─── https://fsfe.org/news/nl/nl-201804.nl.html
├─BROKEN─ https://fsfe.org/news/2018/news-20180414 (HTTP_404)
Finished! 95 links found. 92 excluded. 1 broken.
