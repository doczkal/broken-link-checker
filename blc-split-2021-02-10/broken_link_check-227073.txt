Getting links from: https://fsfe.org/news/nl/nl-201302.hr.html
├───OK─── https://fsfe.org/news/2012/news-20121211-01.hr.html
├───OK─── https://fsfe.org/news/2012/news-20121217-01.hr.html
├───OK─── https://fsfe.org/news/2012/report-2012.hr.html
├───OK─── https://fsfe.org/activities/ilovefs/2013/index.hr.html
├───OK─── https://fsfe.org/activities/ilovefs/2013/whylovefs.hr.html
├─BROKEN─ https://fsfe.org/activities/ilovefs/2013/banners.hr.html (HTTP_404)
├─BROKEN─ https://fsfe.org/order/promoorder.hr.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.hr.html (HTTP_404)
Finished! 119 links found. 111 excluded. 3 broken.
