Getting links from: https://fsfe.org/news/2017/news-20171107-01.sl.html
├───OK─── https://fsfe.org/news/2017/news-20171107-01.en.html
├───OK─── https://fsfe.org/news/2017/news-20171107-01.nl.html
├─BROKEN─ https://fsfe.org/about/legal/constitution#id-fellowship-seats (HTTP_404)
Finished! 69 links found. 66 excluded. 1 broken.
