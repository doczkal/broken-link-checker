Getting links from: https://fsfe.org/news/2017/news-20171207-01.it.html
├─BROKEN─ https://fsfe.org/news/2017/poster.png (HTTP_404)
├─BROKEN─ https://fsfe.org/img/poster.jpg (HTTP_404)
├───OK─── https://fsfe.org/about/funds/2016.it.html
├─BROKEN─ https://fsfe.org/order/2016/tshirt-nocloud-green-front-small.jpg (HTTP_404)
├─BROKEN─ https://fsfe.org/order/2016/tshirt-gnupg-blue-front-small.jpg (HTTP_404)
├─BROKEN─ https://fsfe.org/order/2016/girlie-ilovefs-darkblue-front-small.jpg (HTTP_404)
├─BROKEN─ https://fsfe.org/order/2017/zipped-fsfs-black-front-small.jpg (HTTP_404)
├─BROKEN─ https://fsfe.org/order/2017/zipped-fsfs-burgundy-back-small.jpg (HTTP_404)
├───OK─── https://fsfe.org/about/people/avatars/sliwinski.png
Finished! 181 links found. 172 excluded. 7 broken.
