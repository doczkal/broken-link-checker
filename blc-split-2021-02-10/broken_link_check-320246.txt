Getting links from: https://fsfe.org/news/2008/index.nn.html
├───OK─── https://fsfe.org/news/2008/news-20081215-01.nn.html
├─BROKEN─ https://fsfe.org/about/legal/constitution.nn.html (HTTP_404)
├───OK─── https://fsfe.org/news/2008/news-20081208-01.nn.html
├───OK─── https://fsfe.org/news/2008/gnu-25-years.nn.html
├───OK─── https://fsfe.org/news/2008/news-20080305-01.nn.html
├───OK─── https://fsfe.org/news/2008/news-20080301-01.nn.html
├───OK─── https://fsfe.org/news/2008/news-20080222-01.nn.html
├───OK─── https://fsfe.org/news/2008/news-20080220-01.nn.html
├───OK─── https://fsfe.org/news/2008/news-20080214-01.nn.html
├───OK─── https://fsfe.org/news/2008/news-20080118-01.nn.html
Finished! 138 links found. 128 excluded. 1 broken.
