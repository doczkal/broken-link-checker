Getting links from: https://fsfe.org/news/nl/nl-201309.el.html
├───OK─── https://fsfe.org/news/2013/news-20130612-01.el.html
├───OK─── https://fsfe.org/activities/msooxml/msooxml-interoperability.el.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.el.html (HTTP_404)
Finished! 115 links found. 112 excluded. 1 broken.
