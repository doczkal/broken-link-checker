Getting links from: https://fsfe.org/news/nl/nl-201112.es.html
├───OK─── https://fsfe.org/news/nl/nl-201112.el.html
├───OK─── https://fsfe.org/news/2011/news-20111122-01.es.html
├───OK─── https://fsfe.org/freesoftware/education/education.es.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.es.html (HTTP_404)
Finished! 109 links found. 105 excluded. 1 broken.
