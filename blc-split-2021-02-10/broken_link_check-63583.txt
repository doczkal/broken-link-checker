Getting links from: https://fsfe.org/contribute/web/tagging.uk.html
├───OK─── https://fsfe.org/contribute/web/tagging.en.html
├─BROKEN─ https://fsfe.org/uk/uk.uk.html (HTTP_404)
├───OK─── https://fsfe.org/tags/tags.uk.html
├───OK─── https://fsfe.org/tags/tagged.uk.html
├───OK─── https://fsfe.org/contribute/web/css.uk.html
├───OK─── https://fsfe.org/contribute/template.uk.html
Finished! 53 links found. 47 excluded. 1 broken.
