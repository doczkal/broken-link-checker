Getting links from: https://fsfe.org/about/people/testimonials.ru.html#kroah-hartman
├───OK─── https://fsfe.org/about/people/interviews/cryptie.ru.html#video-30s-cryptie
├───OK─── https://fsfe.org/about/people/interviews/lequertier.ru.html#video-30s-vincent
├───OK─── https://fsfe.org/about/people/interviews/zerolo.ru.html#video-30s-tomas
├─BROKEN─ https://fsfe.org/about/people/interviews/bernhard.ru.html#video-30s-bernhard (HTTP_404)
├───OK─── https://fsfe.org/about/people/interviews/weitzhofer.ru.html#interview
├───OK─── https://fsfe.org/about/people/interviews/ockers.ru.html#video-30s-andre
├───OK─── https://fsfe.org/about/people/interviews/mueller.ru.html#interview
├───OK─── https://fsfe.org/about/people/interviews/snow.ru.html#interview
├───OK─── https://fsfe.org/about/people/interviews/gkotsopoulou.ru.html#interview
Finished! 122 links found. 113 excluded. 1 broken.
