Getting links from: https://fsfe.org/news/2016/news-20160224-01.pt.html
├───OK─── https://fsfe.org/news/2015/news-20150303-01.pt.html
├─BROKEN─ https://fsfe.org/activities/ilovefs/whylovefs/photos/gallery/ilovefs-gallery-thumb-25.jpg (HTTP_404)
├───OK─── https://fsfe.org/news/2015/news-20150506-01.pt.html
├───OK─── https://fsfe.org/news/2015/news-20150605-02.pt.html
├───OK─── https://fsfe.org/news/2015/news-20150918-02.pt.html
Finished! 90 links found. 85 excluded. 1 broken.
