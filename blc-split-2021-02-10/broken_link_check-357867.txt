Getting links from: https://fsfe.org/news/nl/nl-201308.et.html
├─BROKEN─ https://fsfe.org/news/2013/2013-07-26_Open_Letter_to_NEC.et.html (HTTP_404)
├───OK─── https://fsfe.org/news/2013/news-20130712-01.et.html
├───OK─── https://fsfe.org/freesoftware/standards/transparency-letter.et.html
├───OK─── https://fsfe.org/about/fsfnetwork.et.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.et.html (HTTP_404)
├───OK─── https://fsfe.org/about/js-licences.et.html
├───OK─── https://fsfe.org/about/legal/imprint.et.html
Finished! 112 links found. 105 excluded. 2 broken.
