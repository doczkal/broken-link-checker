Getting links from: https://fsfe.org/news/nl/nl-201308.sv.html#
├───OK─── https://fsfe.org/news/nl/nl-201308.en.html
├───OK─── https://fsfe.org/news/nl/nl-201308.es.html
├─BROKEN─ https://fsfe.org/news/2013/2013-07-26_Open_Letter_to_NEC.sv.html (HTTP_404)
├───OK─── https://fsfe.org/freesoftware/standards/transparency-letter.sv.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.sv.html (HTTP_404)
Finished! 112 links found. 107 excluded. 2 broken.
