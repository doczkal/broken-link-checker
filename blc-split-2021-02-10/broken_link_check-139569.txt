Getting links from: https://fsfe.org/contribute/translators/index.nn.html
├─BROKEN─ https://fsfe.org/evets/events.nn.html (HTTP_404)
├───OK─── https://fsfe.org/news/newsletter.nn.html
├───OK─── https://fsfe.org/contribute/spreadtheword.nn.html
├───OK─── https://fsfe.org/contribute/translators/wordlist.nn.html
Finished! 86 links found. 82 excluded. 1 broken.
