Getting links from: https://fsfe.org/news/nl/nl-201411.nl.html
├───OK─── https://fsfe.org/news/2010/news-20100803-01.nl.html
├───OK─── https://fsfe.org/contribute/internship.nl.html
├───OK─── https://fsfe.org/news/2014/news-20140326-02.nl.html
├─BROKEN─ https://fsfe.org/news/nl/about/kirschner/kirschner.nl.html (HTTP_404)
Finished! 110 links found. 106 excluded. 1 broken.
