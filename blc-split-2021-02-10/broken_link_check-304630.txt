Getting links from: https://fsfe.org/activities/igf/wgig.de.html
├───OK─── https://fsfe.org/activities/igf/wgig.el.html
├─BROKEN─ https://fsfe.org/activities/igf/WGIG-WP-Cybercrime-Comments.pdf (HTTP_404)
├─BROKEN─ https://fsfe.org/activities/igf/WGIG-WP-IPR-Comments.pdf (HTTP_404)
Finished! 60 links found. 57 excluded. 2 broken.
