Getting links from: https://fsfe.org/news/nl/nl-201206.sr.html
├───OK─── https://fsfe.org/news/2012/news-20120509-01.sr.html
├───OK─── https://fsfe.org/news/2012/news-20120509-02.sr.html
├───OK─── https://fsfe.org/news/2012/news-20120528-01.sr.html
├───OK─── https://fsfe.org/news/2012/news-20120525-01.sr.html
├───OK─── https://fsfe.org/activities/pdfreaders/parliamentary-questions-eu.sr.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.sr.html (HTTP_404)
Finished! 103 links found. 97 excluded. 1 broken.
