Getting links from: https://fsfe.org/news/nl/nl-201302.el.html
├───OK─── https://fsfe.org/news/2012/report-2012.el.html
├───OK─── https://fsfe.org/activities/ilovefs/2013/index.el.html
├───OK─── https://fsfe.org/activities/ilovefs/2013/whylovefs.el.html
├─BROKEN─ https://fsfe.org/activities/ilovefs/2013/banners.el.html (HTTP_404)
├─BROKEN─ https://fsfe.org/order/promoorder.el.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.el.html (HTTP_404)
Finished! 119 links found. 113 excluded. 3 broken.
