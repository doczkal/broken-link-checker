Getting links from: https://fsfe.org/about/people/roy/roy.it.html
├───OK─── https://fsfe.org/activities/ftf/ftf.it.html
├─BROKEN─ https://fsfe.org/fr/index.it.html (HTTP_404)
├─BROKEN─ https://fsfe.org/legal (HTTP_404)
├───OK─── https://fsfe.org/fellowship/index.it.html
Finished! 80 links found. 76 excluded. 2 broken.
