Getting links from: https://fsfe.org/news/2019/news-20191022-01.ru.html
├───OK─── https://fsfe.org/news/2016/news-20160725-01.ru.html
├───OK─── https://fsfe.org/news/2019/news-20190514-01.ru.html
├───OK─── https://fsfe.org/news/2019/news-20190515-01.ru.html
├───OK─── https://fsfe.org/news/2019/news-20190515-02.ru.html
├───OK─── https://fsfe.org/news/2019/news-20190827-01.ru.html
├───OK─── https://fsfe.org/activities/radiodirective/statement.ru.html
├───OK─── https://fsfe.org/news/2019/news-20191007-01.ru.html
├───OK─── https://fsfe.org/news/2019/news-20190807-01.ru.html
├───OK─── https://fsfe.org/contribute/promopics/ilovefs-sticker-444px.jpg
├─BROKEN─ https://fsfe.org/news/2019/about/codeofconduct (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/contribute/web/web.ru.html (HTTP_404)
├─BROKEN─ https://fsfe.org/news/2019/news-20191002.ru.html (HTTP_404)
├───OK─── https://fsfe.org/news/2019/news-20190701-01.ru.html
├───OK─── https://fsfe.org/news/2019/news-20191014-01.ru.html
├───OK─── https://fsfe.org/order/2017/kidstshirt-fork-red-front-large-350px.jpg
├───OK─── https://fsfe.org/news/podcast/episode-1.ru.html
├───OK─── https://fsfe.org/tags/tagged-routers.ru.html
├───OK─── https://fsfe.org/tags/tagged-radiodirective.ru.html
├───OK─── https://fsfe.org/tags/tagged-sustainability.ru.html
├───OK─── https://fsfe.org/tags/tagged-ngi.ru.html
Finished! 223 links found. 203 excluded. 3 broken.
