Getting links from: https://fsfe.org/contact/index.hr.html
├───OK─── https://fsfe.org/about/index.hr.html
├───OK─── https://fsfe.org/about/legal/legal.hr.html
├───OK─── https://fsfe.org/activities/ln/ln.hr.html
├───OK─── https://fsfe.org/activities/licence-questions/licence-questions.hr.html
├───OK─── https://fsfe.org/about/team.hr.html
├─BROKEN─ https://fsfe.org/about/codeofconduct/index.hr.html (HTTP_404)
Finished! 86 links found. 80 excluded. 1 broken.
