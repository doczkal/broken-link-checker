Getting links from: https://fsfe.org/news/nl/nl-201202.nn.html
├───OK─── https://fsfe.org/news/nl/nl-201202.es.html
├───OK─── https://fsfe.org/news/2012/news-20120131-01.nn.html
├─BROKEN─ https://fsfe.org/activities/ilovefs/2012/banners.nn.html (HTTP_404)
├───OK─── https://fsfe.org/activities/ilovefs/2012/unperfekthaus.nn.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.nn.html (HTTP_404)
Finished! 109 links found. 104 excluded. 2 broken.
