Getting links from: https://fsfe.org/contact/contact.ca.html
├───OK─── https://fsfe.org/contact/contact.de.html
├───OK─── https://fsfe.org/about/people/kirschner/index.ca.html
├─BROKEN─ https://fsfe.org/about/codeofconduct/index.ca.html (HTTP_404)
Finished! 86 links found. 83 excluded. 1 broken.
