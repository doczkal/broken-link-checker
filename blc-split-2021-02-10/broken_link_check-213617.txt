Getting links from: https://fsfe.org/news/nl/nl-201501.nn.html
├───OK─── https://fsfe.org/news/nl/nl-201501.sq.html
├───OK─── https://fsfe.org/about/people/gerloff
├───OK─── https://fsfe.org/fellowship/about/fellowship.nn.html#youget
├─BROKEN─ https://fsfe.org/about/legal/constitution.nn.html#id-fellowship-seats (HTTP_404)
├─BROKEN─ https://fsfe.org/about/ojasild (HTTP_404)
Finished! 88 links found. 83 excluded. 2 broken.
