Getting links from: https://fsfe.org/about/funds/2005.hr.html
├───OK─── https://fsfe.org/about/funds/2005.de.html
├───OK─── https://fsfe.org/about/funds/2005.el.html
├───OK─── https://fsfe.org/about/funds/2005.fr.html
├───OK─── https://fsfe.org/about/funds/2005.it.html
├───OK─── https://fsfe.org/about/funds/2005.nl.html
├───OK─── https://fsfe.org/help/thankgnus-2005.hr.html
├─BROKEN─ https://fsfe.org/about/legal/de/de.hr.html (HTTP_404)
Finished! 87 links found. 80 excluded. 1 broken.
