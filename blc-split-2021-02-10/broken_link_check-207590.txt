Getting links from: https://fsfe.org/news/nl/nl-201110.et.html
├───OK─── https://fsfe.org/news/nl/nl-201110.el.html
├───OK─── https://fsfe.org/news/nl/nl-201110.fr.html
├───OK─── https://fsfe.org/freesoftware/education/eduteam.et.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.et.html (HTTP_404)
Finished! 101 links found. 97 excluded. 1 broken.
