Getting links from: https://fsfe.org/news/nl/nl-201111.fi.html
├───OK─── https://fsfe.org/news/nl/nl-201111.en.html
├───OK─── https://fsfe.org/news/nl/nl-201111.it.html
├─BROKEN─ https://fsfe.org/news/nl/donate/thankgnus.fi.html (HTTP_404)
Finished! 99 links found. 96 excluded. 1 broken.
